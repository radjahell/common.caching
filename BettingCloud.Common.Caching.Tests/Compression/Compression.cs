﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Compression
{
    public partial class Compression : BaseTests
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformSet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                client.Set(key, data, ItemPolicy.Default);

                var cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => { return data; };

            using (var client = CreateClient())
            {
                var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

                Assert.NotNull(cachedData);
                Assert.Equal(data, cachedData);

                cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Disable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.Disable();
                c.UseCompression(10);

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
