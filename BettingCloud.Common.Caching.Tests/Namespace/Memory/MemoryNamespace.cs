﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Tests.Namespace.Memory
{
    public class MemoryNamespace : Namespace
    {
        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.Disable();
                c.UseNamespace("test");
                c.Policies
                    .Add(ItemPolicy.Default)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
