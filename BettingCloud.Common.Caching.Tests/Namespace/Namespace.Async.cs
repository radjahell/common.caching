﻿using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Namespace
{
    public abstract partial class Namespace : BaseTests
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task SetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, ItemPolicy.Default.Name);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task AddOrGetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => data;
            var client = CreateClient();
            var cachedData = await client.AddOrGetAsync(key, fetch, ItemPolicy.Default.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }
    }
}
