﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Tests.Namespace.Redis
{
    public class RedisNamespace : Namespace
    {
        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Disable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.UseNamespace("test");

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
