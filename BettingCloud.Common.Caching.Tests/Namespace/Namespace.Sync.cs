﻿using BettingCloud.Common.Caching.Policy;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Namespace
{
    public abstract partial class Namespace : BaseTests
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void Set<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, ItemPolicy.Default.Name);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void AddOrGet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => data;
            var client = CreateClient();
            var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }
    }
}
