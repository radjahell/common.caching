﻿#if NETCORE
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
#endif

namespace BettingCloud.Common.Caching.Tests
{
    public interface IConfigurationProvider
    {
        string Get(string key);

        int? GetInt32(string key);
    }

    internal class ConfigurationProvider : IConfigurationProvider
    {
#if NET
        public string Get(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
#endif

#if NETCORE

        public ConfigurationProvider()
        {
            Configuration = new ConfigurationBuilder()
                            .AddJsonFile("config.json")
                            .Build();
        }

        private IConfiguration Configuration { get; }

        public string Get(string key)
        {
            return Configuration[key];
        }
#endif

        public int? GetInt32(string key)
        {
            int port;
            var portValue = Get(key);

            if (int.TryParse(portValue, out port))
            {
                return port;
            }

            return null;
        }
    }
}
