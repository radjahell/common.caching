﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class LocalCache
    {
        #region Set<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformSet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            client.Set(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformSetWithNormalItemPolicy<T>(T data)
        {
            performSet(data, NormalPolicy.Name);
        }

        private void performSet<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            client.Set(key, data, policyName);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        #endregion

        #region AddOrGet<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<T> fetch = () => { return data; };

            var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGetWithNormalItemPolicy<T>(T data)
        {
            performAddOrGetWithItemPolicy(data, NormalPolicy.Name);
        }

        public void performAddOrGetWithItemPolicy<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<T> fetch = () => { return data; };

            var cachedData = client.AddOrGet(key, fetch, policyName);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }
        
        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformInvalidate<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            client.Set(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            client.Invalidate(key);

            cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion

        #region Remove

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformRemove<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            client.Set(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            client.Remove(key);

            cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion
    }
}
