﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class LocalCache
    {
        #region Set<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformSetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformSetWithNormalItemPolicyAsync<T>(T data)
        {
            await performSetAsync(data, NormalPolicy.Name);
        }

        private async Task performSetAsync<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            await client.SetAsync(key, data, policyName);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        #endregion

        #region AddOrGet<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformAddOrGetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<T> fetch = () => { return data; };

            var cachedData = await client.AddOrGetAsync(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }


        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformAddOrGetWithFuncOfTask<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<Task<T>> fetch = () => { return Task.FromResult(data); };

            var cachedData = await client.AddOrGetAsync(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        public async Task performAddOrGetWithItemPolicyAsync<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<T> fetch = () => { return data; };

            var cachedData = await client.AddOrGetAsync(key, fetch, policyName);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
        }

        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformInvalidateAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            await client.InvalidateAsync(key);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion

        #region Remove

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformRemoveAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            await client.RemoveAsync(key);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion
    }
}
