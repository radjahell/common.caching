﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Tests.Models
{
    public class UserModel
    {
        private static string[] Names = new string[] { "Kellie", "Hyacinth", "Izetta", "Phillip", "Cole", "Rupert", "Margie", "Kareem", "Amparo", "Karan", "Leon", "Stephany", "Macy", "Mario", "Theron", "Clarine", "Luetta", "Annelle", "Garrett", "Pearline", "Larry", "Stephine", "Whitley", "Lara", "Sophie", "Betsey", "Billye", "Sheryll", "Jerrica", "Madeleine", "Larita", "Moses", "Branden", "Mckenzie", "Debbi", "Romana", "Matha", "Nakisha", "Katheryn", "Chung", "Arnulfo", "Delila", "Miss", "Marchelle", "Kasha", "Mellie", "Luna", "Quyen", "Delta", "Catheryn" };

        public static UserModel Generate()
        {
            var rnd = new Random();
            var name = Names[rnd.Next(0, Names.Length - 1)];

            return new UserModel()
            {
                Name = name,
                Email = $"{name.ToLower()}@gmail.com"
            };
        }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}
