﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Tests;
using System;
using System.Collections.Concurrent;
using Xunit;

namespace BettingCloud.Common.Caching.DotNetCore.Tests
{
    public class StressTests : BaseTests
    {
        [Theory(Skip = "PerfomanceTest not required")]
        [MemberData(nameof(TestData))]
        public void RemoveKey<T>(T data)
        {
            var parallelProcesses = 10;
            var iterations = 100000;
            var client = CreateClient();
            var keys = new ConcurrentBag<string>();
            Func<T> fetch = () => { return data; };

            //Parallel.For(0, parallelProcesses, (index) =>
            //{
                for (var i = 0; i < iterations; i++)
                {
                    var key = Guid.NewGuid().ToString();
                    var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

                    Assert.NotNull(cachedData);

                    keys.Add(key);

                    cachedData = client.Get<T>(key);
                    AssertEquals(data, cachedData);
                }
            //});

            client.Dispose();
        }

        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }
        
        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Disable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                //c.Notifications.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.UseNamespace("sports");

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
