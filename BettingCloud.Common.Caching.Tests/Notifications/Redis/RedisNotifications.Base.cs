﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
//using StackExchange.Redis;

namespace BettingCloud.Common.Caching.Tests.Redis
{
    public partial class RedisNotifications : BaseTests
    {
        public string RedisConnectionString => Config.Get("RedisConnectionString");

        public int RedisDatabaseId => Config.GetInt32("RedisDatabaseId").Value;

        //internal IDatabase RedisDatabase
        //{
        //    get
        //    {
        //        var connection = ConnectionMultiplexer.Connect(RedisConnectionString);
        //        return connection.GetDatabase(RedisDatabaseId);
        //    }
        //}

        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.UseRedis(RedisConnectionString, RedisDatabaseId);

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
