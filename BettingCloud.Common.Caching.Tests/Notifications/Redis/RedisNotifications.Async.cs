﻿using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Redis
{
    public partial class RedisNotifications
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task DeleteKeyFromRedisAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();

            T Fetch()
            {
                return data;
            }

            var cachedData = await client.AddOrGetAsync(key, Fetch, ItemPolicy.Default);

            Assert.NotEqual(default(T), cachedData);
            AssertEquals(data, cachedData);

           // RedisDatabase.KeyDelete(key);

            Thread.Sleep(200);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }


        #region Remove

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task RemoveKeyAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client1 = CreateClient();
            var client2 = CreateClient();

            T Fetch()
            {
                return data;
            }

            var cachedData = await client1.AddOrGetAsync(key, Fetch, ItemPolicy.Default);

            Assert.NotEqual(default(T), cachedData);
            AssertEquals(data, cachedData);

            cachedData = await client2.GetAsync<T>(key);

            if (cachedData.IsNullOrDefault())
            {
                Console.WriteLine(cachedData);
            }

            Assert.NotEqual(default(T), cachedData);
            AssertEquals(data, cachedData);

            await client1.RemoveAsync(key);

            Thread.Sleep(100);

            cachedData = await client2.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }
        
        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task InvalidateKeyAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client1 = CreateClient();
            var client2 = CreateClient();

            Func<T> fetch = () => { return data; };

            var cachedData = await client1.AddOrGetAsync(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = await client2.GetAsync<T>(key);

            if (cachedData.IsNullOrDefault())
            {
                Console.WriteLine(cachedData);
            }

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            await client1.InvalidateAsync(key);

            Thread.Sleep(200);

            cachedData = await client2.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion
    }
}
