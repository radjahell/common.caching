﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Enumerations;
using System;

namespace BettingCloud.Common.Caching.Tests.RabbitMQ
{
    public partial class RabbitMQNotifications : BaseTests, IDisposable
    {
        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        public string RabbitMQHost
        {
            get
            {
                return Config.Get("RabbitMQHost");
            }
        }

        public string RabbitMQExchange
        {
            get
            {
                return Config.Get("RabbitMQExchange");
            }
        }
        
        public string RabbitMQVirtualHost
        {
            get
            {
                return Config.Get("RabbitMQVirtualHost");
            }
        }

        public string RabbitMQUsername
        {
            get
            {
                return Config.Get("RabbitMQUsername");
            }
        }

        public string RabbitMQPassword
        {
            get
            {
                return Config.Get("RabbitMQPassword");
            }
        }

        public int? RabbitMQPort
        {
            get
            {
                return Config.GetInt32("RabbitMQPort");
            }
        }
        
        internal override ICacheClient CreateClient()
        {
            return Create(CacheLocationType.Both,
                redisConnectionString: RedisConnectionString,
                redisDatabaseId: RedisDatabaseId,
                rabbitMQHost: RabbitMQHost,
                rabbitMQPort: RabbitMQPort,
                rabbitMQVirtualHost: RabbitMQVirtualHost,
                rabbitMQExchangeName: RabbitMQExchange,
                rabbitMQUsername: RabbitMQUsername,
                rabbitMQPassword: RabbitMQPassword);
        }

        public void Dispose()
        {
        }
    }
}
