﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.RabbitMQ
{
    public partial class RabbitMQNotifications
    {
        #region Remove

        [Theory]
        [MemberData(nameof(TestData))]
        public void RemoveKey<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client1 = CreateClient();
            var client2 = CreateClient();

            Func<T> fetch = () => { return data; };

            var cachedData = client1.AddOrGet(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = client2.Get<T>(key);

            if (cachedData.IsNullOrDefault())
            {
                Console.WriteLine(cachedData);
            }

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            client1.Remove(key);

            Thread.Sleep(200);

            cachedData = client2.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }
        
        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(TestData))]
        public void InvalidateKey<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client1 = CreateClient();
            var client2 = CreateClient();

            Func<T> fetch = () => { return data; };

            var cachedData = client1.AddOrGet(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = client2.Get<T>(key);

            if (cachedData.IsNullOrDefault())
            {
                Console.WriteLine(cachedData);
            }

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            client1.Invalidate(key);

            Thread.Sleep(100);

            cachedData = client2.Get<T>(key);

            if (cachedData.IsNullOrDefault() == false)
            {
                Thread.Sleep(500);
                cachedData = client2.Get<T>(key);
            }

            Assert.True(cachedData.IsNullOrDefault());
        }

        #endregion
    }
}
