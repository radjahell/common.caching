﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.FetchException.Local
{
    public partial class ThrowException
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGetSync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = Create(CacheLocationType.Local);
            Func<T> fetch = () => { throw new Exception(data.ToString()); };

            var exception = Assert.Throws<CacheFetchException>(() =>
            {
                var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);
            });

            Assert.NotNull(exception);
            Assert.NotNull(exception.InnerException);
            Assert.IsType<Exception>(exception.InnerException);
            Assert.Equal(data.ToString(), exception.InnerException.Message);
        }
    }
}
