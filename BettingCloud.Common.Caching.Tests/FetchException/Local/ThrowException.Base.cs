﻿using BettingCloud.Common.Caching.Client;
using System;

namespace BettingCloud.Common.Caching.Tests.FetchException.Local
{
    public partial class ThrowException : BaseTests
    {
        internal override ICacheClient CreateClient()
        {
            throw new NotSupportedException();
        }
    }
}
