﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Tests.FetchException.Remote.Redis
{
    public partial class ThrowException : BaseTests
    {
        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.Disable();

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
