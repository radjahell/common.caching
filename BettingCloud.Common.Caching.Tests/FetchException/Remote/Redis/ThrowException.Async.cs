﻿using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.FetchException.Remote.Redis
{
    public partial class ThrowException
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task PerformAddOrGetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            Func<T> fetch = () => { throw new Exception(data.ToString()); };

            var exception = await Assert.ThrowsAsync<CacheFetchException>(async () =>
            {
                var cachedData = await client.AddOrGetAsync(key, fetch, ItemPolicy.Default);
            });

            Assert.NotNull(exception);
            Assert.NotNull(exception.InnerException);
            Assert.IsType<Exception>(exception.InnerException);
            Assert.Equal(data.ToString(), exception.InnerException.Message);
        }
    }
}
