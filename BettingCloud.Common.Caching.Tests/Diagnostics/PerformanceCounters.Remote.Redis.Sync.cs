﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Diagnostics
{
    public partial class PerformanceCounters
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void RemoteRedisSet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            _redisClient.Set(key, data, ItemPolicy.Default);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void RemoteRedisHit<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            _redisClient.Set(key, data, ItemPolicy.Default);

            var cachedData = _redisClient.Get<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
            Assert.Equal(statistics.RemoteHit, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void RemoteRedisMiss<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            var cachedData = _redisClient.Get<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteMiss, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void RemoteRedisRemove<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            _redisClient.Set(key, data, ItemPolicy.Default);

            var cachedData = _redisClient.Get<T>(key);
            _redisClient.Remove(key);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
            Assert.Equal(statistics.RemoteHit, 1L);
            Assert.Equal(statistics.RemoteRemove, 1L);
        }
    }
}
