﻿using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Diagnostics
{
    public partial class PerformanceCounters
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void LocalSet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            client.Set(key, data, ItemPolicy.Default);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void LocalHit<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            client.Set(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
            Assert.Equal(statistics.LocalHit, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void LocalMiss<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();

            var cachedData = client.Get<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalMiss, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void LocalRemove<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            client.Set(key, data, ItemPolicy.Default);

            var cachedData = client.Get<T>(key);
            client.Remove(key);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
            Assert.Equal(statistics.LocalHit, 1L);
            Assert.Equal(statistics.LocalRemove, 1L);
        }
    }
}
