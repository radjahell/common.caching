﻿using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Diagnostics
{
    public partial class PerformanceCounters
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task LocalSetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            await client.SetAsync(key, data, ItemPolicy.Default);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task LocalHitAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
            Assert.Equal(statistics.LocalHit, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task LocalMissAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();

            var cachedData = await client.GetAsync<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalMiss, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task LocalRemoveAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateLocalClient();
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);
            await client.RemoveAsync(key);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.LocalSet, 1L);
            Assert.Equal(statistics.LocalHit, 1L);
            Assert.Equal(statistics.LocalRemove, 1L);
        }
    }
}
