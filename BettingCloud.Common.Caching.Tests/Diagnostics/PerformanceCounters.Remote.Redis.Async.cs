﻿using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Diagnostics
{
    public partial class PerformanceCounters
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task RemoteRedisSetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            await _redisClient.SetAsync(key, data, ItemPolicy.Default);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task RemoteRedisHitAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            await _redisClient.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await _redisClient.GetAsync<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
            Assert.Equal(statistics.RemoteHit, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task RemoteRedisMissAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            var cachedData = await _redisClient.GetAsync<T>(key);
            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteMiss, 1L);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task RemoteRedisRemoveAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            await _redisClient.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await _redisClient.GetAsync<T>(key);
            await _redisClient.RemoveAsync(key);

            var statistics = GetStatistic(key);

            Assert.Equal(statistics.RemoteSet, 1L);
            Assert.Equal(statistics.RemoteHit, 1L);
            Assert.Equal(statistics.RemoteRemove, 1L);
        }
    }
}
