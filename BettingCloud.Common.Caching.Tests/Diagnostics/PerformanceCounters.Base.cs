﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
//using BettingCloud.Common.Diagnostics.Performance;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Tests.Diagnostics
{
    public partial class PerformanceCounters : BaseTests, IDisposable //,IPerformanceProvider
    {
        private readonly ConcurrentDictionary<string, PerformanceCounterStatistics> _statistics;
        private readonly ICacheClient _redisClient;

        public PerformanceCounters()
        {
            _statistics = new ConcurrentDictionary<string, PerformanceCounterStatistics>();
            _redisClient = CreateRemoteRedisClient();
        }

        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }
        
        public Task HitAsync(string key, Dictionary<string, string> additionalData = null)
        {
            Action<PerformanceCounterStatistics> action = (stat) =>
            {
                if (additionalData == null)
                    return;

                var cacheLocation = Enum.Parse(typeof(CacheLocationType), additionalData["CacheLocation"]);
                var type = additionalData["Type"];

                if (cacheLocation.Equals(CacheLocationType.Local))
                {
                    switch (type)
                    {
                        case "Hit":
                            Interlocked.Increment(ref stat.LocalHit);
                            break;

                        case "Remove":
                            Interlocked.Increment(ref stat.LocalRemove);
                            break;

                        case "Set":
                            Interlocked.Increment(ref stat.LocalSet);
                            break;

                        case "Miss":
                            Interlocked.Increment(ref stat.LocalMiss);
                            break;
                    }
                }
                else
                {
                    switch (type)
                    {
                        case "Hit":
                            Interlocked.Increment(ref stat.RemoteHit);
                            break;

                        case "Remove":
                            Interlocked.Increment(ref stat.RemoteRemove);
                            break;

                        case "Set":
                            Interlocked.Increment(ref stat.RemoteSet);
                            break;

                        case "Miss":
                            Interlocked.Increment(ref stat.RemoteMiss);
                            break;
                    }
                }
            };

            AddStatistic(key, action);

            return Task.CompletedTask;
        }

        public Task DurationAsync(string action, long totalTicks, Dictionary<string, string> additionalData = null)
        {
            return Task.CompletedTask;
            //throw new NotImplementedException();
        }
        
        private void AddStatistic(string key, Action<PerformanceCounterStatistics> action)
        {
            PerformanceCounterStatistics stat;

            if (_statistics.TryGetValue(key, out stat) == false)
            {
                stat = new PerformanceCounterStatistics();
                stat = _statistics.GetOrAdd(key, stat);
            }

            action(stat);
        }

        private PerformanceCounterStatistics GetStatistic(string key)
        {
            PerformanceCounterStatistics stat;

            if (_statistics.TryGetValue(key, out stat))
            {
                return stat;
            }

            return PerformanceCounterStatistics.Default;
        }

        internal override ICacheClient CreateClient()
        {
            throw new NotSupportedException();
            //return CacheClient.Create(c =>
            //{
            //    c.LocalCache.Enable();
            //    c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
            //    c.Notifciations.UseRedis(RedisConnectionString);
            //    c.Diagnostics.UsePerformanceCounters(this);

            //    c.Policies
            //        .Add(NormalPolicy)
            //        .SetDefault(ItemPolicy.Default);
            //});
        }

        internal ICacheClient CreateLocalClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.Disable();
                c.Notifications.Disable();
                //c.Diagnostics.UsePerformanceProvider(this);

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }

        internal ICacheClient CreateRemoteRedisClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Disable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.Disable();
               // c.Diagnostics.UsePerformanceProvider(this);

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }

        public void Dispose()
        {
        }
    }

    public class PerformanceCounterStatistics
    {
        public static PerformanceCounterStatistics Default = new PerformanceCounterStatistics();

        public long LocalHit = 0L;

        public long LocalMiss = 0L;

        public long LocalSet = 0L;

        public long LocalRemove = 0L;

        public long RemoteHit = 0L;

        public long RemoteMiss = 0L;

        public long RemoteSet = 0L;

        public long RemoteRemove = 0L;
    }
}
