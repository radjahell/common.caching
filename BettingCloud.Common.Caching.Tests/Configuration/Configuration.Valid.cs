﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
using System.Collections.Generic;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class Configuration
    {
        [Fact]
        public void AddMultiplePolicies()
        {
            var policies = new List<ItemPolicy>()
            {
                ItemPolicy.Default                
            };

            var client = CacheClient.Create(c => {
                c.LocalCache.Enable();
                c.Policies.Add(policies);
            });

            Assert.NotNull(client);
        }
    }
}
