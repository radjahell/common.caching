﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Validations;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class Configuration
    {
        [Fact]
        public void LocalRemoteBothDisabledErrorMessage()
        {
            var exception = Assert.Throws<CacheConfigurationException>(() =>
            {
                CacheClient.Create(c => { });
            });

            var expectedMessage = ClientSettingsValidation.LocalRemoteBothDisabledErrorMessage;
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void RemoteNotConfiguredErrorMessage()
        {
            var exception = Assert.Throws<CacheConfigurationException>(() =>
            {
                CacheClient.Create(c => {
                    c.RemoteCache.Enable();
                });
            });

            var expectedMessage = ClientSettingsValidation.RemoteNotConfiguredErrorMessage;
            Assert.Equal(expectedMessage, exception.Message);
        }
        

        [Fact]
        public void NotificationsRequireRemoteToBeEnabledErrorMessage()
        {
            var exception = Assert.Throws<CacheConfigurationException>(() =>
            {
                CacheClient.Create(c => {
                    c.LocalCache.Enable();
                    c.Notifications.Enable();
                });
            });

            var expectedMessage = ClientSettingsValidation.NotificationsRequireRemoteToBeEnabledErrorMessage;
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void LoggerNotConfiguredErrorMessage()
        {
            var exception = Assert.Throws<CacheConfigurationException>(() =>
            {
                CacheClient.Create(c => {
                    c.LocalCache.Enable();
                    //c.Diagnostics.UseLogProvider(null);
                });
            });

            var expectedMessage = ClientSettingsValidation.LoggerNotConfiguredErrorMessage;
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void CachePoliciesDefaultNotConfiguredErrorMessage()
        {
            var exception = Assert.Throws<CacheConfigurationException>(() =>
            {
                CacheClient.Create(c => {
                    c.LocalCache.Enable();
                    c.Policies
                        .Add(ItemPolicy.Default)
                        .SetDefault(null);
                });
            });

            var expectedMessage = ClientSettingsValidation.CachePoliciesDefaultNotConfiguredErrorMessage;
            Assert.Equal(expectedMessage, exception.Message);
        }
    }
}
