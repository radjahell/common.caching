﻿using Newtonsoft.Json;
using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Tests.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public abstract class BaseTests
    {
        public BaseTests()
        {
            Config = new ConfigurationProvider();
        }

        protected IConfigurationProvider Config { get; private set; }

        public static IEnumerable<object[]> TestData()
        {
            var data = new List<object[]>();
            data.Add(new object[] { "string data" });       // String
            data.Add(new object[] { int.MaxValue });        // Int32
            data.Add(new object[] { uint.MaxValue });       // UInt32
            data.Add(new object[] { short.MaxValue });      // Int16
            data.Add(new object[] { ushort.MaxValue });     // UInt16
            data.Add(new object[] { long.MaxValue });       // Int64
            data.Add(new object[] { ulong.MaxValue });      // UInt64
            data.Add(new object[] { true });                // Boolean
            data.Add(new object[] { float.MaxValue });      // Float
            data.Add(new object[] { double.MaxValue });     // Double
            data.Add(new object[] { UserModel.Generate() });// Complex object

            return data;
        }

        internal static ItemPolicy NormalPolicy = ItemPolicy.Builder
                .LocalPolicy(TimeSpan.FromMinutes(30))
                .RemotePolicy(TimeSpan.FromMinutes(30))
                .Create("Normal");

        internal void AssertEquals(object left, object right)
        {
            var jsonLeft = JsonConvert.SerializeObject(left);
            var jsonRight = JsonConvert.SerializeObject(right);

            Assert.Equal(jsonLeft, jsonRight);
        }

        internal abstract ICacheClient CreateClient();

        internal ICacheClient Create(
            CacheLocationType cacheLocation,
            string redisConnectionString = null,
            int redisDatabaseId = 0,
            string rabbitMQHost = null,
            int? rabbitMQPort = null,
            string rabbitMQExchangeName = null,
            string rabbitMQUsername = null,
            string rabbitMQPassword = null,
            string rabbitMQVirtualHost = null,
            ItemPolicy[] policies = null)
        {
            return CacheClient.Create(c =>
            {
                if (cacheLocation.Has(CacheLocationType.Local))
                {
                    c.LocalCache.Enable();
                }

                if (cacheLocation.Has(CacheLocationType.Remote))
                {
                    c.RemoteCache.Enable();

                    if (string.IsNullOrEmpty(redisConnectionString) == false)
                    {
                        c.RemoteCache.UseRedis(redisConnectionString, redisDatabaseId);
                    }
                }

                if (rabbitMQHost != null)
                {
                    c.Notifications.UseRabbitMQ(rabbitMQHost, rabbitMQExchangeName, rabbitMQPort, rabbitMQUsername, rabbitMQPassword, rabbitMQVirtualHost);
                }

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);

                if (policies != null)
                {
                    foreach (var policy in policies)
                    {
                        c.Policies.Add(policy);
                    }
                }
            });
        }
    }
}
