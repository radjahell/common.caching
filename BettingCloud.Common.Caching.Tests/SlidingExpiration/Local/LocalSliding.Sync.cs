﻿using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration.Local
{
    public partial class LocalSliding
    {
        [Theory]
        [InlineData(TestData)]
        public void Expire<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, SlidingPolicy.Name);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            Thread.Sleep((int)SlidingPolicy.Local.Ttl.Value.TotalMilliseconds + 500);

            cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [InlineData(TestData)]
        public void Sliding<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, SlidingPolicy.Name);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
            var iterations = 5;

            for (var i = 0; i < iterations; i++)
            {
                Thread.Sleep((int)(SlidingPolicy.Local.Ttl.Value.TotalMilliseconds / iterations));

                cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                Assert.Equal(data, cachedData);
            }
        }
    }
}
