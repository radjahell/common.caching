﻿using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration.Local
{
    public partial class LocalSliding
    {
        [Theory]
        [InlineData(TestData)]
        public async Task ExpireAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, SlidingPolicy.Name);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            Thread.Sleep((int)SlidingPolicy.Local.Ttl.Value.TotalMilliseconds + 500);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [InlineData(TestData)]
        public async Task SlidingAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, SlidingPolicy.Name);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
            var iterations = 5;

            for (var i = 0; i < iterations; i++)
            {
                Thread.Sleep((int)(SlidingPolicy.Local.Ttl.Value.TotalMilliseconds / iterations));

                cachedData = await client.GetAsync<T>(key);

                Assert.NotNull(cachedData);
                Assert.Equal(data, cachedData);
            }
        }
    }
}
