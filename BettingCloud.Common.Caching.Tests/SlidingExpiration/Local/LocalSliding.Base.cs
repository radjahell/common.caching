﻿using BettingCloud.Common.Caching.Client;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration.Local
{
    public partial class LocalSliding : BaseSlidingTests
    {
        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.Disable();
                c.Notifications.Disable();

                c.Policies
                    .Add(SlidingPolicy)
                    .SetDefault(SlidingPolicy);
            });
        }
    }
}
