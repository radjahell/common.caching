﻿using System;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration
{
    public abstract class BaseSlidingTests : BaseTests
    {
        protected new const string TestData = "TEST DATA";

        internal static ItemPolicy SlidingPolicy = ItemPolicy.Builder
                .LocalPolicy(TimeSpan.FromSeconds(20), true)
                .RemotePolicy(TimeSpan.FromSeconds(20), true)
                .Create("Sliding");

        internal static ItemPolicy SlidingLongPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromMinutes(20), true)
            .RemotePolicy(TimeSpan.FromMinutes(20), true)
            .Create("SlidingLong");

        public string RedisConnectionString => Config.Get("RedisConnectionString");

        // ReSharper disable once PossibleInvalidOperationException
        public int RedisDatabaseId => Config.GetInt32("RedisDatabaseId").Value;
    }
}
