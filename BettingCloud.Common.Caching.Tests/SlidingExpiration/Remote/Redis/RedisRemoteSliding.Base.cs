﻿using BettingCloud.Common.Caching.Client;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration.Remote.Redis
{
    public partial class RedisRemoteSliding : BaseSlidingTests
    {
        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Disable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.Disable();

                c.Policies
                    .Add(SlidingPolicy)
                    .Add(SlidingLongPolicy)
                    .SetDefault(SlidingPolicy);
            });
        }
    }
}
