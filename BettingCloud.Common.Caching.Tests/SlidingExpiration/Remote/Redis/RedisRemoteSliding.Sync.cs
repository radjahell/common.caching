﻿using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.SlidingExpiration.Remote.Redis
{
    public partial class RedisRemoteSliding
    {
        [Theory]
        [InlineData(TestData)]
        public void Expire<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => data;
            var client = CreateClient();
            var cachedData = client.AddOrGet(key, fetch, SlidingPolicy.Name);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            var waitTime = SlidingPolicy.Local.Ttl.Value.Add(TimeSpan.FromSeconds(10));

            Thread.Sleep(waitTime);

            cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [InlineData(TestData)]
        public void Sliding<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, SlidingPolicy.Name);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);
            var iterations = 5;

            for (var i = 0; i < iterations; i++)
            {
                Thread.Sleep((int)(SlidingPolicy.Local.Ttl.Value.TotalMilliseconds / iterations));

                cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                Assert.Equal(data, cachedData);
            }
        }

        //[Theory]
        //[InlineData(TestData)]
        //public void SlidingTest2<T>(T data)
        //{
        //    var key = Guid.NewGuid().ToString();
        //    var client = CreateClient();

        //    T Fetch()
        //    {
        //        return data;
        //    }

        //    client.AddOrGet(key, Fetch, SlidingLongPolicy.Name);

        //    var cachedData = client.Get<T>(key);

        //    Assert.NotNull(cachedData);
        //    Assert.Equal(data, cachedData);
        //    var iterations = 5;

        //    for (var i = 0; i < iterations; i++)
        //    {
        //        Thread.Sleep((int)(SlidingPolicy.Local.Ttl.Value.TotalMilliseconds / iterations));

        //        cachedData = client.AddOrGet(key, Fetch, SlidingLongPolicy.Name);

        //        Assert.NotNull(cachedData);
        //        Assert.Equal(data, cachedData);
        //    }
        //}
    }
}
