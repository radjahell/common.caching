﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Tests.Remote.Redis
{
    public partial class RedisRemoteCache : BaseTests
    {
        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        private ICacheClient _redisClient;

        internal override ICacheClient CreateClient()
        {
            if (_redisClient != null)
                return _redisClient;

            _redisClient = CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.Disable();

                c.Policies
                    .Add(NormalPolicy)
                    .SetDefault(ItemPolicy.Default);
            });

            return _redisClient;
        }
    }
}
