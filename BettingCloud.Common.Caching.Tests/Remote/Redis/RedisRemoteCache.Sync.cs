﻿using System;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Remote.Redis
{
    public partial class RedisRemoteCache
    {
        #region Set<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformSet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                client.Set(key, data, ItemPolicy.Default);

                var cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformSetWithNormalItemPolicy<T>(T data)
        {
            performSet(data, NormalPolicy.Name);
        }

        private void performSet<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                client.Set(key, data, policyName);

                var cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        #endregion

        #region AddOrGet<T>

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGet<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => { return data; };

            using (var client = CreateClient())
            {
                var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

                Assert.NotNull(cachedData);
                Assert.Equal(data, cachedData);

                cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformAddOrGetWithDefaultValue<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => { return default(T); };

            using (var client = CreateClient())
            {
                var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

                Assert.Equal(default(T), cachedData);

                cachedData = client.Get<T>(key);

                Assert.Equal(default(T), cachedData);
            }
        }

        [Fact]
        public void PerformAddOrGetWithNullValue()
        {
            var key = Guid.NewGuid().ToString();
            Func<string> fetch = () => { return null; };

            using (var client = CreateClient())
            {
                var cachedData = client.AddOrGet(key, fetch, ItemPolicy.Default);

                Assert.Null(cachedData);

                cachedData = client.Get<string>(key);

                Assert.Null(cachedData);
            }
        }

        public void performAddOrGetWithItemPolicy<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => { return data; };

            using (var client = CreateClient())
            {
                var cachedData = client.AddOrGet(key, fetch, policyName);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);

                cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformInvalidate<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                client.Set(key, data, ItemPolicy.Default);

                var cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);

                client.Invalidate(key);

                cachedData = client.Get<T>(key);

                Assert.True(cachedData.IsNullOrDefault());
            }
        }

        #endregion

        #region Remove

        [Theory]
        [MemberData(nameof(TestData))]
        public void PerformRemove<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                client.Set(key, data, ItemPolicy.Default);

                var cachedData = client.Get<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);

                client.Remove(key);

                cachedData = client.Get<T>(key);

                Assert.True(cachedData.IsNullOrDefault());
            }
        }

        #endregion
    }
}
