﻿using System;
using System.Threading.Tasks;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Remote.Redis
{
    public partial class RedisRemoteCache
    {
        #region Set<T>

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public async Task PerformSetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            var client = CreateClient();

            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public Task PerformSetWithNormalItemPolicyAsync<T>(T data)
        {
            return performSetAsync(data, BaseTests.NormalPolicy.Name);
        }

        private async Task performSetAsync<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();

            using (var client = CreateClient())
            {
                await client.SetAsync(key, data, policyName);

                var cachedData = await client.GetAsync<T>(key);

                Assert.NotNull(cachedData);
                AssertEquals(data, cachedData);
            }
        }

        #endregion

        #region AddOrGet<T>

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public async Task PerformAddOrGetAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => {
                return data;
            };

            var client = CreateClient();
            var cachedData = await client.AddOrGetAsync(key, fetch, ItemPolicy.Default);

            Assert.NotNull(cachedData);
            Assert.Equal(data, cachedData);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public Task PerformAddOrGetWithNormalItemPolicyAsync<T>(T data)
        {
            return performAddOrGetWithItemPolicyAsync(data, BaseTests.NormalPolicy.Name);
        }

        public async Task performAddOrGetWithItemPolicyAsync<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => { return data; };

            var client = CreateClient();
            var cachedData = await client.AddOrGetAsync(key, fetch, policyName);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        #endregion

        #region Invalidate

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public async Task PerformInvalidateAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            var client = CreateClient();
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            await client.InvalidateAsync(key);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(GenericExtensions.IsNullOrDefault<T>(cachedData));
        }

        #endregion

        #region Remove

        [Theory]
        [MemberData(nameof(BaseTests.TestData))]
        public async Task PerformRemoveAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();

            var client = CreateClient();
            await client.SetAsync(key, data, ItemPolicy.Default);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            await client.RemoveAsync(key);

            cachedData = await client.GetAsync<T>(key);

            Assert.True(GenericExtensions.IsNullOrDefault<T>(cachedData));
        }

        #endregion
    }
}
