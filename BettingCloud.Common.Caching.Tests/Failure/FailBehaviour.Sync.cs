﻿using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using System;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Failure
{
    public partial class FailBehaviour
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public void Fetch<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var counter = 0;
            Func<T> fetch = () =>
            {
                counter++;
                return data;
            };

            var client = CreateClient();
            var cachedData = client.AddOrGet(key, fetch, AlwaysFetchItemPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 1);

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 2);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void FetchTwice<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var counter = 0;
            Func<T> fetch = () =>
            {
                counter++;
                return data;
            };

            var client = CreateClient();
            var cachedData = client.AddOrGet(key, fetch, AlwaysFetchItemPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 1);

            cachedData = client.AddOrGet(key, fetch, AlwaysFetchItemPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 2);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void FetchDefault<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, AlwaysFetchItemPolicy.Name);

            var cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void UseMemory<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, UseMemoryItemPolicy.Name);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void ThrowException<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            client.Set(key, data, ThrowItemPolicy.Name);

            Assert.Throws<CacheFailureException>(() =>
            {
                var cachedData = client.Get<T>(key);
            });
        }
    }
}
