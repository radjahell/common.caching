﻿using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests.Failure
{
    public partial class FailBehaviour
    {
        [Theory]
        [MemberData(nameof(TestData))]
        public async Task FetchAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var counter = 0;
            Func<T> fetch = () =>
            {
                counter++;
                return data;
            };

            var client = CreateClient();
            var cachedData = await client.AddOrGetAsync(key, fetch, AlwaysFetchItemPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 1);

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
            Assert.True(counter == 2);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task FetchDefaultAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, AlwaysFetchItemPolicy.Name);

            var cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task UseMemoryAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, UseMemoryItemPolicy.Name);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public async Task ThrowExceptionAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();
            await client.SetAsync(key, data, ThrowItemPolicy.Name);

            await Assert.ThrowsAsync<CacheFailureException>(async () =>
            {
                 await client.GetAsync<T>(key);
            });
        }
    }
}
