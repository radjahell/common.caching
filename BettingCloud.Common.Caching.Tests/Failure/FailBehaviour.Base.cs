﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.DotNetCore.Tests.Mocks;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using System;

namespace BettingCloud.Common.Caching.Tests.Failure
{
    public partial class FailBehaviour : BaseTests
    {
       // private readonly Logger _logger = new Logger();

        private ItemPolicy ThrowItemPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromMinutes(5))
            .RemotePolicy(TimeSpan.FromMinutes(5))
            .Create("ThrowItemPolicy");

        private ItemPolicy AlwaysFetchItemPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromMinutes(5))
            .RemotePolicy(TimeSpan.FromMinutes(5))
            .Create("AlwaysFetchItemPolicy");

        private ItemPolicy UseMemoryItemPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromMinutes(5))
            .RemotePolicy(TimeSpan.FromMinutes(5))
            .Create("UseMemoryItemPolicy");

        internal override ICacheClient CreateClient()
        {
            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.UseRedis("wrong connection string", 0);
               // c.Diagnostics.UseLogProvider(_logger);
                c.OnFailure
                    .Use(FailureBehaviour.UseFetchOrDefault, AlwaysFetchItemPolicy.Name)
                    .Use(FailureBehaviour.ThrowException, ThrowItemPolicy.Name)
                    .Use(FailureBehaviour.UseMemory, UseMemoryItemPolicy.Name)
                    .SetDefault(FailureBehaviour.NotSet);

                c.Policies
                    .Add(ThrowItemPolicy)
                    .Add(AlwaysFetchItemPolicy)
                    .Add(UseMemoryItemPolicy)
                    .SetDefault(ItemPolicy.Default);
            });
        }
    }
}
