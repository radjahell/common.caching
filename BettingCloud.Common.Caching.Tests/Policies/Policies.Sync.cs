﻿using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class Policies
    {
        [Theory]
        [InlineData("test data")]
        public void ExpireBothIn5Seconds<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => data;
            var client = CreateClient();

            var cachedData = client.AddOrGet(key, fetch, ExpireIn5SecondsPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            Thread.Sleep(TimeSpan.FromSeconds(30));

            cachedData = client.Get<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [InlineData("test data")]
        public void ExpireLocalIn5Seconds<T>(T data)
        {
            ExpireTest(data, ExpireIn5SecondsLocalOnlyPolicy.Name);
        }

        [Theory]
        [InlineData("test data")]
        public void ExpireRemoteIn5Seconds<T>(T data)
        {
            ExpireTest(data, ExpireIn5SecondRemoteOnlyPolicy.Name);
        }

        private void ExpireTest<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();

            client.Set(key, data, policyName);

            var cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            Thread.Sleep(TimeSpan.FromSeconds(1));

            cachedData = client.Get<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }
    }
}
