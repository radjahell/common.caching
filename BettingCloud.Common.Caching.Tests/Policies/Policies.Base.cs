﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
using System;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class Policies : BaseTests, IDisposable
    {
        internal static ItemPolicy ExpireIn5SecondsPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromSeconds(5))
            .RemotePolicy(TimeSpan.FromSeconds(5))
            .Create("ExpireIn5SecondsPolicy");

        internal static ItemPolicy ExpireIn5SecondsLocalOnlyPolicy = ItemPolicy.Builder
            .LocalPolicy(TimeSpan.FromSeconds(5))
            .Create("ExpireIn5SecondsLocalOnlyPolicy");

        internal static ItemPolicy ExpireIn5SecondRemoteOnlyPolicy = ItemPolicy.Builder
            .RemotePolicy(TimeSpan.FromSeconds(5))
            .Create("ExpireIn5SecondsRemoteOnlyPolicy");
        
        public string RabbitMQHost
        {
            get
            {
                return Config.Get("RabbitMQHost");
            }
        }

        public string RabbitMQExchange
        {
            get
            {
                return Config.Get("RabbitMQExchange");
            }
        }
        
        public string RabbitMQVirtualHost
        {
            get
            {
                return Config.Get("RabbitMQVirtualHost");
            }
        }

        public string RabbitMQUsername
        {
            get
            {
                return Config.Get("RabbitMQUsername");
            }
        }

        public string RabbitMQPassword
        {
            get
            {
                return Config.Get("RabbitMQPassword");
            }
        }

        public int? RabbitMQPort
        {
            get
            {
                return Config.GetInt32("RabbitMQPort");
            }
        }

        public string RedisConnectionString
        {
            get
            {
                return Config.Get("RedisConnectionString");
            }
        }

        public int RedisDatabaseId
        {
            get
            {
                return Config.GetInt32("RedisDatabaseId").Value;
            }
        }

        internal override ICacheClient CreateClient()
        {
            var policies = new[]
            {
                ExpireIn5SecondsPolicy,
                ExpireIn5SecondsLocalOnlyPolicy,
                ExpireIn5SecondRemoteOnlyPolicy
            };

            return CacheClient.Create(c =>
            {
                c.LocalCache.Enable();
                c.RemoteCache.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Notifications.UseRedis(RedisConnectionString, RedisDatabaseId);
                c.Policies
                    .Add(policies)
                    .SetDefault(ItemPolicy.Default);
            });
        }

        public void Dispose()
        {
        }
    }
}
