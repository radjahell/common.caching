﻿using BettingCloud.Common.Caching.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BettingCloud.Common.Caching.Tests
{
    public partial class Policies
    {
        [Theory]
        [InlineData("test data")]
        public async Task ExpireBothIn1SecondAsync<T>(T data)
        {
            var key = Guid.NewGuid().ToString();
            Func<T> fetch = () => data;
            var client = CreateClient();

            var cachedData = await client.AddOrGetAsync(key, fetch, ExpireIn5SecondsPolicy.Name);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            Thread.Sleep(TimeSpan.FromSeconds(20));

            cachedData = await client.GetAsync<T>(key);

            Assert.True(cachedData.IsNullOrDefault());
        }

        [Theory]
        [InlineData("test data")]
        public Task ExpireLocalIn1SecondAsync<T>(T data)
        {
            return ExpireTestAsync(data, ExpireIn5SecondsLocalOnlyPolicy.Name);
        }

        [Theory]
        [InlineData("test data")]
        public Task ExpireRemoteIn1SecondAsync<T>(T data)
        {
            return ExpireTestAsync(data, ExpireIn5SecondRemoteOnlyPolicy.Name);
        }

        private async Task ExpireTestAsync<T>(T data, string policyName)
        {
            var key = Guid.NewGuid().ToString();
            var client = CreateClient();

            client.Set(key, data, policyName);

            var cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);

            Thread.Sleep(TimeSpan.FromSeconds(1));

            cachedData = await client.GetAsync<T>(key);

            Assert.NotNull(cachedData);
            AssertEquals(data, cachedData);
        }
    }
}
