﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching
{
    public class CacheItem<T>
    {

        public CacheItem(string key, T value)
        {
            Value = value;
            Key = key;
        }

        public T Value { get; set; }
        public string Key { get; set; } 
    }
}
