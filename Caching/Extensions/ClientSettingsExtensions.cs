﻿using Caching.Enums;
using Caching.Interfaces;

namespace Caching.Extensions
{
    public static class ClientSettingsExtensions
    { 
        internal static CacheType LocationType(this ISettings settings)
        {
            var locationType = CacheType.None;

            if (settings != null)
            {
                if (settings.LocalCacheEnabled)
                {
                    locationType = locationType | CacheType.Memory;
                } 
            }

            return locationType;
        }
    }
}
