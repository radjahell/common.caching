﻿using Caching.Enums;
using Caching.Extensions;
using Caching.Interfaces;
using System;

namespace Caching
{
    public class CacheClient : ICacheClient
    {

        private IContext _context;

        public CacheClient()
        {
        }

        public CacheClient(IContext context)
        {
            _context = context;
        }


        public static ICacheClient Create(Action<IClientBuilder> configure = null)
        {
            //Create settings
            var settings = new Settings();
            //Create builders
            var clientBuilder = new ClientBuilder(settings);

            configure?.Invoke(clientBuilder);

            //Create context 
            var context = new Context(settings);
            //Create client
            var client = new CacheClient(context);
            context.Client = client;

            return client;
        } 

        public CacheItem<T> AddOrGet<T>(string key, Func<T> fetch, CachePolicy cachePolicy)
        {
            var cacheType = _context.settings.LocationType();
            if (cacheType.HasFlag(CacheType.Memory))
            {
                if (!_context.MemoryCache.Contains(key)) 
                    _context.MemoryCache.Set<T>(key, fetch.Invoke(), cachePolicy); 
            } 
          
            return _context.MemoryCache.Get<T>(key); 
        } 

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key)
        {
            throw new NotImplementedException();
        }

        public void Invalidate(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, T item, string policyName)
        {
            throw new NotImplementedException();
        }
    }
}
