﻿using Caching.Interfaces;
using System;

namespace Caching
{
    public interface ICacheClient : IDisposable
    {  
        T Get<T>(string key);

        CacheItem<T> AddOrGet<T>(string key, Func<T> fetch, CachePolicy cachePolicy);

        void Set<T>(string key, T item, string policyName);  

        void Invalidate(string key); 

        void Remove(string key);

    //    ICacheClient Create(Action<IClientBuilder> configure = null)
    }
}
