﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching.Interfaces
{
    public interface ILocalCacheProvider
    {
        bool Contains(string key);

        void Flush();

        void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync);

         

        void Remove(string key);
    }
}
