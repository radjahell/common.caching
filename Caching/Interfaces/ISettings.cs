﻿namespace Caching.Interfaces
{
    public interface ISettings
    {
        bool LocalCacheEnabled { get; set; }
    }
}