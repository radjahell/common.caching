﻿using System;
using System.Threading.Tasks;

namespace Caching.Interfaces
{
    public interface IMemoryCacheProvider
    {
        bool Contains(string key);
        void Dispose();
        void Flush();
        void Remove(string key);
        void Set<T>(string key, T value, CachePolicy policy);
        CacheItem<T> Get<T>(string key);
    }
}