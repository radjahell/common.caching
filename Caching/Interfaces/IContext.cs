﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching.Interfaces
{
    public interface IContext
    {
        bool LocalCacheEnabled { get; set; }
        IMemoryCacheProvider MemoryCache { get; set; }
        ISettings settings { get; }
    }
}
