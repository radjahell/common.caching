﻿namespace Caching.Interfaces
{
    public interface IMemoryCacheBuilder
    { 
        IMemoryCacheBuilder Enable(); 

        IMemoryCacheBuilder Disable();
    }
}
