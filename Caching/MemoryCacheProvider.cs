﻿using Caching.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Caching
{
    public class MemoryCacheProvider : IMemoryCacheProvider
    {
        private IMemoryCache _cache;
        private static readonly object SyncLock = new object();

        public MemoryCacheProvider(IContext context) 
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public bool Contains(string key)
        {
            return _cache.TryGetValue(key, out object _);
        }

        public void Dispose()
        {
            _cache.Dispose();
        }

        public void Flush()
        {
            lock (SyncLock)
            {
                var currentCache = _cache;
                _cache = new MemoryCache(new MemoryCacheOptions());
                currentCache.Dispose();
            }
        } 

        public CacheItem<T> Get<T>(string key)
        { 
            var value = _cache.Get(key);
            return (CacheItem<T>)value; 
        } 

        public void Set<T>(string key, T value, CachePolicy policy)
        {
            var memoryCacheEntryOptions = new MemoryCacheEntryOptions
            {
                SlidingExpiration = policy.Ttl
            };

            var cacheItem = new CacheItem<T>(key,value);
            _cache.Set(key, cacheItem, memoryCacheEntryOptions);
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }
    }
}

