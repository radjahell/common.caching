﻿using Caching.Interfaces;

namespace Caching
{
    public class Context : IContext
    {
        public Context(ISettings settings)
        {
            this.settings = settings;

            if (settings.LocalCacheEnabled)
            {
                MemoryCache = new MemoryCacheProvider(this);
            }
            else 
            { 
                MemoryCache = new NullMemoryCacheProvider();
            }
            
        }

        public ISettings settings { get; }
        public IMemoryCacheBuilder MemoryCacheBuilder { get; }
        public ICacheClient Client { get; internal set; }
        public IMemoryCacheProvider MemoryCache {get; set;}
        private bool _localCacheEnabled { get; set; }

        public bool LocalCacheEnabled
        {
            get => _localCacheEnabled;
            set => _localCacheEnabled = value;
        }
    }
}
