﻿using Caching.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching
{
    public class Settings : ISettings
    {
        public Settings()
        {  
        }

        public bool LocalCacheEnabled { get; set; }  
    }
}
