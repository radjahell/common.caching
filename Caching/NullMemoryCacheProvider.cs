﻿using System;
using System.Threading.Tasks;
using Caching.Interfaces;

namespace Caching
{
    internal class NullMemoryCacheProvider : IMemoryCacheProvider
    {
        public bool Contains(string key)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Flush()
        {
            throw new NotImplementedException();
        }

        public CacheItem<T> Get<T>(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync)
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, CacheItem<T> value)
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, T value)
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, T value, CachePolicy policy)
        {
            throw new NotImplementedException();
        }
    }
}