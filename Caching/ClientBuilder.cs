﻿using Caching.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching
{
    public class ClientBuilder : IClientBuilder
    {
        public IMemoryCacheBuilder MemoryCache { get; set; } 

        private readonly ISettings _settings;

        internal ClientBuilder(ISettings context)
        { 
            _settings = context; 
            MemoryCache = new MemoryCacheBuilder(_settings); 
        }
    }
}
