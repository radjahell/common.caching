﻿using Caching.Interfaces;

namespace Caching
{
    public class MemoryCacheBuilder : IMemoryCacheBuilder
    {
        private readonly ISettings _settings;

        internal MemoryCacheBuilder(ISettings settings)
        {
            _settings = settings;
        } 

        public IMemoryCacheBuilder Enable()
        {
            _settings.LocalCacheEnabled = true;
            return this;
        } 

        public IMemoryCacheBuilder Disable()
        {
            _settings.LocalCacheEnabled = false;
            return this;
        }
    }
}
