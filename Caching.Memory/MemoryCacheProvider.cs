﻿using Caching.Abstracts;
using System;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;

namespace Caching.Memory
{
    public class MemoryCacheProvider : ICache
    {
        private MemoryCache _cache; 
       
        private static readonly object SyncLock = new object();

        public MemoryCacheProvider()
        {
            _cache = new MemoryCache("1");
        } 

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        public void Flush()
        {
            lock (SyncLock)
            {
                var currentCache = _cache;
                _cache = new MemoryCache("1");
                currentCache.Dispose();
            }
        }

        public void Set<T>(string key, T value, ItemPolicy itemPolicy)
        {
            var cachePolicy = new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default
            };

            if (itemPolicy.Ttl.HasValue)
            {
                if (itemPolicy.IsSliding)
                {
                    cachePolicy.SlidingExpiration = itemPolicy.Ttl.Value;
                }
                else
                {
                    var ttlInTicks = itemPolicy.Ttl.Value.Ticks;
                    cachePolicy.AbsoluteExpiration = DateTime.UtcNow.AddTicks(ttlInTicks);
                }
            } 

            var cacheItem = new CacheItem<T>(key, value, DateTime.Now, itemPolicy.Ttl ,itemPolicy.IsSliding);
            _cache.Set(key, cacheItem, cachePolicy);
        }

        

        public async  Task<bool> ContainsAsync(string key)
        {
            return _cache[key] != null;
        }

        public async Task<CacheItem<T>> GetAsync<T>(string key)
        {
            
                var item = _cache.Get(key);
                return (CacheItem<T>)item; 
        }
    }
}
