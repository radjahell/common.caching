﻿using System;

namespace BettingCloud.Common.Caching.Policy
{
    /// <summary>
    /// Defines Cache Item Settings.
    /// </summary>
    public class CachePolicy : ICachePolicy
    {
        /// <summary>
        /// Default CachePolicy with no expiry.
        /// </summary>
        public static readonly CachePolicy Default = new CachePolicy();

        /// <summary>
        /// The time-to-live for the cached item.
        /// </summary>
        public TimeSpan? Ttl { get; internal set; }

        /// <summary>
        /// Flags the cache item to be expired using sliding expiration.
        /// </summary>
        public bool IsSliding { get; internal set; }
    }
}
