﻿using EnsureThat;
using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Policy
{
    internal class ItemPolicyRepository : IItemPolicyRepository
    {
        public ItemPolicyRepository()
        {
            _policies = new Dictionary<string, ItemPolicy>();
            DefaultPolicy = ItemPolicy.Default;
        }

        private readonly Dictionary<string, ItemPolicy> _policies;

        public ItemPolicy DefaultPolicy { get; set; }

        public int Count => _policies.Count;

        public void Add(string name, ItemPolicy policy)
        {
            Ensure.That(name, Constants.Validation.Keys.Name).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            _policies[name] = policy;
        }

        public bool Exists(string policyName)
        {
            if (string.IsNullOrWhiteSpace(policyName))
            {
                return false;
            }

            return _policies.TryGetValue(policyName, out var policy);
        }

        public ItemPolicy Get(string policyName)
        {
            if (string.IsNullOrWhiteSpace(policyName))
            {
                return DefaultPolicy;
            }

            if (_policies.TryGetValue(policyName, out var policy))
                return policy;

            return DefaultPolicy ?? ItemPolicy.Default;
        }
    }
}
