﻿using System;

namespace BettingCloud.Common.Caching.Policy
{
    /// <summary>
    /// Represents cache item settings for each providers.
    /// </summary>
    public class ItemPolicy
    {
        /// <summary>
        /// The default ItemPolicy. Means the item is never cached.
        /// </summary>
        public static readonly ItemPolicy Default = new ItemPolicy { Name = "(default)" };

        internal ItemPolicy()
        {
            Name = "(notset)";
            Local = new CachePolicy();
            Remote = new CachePolicy();
        }

        /// <summary>
        /// Name of the policy.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// The local cache policy.
        /// </summary>
        public ICachePolicy Local { get; internal set; }

        /// <summary>
        /// The remote cache policy.
        /// </summary>
        public ICachePolicy Remote { get; internal set; }

        /// <summary>
        /// A Fluent interface to create a new item policy.
        /// </summary>
        public static IItemPolicyBuilder Builder
        {
            get
            {
                return new ItemPolicyBuilder();
            }
        }
    }
}
