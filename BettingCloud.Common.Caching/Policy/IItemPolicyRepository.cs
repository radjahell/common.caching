﻿namespace BettingCloud.Common.Caching.Policy
{
    internal interface IItemPolicyRepository
    {
        int Count { get; }

        ItemPolicy DefaultPolicy { get; set; }

        bool Exists(string policyName);

        ItemPolicy Get(string policyName);

        void Add(string policyName, ItemPolicy policy);
    }
}
