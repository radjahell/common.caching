﻿using System;

namespace BettingCloud.Common.Caching.Policy
{
    /// <summary>
    /// Fluent ItemPolicy configurator. Required to create cache item policies.
    /// </summary>
    public interface IItemPolicyBuilder
    {
        /// <summary>
        /// Configuration settings for the local cache.
        /// </summary>
        /// <param name="ttlInSecs">TTl in seconds</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder LocalPolicy(int ttlInSecs);

        /// <summary>
        /// Configuration settings for the local cache.
        /// </summary>
        /// <param name="ttlInSecs">TTl in seconds</param>
        /// <param name="isSliding">Indicates whether the cache item should use absolute or sliding expiration.</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder LocalPolicy(int ttlInSecs, bool isSliding);

        /// <summary>
        /// Configuration settings for the local cache.
        /// </summary>
        /// <param name="ttl">TTl in seconds</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder LocalPolicy(TimeSpan ttl);

        /// <summary>
        /// Configuration settings for the local cache.
        /// </summary>
        /// <param name="ttl">TTl in seconds</param>
        /// <param name="isSliding">Indicates whether the cache item should use absolute or sliding expiration.</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder LocalPolicy(TimeSpan ttl, bool isSliding);

        /// <summary>
        /// Configuration settings for the remote cache.
        /// </summary>
        /// <param name="ttl">TTl in seconds</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder RemotePolicy(TimeSpan ttl);

        /// <summary>
        /// Configuration settings for the remote cache.
        /// </summary>
        /// <param name="ttl">TTl in seconds</param>
        /// <param name="isSliding">Indicates whether the cache item should use absolute or sliding expiration.</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder RemotePolicy(TimeSpan ttl, bool isSliding);

        /// <summary>
        /// Configuration settings for the remote cache.
        /// </summary>
        /// <param name="ttlInSecs">TTl in seconds</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder RemotePolicy(int ttlInSecs);

        /// <summary>
        /// Configuration settings for the remote cache.
        /// </summary>
        /// <param name="ttlInSecs">TTl in seconds</param>
        /// <param name="isSliding">Indicates whether the cache item should use absolute or sliding expiration.</param>
        /// <returns>Fluent interface to configure the policy.</returns>
        IItemPolicyBuilder RemotePolicy(int ttlInSecs, bool isSliding);


        /// <summary>
        /// Creates the policy based on the provided configuration.
        /// </summary>
        /// <returns>An <see cref="ItemPolicy"/> instance.</returns>
        ItemPolicy Create(string name);
    }
}
