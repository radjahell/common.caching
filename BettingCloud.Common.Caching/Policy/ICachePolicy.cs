﻿using System;

namespace BettingCloud.Common.Caching.Policy
{
    /// <summary>
    /// Defines Cache Item Settings.
    /// </summary>
    public interface ICachePolicy
    {
        /// <summary>
        /// The time-to-live for the cached item.
        /// </summary>
        TimeSpan? Ttl { get; }

        /// <summary>
        /// Flags the cache item to be expired using sliding expiration.
        /// </summary>
        bool IsSliding { get; }
    }
}
