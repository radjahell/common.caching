﻿using EnsureThat;
using System;

namespace BettingCloud.Common.Caching.Policy
{
    internal class ItemPolicyBuilder : IItemPolicyBuilder
    {
        private readonly ItemPolicy _policy;

        public ItemPolicyBuilder()
        {
            _policy = new ItemPolicy();
        }

        public IItemPolicyBuilder LocalPolicy(TimeSpan ttl)
        {
            _policy.Local = new CachePolicy
            {
                Ttl = ttl
            };

            return this;
        }

        public IItemPolicyBuilder LocalPolicy(TimeSpan ttl, bool isSliding)
        {
            _policy.Local = new CachePolicy
            {
                Ttl = ttl,
                IsSliding = isSliding
            };

            return this;
        }

        public IItemPolicyBuilder LocalPolicy(int ttlInSecs)
        {
            _policy.Local = new CachePolicy
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs)
            };

            return this;
        }

        public IItemPolicyBuilder LocalPolicy(int ttlInSecs, bool isSliding)
        {
            _policy.Local = new CachePolicy
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs),
                IsSliding = isSliding
            };

            return this;
        }

        public IItemPolicyBuilder RemotePolicy(TimeSpan ttl)
        {
            _policy.Remote = new CachePolicy
            {
                Ttl = ttl
            };

            return this;
        }

        public IItemPolicyBuilder RemotePolicy(TimeSpan ttl, bool isSliding)
        {
            _policy.Remote = new CachePolicy
            {
                Ttl = ttl,
                IsSliding = isSliding
            };

            return this;
        }

        public IItemPolicyBuilder RemotePolicy(int ttlInSecs)
        {
            _policy.Remote = new CachePolicy
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs)
            };

            return this;
        }

        public IItemPolicyBuilder RemotePolicy(int ttlInSecs, bool isSliding)
        {
            _policy.Remote = new CachePolicy
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs),
                IsSliding = isSliding
            };

            return this;
        }

        public ItemPolicy Create(string name)
        {
            Ensure.That(name, Constants.Validation.Keys.Name).IsNotNullOrEmpty();

            _policy.Name = name;
            return _policy;
        }
    }
}
