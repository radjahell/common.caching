﻿using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Policy
{
    /// <summary>
    /// Used to configure item policies globally.
    /// </summary>
    public interface IPolicyConfigurationProvider
    {
        /// <summary>
        /// Should return the list of preconfigured item policies.
        /// </summary>
        /// <returns>An enumerable of KeyValuePair . Key = cache item key, Value = item policy.</returns>
        IEnumerable<ItemPolicy> GetPolicies();
    }
}
