﻿using System;

namespace BettingCloud.Common.Caching.Enumerations
{
    [Flags]
    internal enum NotificationType
    {
        None = 0,
        RabbitMq = 1,
        Redis = 2
    }
}
