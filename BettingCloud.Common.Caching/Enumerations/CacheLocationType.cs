﻿using System;

namespace BettingCloud.Common.Caching.Enumerations
{
    /// <summary>
    /// Cache location type.
    /// </summary>
    [Flags]
    public enum CacheLocationType
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Local (in-process)
        /// </summary>
        Local = 1,

        /// <summary>
        /// Remote
        /// </summary>
        Remote = 2,

        /// <summary>
        /// Both (local+remote)
        /// </summary>
        Both = Local | Remote
    }
}
