﻿namespace BettingCloud.Common.Caching.Enumerations
{
    public enum FailureBehaviour
    {
        NotSet = 0,
        ThrowException = 1,
        UseMemory = 2,
        UseFetchOrDefault = 4
    }
}
