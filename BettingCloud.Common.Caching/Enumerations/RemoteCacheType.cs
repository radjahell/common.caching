﻿namespace BettingCloud.Common.Caching.Enumerations
{
    internal enum RemoteCacheType
    {
        None = 0,
        Redis = 2
    }
}
