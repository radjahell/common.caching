﻿namespace BettingCloud.Common.Caching.Enumerations
{
    internal enum RemoteCacheSetResult
    {
        None = 0,
        Failed = 1,
        Added = 2,
        Updated = 4
    }
}
