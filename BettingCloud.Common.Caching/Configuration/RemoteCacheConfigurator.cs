﻿using BettingCloud.Common.Caching.Enumerations;

namespace BettingCloud.Common.Caching.Configuration
{
    internal class RemoteCacheConfigurator : IRemoteCacheConfigurator
    {
        private readonly IClientSettings _settings;

        internal RemoteCacheConfigurator(IClientSettings settings)
        {
            _settings = settings;
            ClearRemoteSettings();
        }

        /// <summary>
        /// Enables the remote cache.
        /// </summary>
        public IRemoteCacheConfigurator Enable()
        {
            _settings.RemoteCacheEnabled = true;
            return this;
        }

        /// <summary>
        /// Disables the remote cache.
        /// </summary>
        public IRemoteCacheConfigurator Disable()
        {
            _settings.RemoteCacheEnabled = false;
            return this;
        }

        /// <summary>
        /// Configure the remote cache to use Redis.
        /// </summary>
        public IRemoteCacheConfigurator UseRedis(string connectionString, int databaseId)
        {
            ClearRemoteSettings();

            _settings.RemoteCacheEnabled = true;
            _settings.RemoteCacheType = RemoteCacheType.Redis;
            _settings.RemoteRedisConnectionString = connectionString;
            _settings.RemoteRedisDatabaseId = databaseId;

            return this;
        }

        private void ClearRemoteSettings()
        {
            _settings.RemoteCacheEnabled = false;
            _settings.RemoteCacheType = RemoteCacheType.None;
            _settings.RemoteRedisConnectionString = null;
            _settings.RemoteRedisDatabaseId = null;
        }
    }
}
