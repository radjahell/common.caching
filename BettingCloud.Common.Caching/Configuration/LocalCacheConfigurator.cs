﻿namespace BettingCloud.Common.Caching.Configuration
{
    internal class LocalCacheConfigurator : ILocalCacheConfigurator
    {
        private readonly IClientSettings _settings;

        internal LocalCacheConfigurator(IClientSettings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Enables the local in-memory cache.
        /// </summary>
        public ILocalCacheConfigurator Enable()
        {
            _settings.LocalCacheEnabled = true;
            return this;
        }

        /// <summary>
        /// Disables the local in-memory cache.
        /// </summary>
        public ILocalCacheConfigurator Disable()
        {
            _settings.LocalCacheEnabled = false;
            return this;
        }
    }
}
