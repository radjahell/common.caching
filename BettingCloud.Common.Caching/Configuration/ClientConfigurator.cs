﻿using BettingCloud.Common.Caching.Policy;
//using BettingCloud.Common.Configuration;
using EnsureThat;

namespace BettingCloud.Common.Caching.Configuration
{
    internal class ClientConfigurator : IClientConfigurator
    {
        private readonly IClientSettings _settings;

        internal ClientConfigurator(IClientSettings settings)
        {
            Ensure.That(settings, Constants.Validation.Keys.Settings).IsNotNull();

            _settings = settings;

            UseNamespace(string.Empty);

            LocalCache = new LocalCacheConfigurator(_settings);
            RemoteCache = new RemoteCacheConfigurator(_settings);
            Notifications = new NotificationsConfigurator(_settings);

            Policies = new PolicyConfigurator(_settings);
            Policies.Add(ItemPolicy.Default);

           // Diagnostics = new DiagnosticsConfigurator(_settings);
            OnFailure = new FailureConfigurator(_settings);
        }

        public ILocalCacheConfigurator LocalCache { get; }

        public IRemoteCacheConfigurator RemoteCache { get; }

        public INotificationsConfigurator Notifications { get; }

        public IPolicyConfigurator Policies { get; }

       // public IDiagnosticsConfigurator Diagnostics { get; }

        public IFailureConfigurator OnFailure { get; }

        public IClientConfigurator UseNamespace(string ns)
        {
            if (string.IsNullOrEmpty(ns) == false)
            {
                _settings.Namespace = $"{ns}:";
            }

            return this;
        }

        public IClientConfigurator UseCompression(int byteSizeThreshold = 1000)
        {
            _settings.UseCompression = true;
            _settings.UseCompressionByteSizeThreshold = byteSizeThreshold;
            return this;
        }
    }
}
