﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Configuration
{
    internal class FailureConfigurator : IFailureConfigurator
    {
        private readonly IClientSettings _settings;

        internal FailureConfigurator(IClientSettings settings)
        {
            _settings = settings;
        }

        public IFailureConfigurator SetDefault(FailureBehaviour behaviour)
        {
            _settings.FailureBehaviours.SetDefault(behaviour);
            return this;
        }

        public IFailureConfigurator Use(FailureBehaviour behaviour, string policyName)
        {
            _settings.FailureBehaviours.Add(behaviour, policyName);
            return this;
        }

        public IFailureConfigurator Use(FailureBehaviour behaviour, ItemPolicy policy)
        {
            _settings.FailureBehaviours.Add(behaviour, policy);
            return this;
        }
    }
}
