﻿using BettingCloud.Common.Caching.Policy;
using EnsureThat;
using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Configuration
{
    public class PolicyConfigurator : IPolicyConfigurator
    {
        private readonly IClientSettings _settings;

        internal PolicyConfigurator(IClientSettings settings)
        {
            _settings = settings;
        }

        public IPolicyConfigurator Add(ItemPolicy policy)
        {
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            _settings.CachedPolicies.Add(policy.Name, policy);

            return this;
        }

        public IPolicyConfigurator Add(IEnumerable<ItemPolicy> policies)
        {
            foreach (var policy in policies)
            {
                _settings.CachedPolicies.Add(policy.Name, policy);
            }

            return this;
        }

        public IPolicyConfigurator SetDefault(ItemPolicy policy)
        {
            _settings.CachedPolicies.DefaultPolicy = policy;

            return this;
        }
    }
}
