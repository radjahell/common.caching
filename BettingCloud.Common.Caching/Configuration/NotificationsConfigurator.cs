﻿using BettingCloud.Common.Caching.Enumerations;
using EnsureThat;

namespace BettingCloud.Common.Caching.Configuration
{
    internal class NotificationsConfigurator : INotificationsConfigurator
    {
        private readonly IClientSettings _settings;

        internal NotificationsConfigurator(IClientSettings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Enables the cache change notifications.
        /// </summary>
        public INotificationsConfigurator Enable()
        {
            _settings.NotificationEnabled = true;
            return this;
        }

        /// <summary>
        /// Disables the cache change notifications.
        /// </summary>
        public INotificationsConfigurator Disable()
        {
            ClearNotificationSettings();

            _settings.NotificationEnabled = false;
            return this;
        }

        /// <summary>
        /// Configure the cache change notifications to use RabbitMQ.
        /// </summary>
        public INotificationsConfigurator UseRabbitMQ(
            string host,
            string exchangeName,
            int? port = null,
            string username = null,
            string password = null,
            string virtualHost = null)
        {
            Ensure.That(host, Constants.Validation.Keys.Host).IsNotNull();

            ClearNotificationSettings();

            _settings.NotificationEnabled = true;
            _settings.NotificationType = NotificationType.RabbitMq;
            _settings.NotificationRabbitMqHost = host;
            _settings.NotificationRabbitMqPort = port;
            _settings.NotificationRabbitMqExchangeName = exchangeName;
            _settings.NotificationRabbitMqUsername = username;
            _settings.NotificationRabbitMqPassword = password;
            _settings.NotificationRabbitMqVirtualHost = virtualHost;

            return this;
        }

        /// <summary>
        /// Configure the cache change notifications to use Redis.
        /// </summary>
        public INotificationsConfigurator UseRedis(string connectionString, int databaseId)
        {
            Ensure.That(connectionString, Constants.Validation.Keys.ConnectionString).IsNotNull();
            Ensure.That(databaseId, Constants.Validation.Keys.DatabaseId).IsGte(0).IsLte(15);

            ClearNotificationSettings();

            _settings.NotificationEnabled = true;
            _settings.NotificationType = NotificationType.Redis;
            _settings.NotificationRedisConnectionString = connectionString;
            _settings.NotificationRedisDatabaseId = databaseId;

            return this;
        }

        private void ClearNotificationSettings()
        {
            _settings.NotificationType = NotificationType.None;
            _settings.NotificationEnabled = false;

            // RabbitMQ
            _settings.NotificationRabbitMqHost = null;
            _settings.NotificationRabbitMqPort = null;
            _settings.NotificationRabbitMqExchangeName = null;
            _settings.NotificationRabbitMqUsername = null;
            _settings.NotificationRabbitMqPassword = null;
            _settings.NotificationRabbitMqVirtualHost = null;

            // Redis
            _settings.NotificationRedisConnectionString = null;
        }
    }
}
