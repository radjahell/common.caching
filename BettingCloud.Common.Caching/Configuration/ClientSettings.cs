﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Failure;
using BettingCloud.Common.Caching.Policy;
//using BettingCloud.Common.Diagnostics.Logging;
//using BettingCloud.Common.Diagnostics.Performance;

namespace BettingCloud.Common.Caching.Configuration
{
    internal class ClientSettings : IClientSettings
    {
        public ClientSettings()
        {
            FailureBehaviours = new FailureBehaviourRepository();
            CachedPolicies = new ItemPolicyRepository
            {
                DefaultPolicy = ItemPolicy.Default
            };

            UseCompression = true;
            UseCompressionByteSizeThreshold = 1000;
            Namespace = string.Empty;
        }

        public bool LocalCacheEnabled { get; set; }

        public bool RemoteCacheEnabled { get; set; }

        public RemoteCacheType RemoteCacheType { get; set; }

        public string RemoteRedisConnectionString { get; set; }

        public int? RemoteRedisDatabaseId { get; set; }

        public NotificationType NotificationType { get; set; }

        public bool NotificationEnabled { get; set; }

        public string NotificationRabbitMqHost { get; set; }

        public int? NotificationRabbitMqPort { get; set; }

        public string NotificationRabbitMqExchangeName { get; set; }

        public string NotificationRabbitMqUsername { get; set; }

        public string NotificationRabbitMqPassword { get; set; }

        public string NotificationRabbitMqVirtualHost { get; set; }

        public string NotificationRedisConnectionString { get; set; }

        public int? NotificationRedisDatabaseId { get; set; }

        public bool AllowGzip { get; set; }

       // public ILogProvider LogProvider { get; set; }

       // public IPerformanceProvider PerformanceProvider { get; set; }

        public IItemPolicyRepository CachedPolicies { get; set; }

        public bool UseCompression { get; set; }

        public int UseCompressionByteSizeThreshold { get; set; }

        public IFailureBehaviourRepository FailureBehaviours { get; }

        public string Namespace { get; set; }
    }
}
