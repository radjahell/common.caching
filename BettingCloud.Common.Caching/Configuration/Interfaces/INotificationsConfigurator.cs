﻿using System;

namespace BettingCloud.Common.Caching.Configuration
{
    public interface INotificationsConfigurator
    {
        /// <summary>
        /// Enables the cache change notifications.
        /// </summary>
        INotificationsConfigurator Enable();

        /// <summary>
        /// Disables the cache change notifications.
        /// </summary>
        INotificationsConfigurator Disable();

        /// <summary>
        /// Configure the cache change notifications to use RabbitMQ.
        /// </summary>
        INotificationsConfigurator UseRabbitMQ(
            string host,
            string exchangeName,
            int? port = null,
            string username = null,
            string password = null,
            string virtualHost = null);

        /// <summary>
        /// Configure the cache change notifications to use Redis.
        /// </summary>
        INotificationsConfigurator UseRedis(
            string connectionString,
            int databaseId);
    }
}
