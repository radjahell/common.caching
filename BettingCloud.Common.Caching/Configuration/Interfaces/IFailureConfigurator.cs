﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Configuration
{
    public interface IFailureConfigurator
    {
        IFailureConfigurator Use(FailureBehaviour behaviour, ItemPolicy policy);

        IFailureConfigurator Use(FailureBehaviour behaviour, string policyName);

        IFailureConfigurator SetDefault(FailureBehaviour behaviour);
    }
}
