﻿//using BettingCloud.Common.Configuration;

namespace BettingCloud.Common.Caching.Configuration
{
    /// <summary>
    /// Allow to configure the cache client.
    /// </summary>
    public interface IClientConfigurator
    {
        /// <summary>
        /// Provides the local in-memory cache configurator.
        /// </summary>
        ILocalCacheConfigurator LocalCache { get; }

        /// <summary>
        /// Provides the remote cache configurator.
        /// </summary>
        IRemoteCacheConfigurator RemoteCache { get; }

        /// <summary>
        /// Provides the remote cache configurator.
        /// </summary>
        INotificationsConfigurator Notifications { get; }

        /// <summary>
        /// Provides the diagnostics configurator.
        /// </summary>
       // IDiagnosticsConfigurator Diagnostics { get; }

        /// <summary>
        /// Provides the policy configurator.
        /// </summary>
        IPolicyConfigurator Policies { get; }

        /// <summary>
        /// Provides the failure configurator.
        /// </summary>
        IFailureConfigurator OnFailure { get; }

        /// <summary>
        /// Adds the specified namespace as a prefix to every key.
        /// </summary>
        /// <param name="ns">The namespace to add to every key.</param>
        IClientConfigurator UseNamespace(string ns);

        IClientConfigurator UseCompression(int byteSizeThreshold);
    }
}
