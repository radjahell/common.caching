﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Failure;
using BettingCloud.Common.Caching.Policy;
//using BettingCloud.Common.Diagnostics.Logging;
//using BettingCloud.Common.Diagnostics.Performance;

namespace BettingCloud.Common.Caching.Configuration
{
    internal interface IClientSettings
    {
        bool LocalCacheEnabled { get; set; }

        bool RemoteCacheEnabled { get; set; }

        RemoteCacheType RemoteCacheType { get; set; }

        string RemoteRedisConnectionString { get; set; }

        int? RemoteRedisDatabaseId { get; set; }

        NotificationType NotificationType { get; set; }

        bool NotificationEnabled { get; set; }

        string NotificationRabbitMqHost { get; set; }

        int? NotificationRabbitMqPort { get; set; }

        string NotificationRabbitMqExchangeName { get; set; }

        string NotificationRabbitMqUsername { get; set; }

        string NotificationRabbitMqPassword { get; set; }

        string NotificationRabbitMqVirtualHost { get; set; }

        string NotificationRedisConnectionString { get; set; }

        int? NotificationRedisDatabaseId { get; set; }

        bool AllowGzip { get; set; }

      //  ILogProvider LogProvider { get; set; }

     //   IPerformanceProvider PerformanceProvider { get; set; }

        IItemPolicyRepository CachedPolicies { get; set; }

        bool UseCompression { get; set; }

        int UseCompressionByteSizeThreshold { get; set;  }

        IFailureBehaviourRepository FailureBehaviours { get; }

        string Namespace { get; set; }
    }
}
