﻿namespace BettingCloud.Common.Caching.Configuration
{
    public interface ILocalCacheConfigurator
    {
        /// <summary>
        /// Enables the local in-memory cache.
        /// </summary>
        ILocalCacheConfigurator Enable();

        /// <summary>
        /// Disables the local in-memory cache.
        /// </summary>
        ILocalCacheConfigurator Disable();
    }
}
