﻿namespace BettingCloud.Common.Caching.Configuration
{
    public interface IRemoteCacheConfigurator
    {
        /// <summary>
        /// Enables the remote cache.
        /// </summary>
        IRemoteCacheConfigurator Enable();

        /// <summary>
        /// Disables the remote cache.
        /// </summary>
        IRemoteCacheConfigurator Disable();

        /// <summary>
        /// Configure the remote cache to use Redis.
        /// </summary>
        IRemoteCacheConfigurator UseRedis(string connectionString, int databaseId);
    }
}
