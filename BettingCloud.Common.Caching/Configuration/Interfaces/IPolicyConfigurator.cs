﻿using BettingCloud.Common.Caching.Policy;
using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Configuration
{
    public interface IPolicyConfigurator
    {
        /// <summary>
        /// Adds a single <see cref="ItemPolicy"/>.
        /// </summary>
        /// <param name="policy">An instance of <see cref="ItemPolicy"/> to add.</param>
        /// <returns>The <see cref="IPolicyConfigurator"/> instance as per Fluent API.</returns>
        IPolicyConfigurator Add(ItemPolicy policy);

        /// <summary>
        /// Adds a list of policies.
        /// </summary>
        /// <param name="policies">A list of policies to be added.</param>
        /// <returns>The <see cref="IPolicyConfigurator"/> instance as per Fluent API.</returns>
        IPolicyConfigurator Add(IEnumerable<ItemPolicy> policies);

        /// <summary>
        /// Set the default <see cref="ItemPolicy"/> to be used when no policy is provided.
        /// </summary>
        /// <param name="policy">The <see cref="ItemPolicy"/> to be used by default.</param>
        /// <returns>The <see cref="IPolicyConfigurator"/> instance as per Fluent API.</returns>
        IPolicyConfigurator SetDefault(ItemPolicy policy);
    }
}
