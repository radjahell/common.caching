﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Exceptions;
using EnsureThat;

namespace BettingCloud.Common.Caching.Validations
{
    internal static class ClientSettingsValidation
    {
        internal const string LocalRemoteBothDisabledErrorMessage = "Local and Remote cache are disabled. At least one provider should be activated.";
        internal const string RemoteNotConfiguredErrorMessage = "Remote is enabled but not configured.";
        internal const string NotificationsRequireRemoteToBeEnabledErrorMessage = "Notifications requires remote cache provider to be enabled.";
        internal const string LoggerNotConfiguredErrorMessage = "A Logger instance must be set.";
        internal const string CachePoliciesNotConfiguredErrorMessage = "CachePolicies instance must be set.";
        internal const string CachePoliciesDefaultNotConfiguredErrorMessage = "A default cache policy must be set.";

        public static Param<IClientSettings> IsValid(this Param<IClientSettings> param)
        {
            var settings = param.Value;

            Ensure.That(settings, Constants.Validation.Keys.Settings).IsNotNull();

            if (settings.LocalCacheEnabled == false &&
                settings.RemoteCacheEnabled == false)
            {
                throw new CacheConfigurationException(LocalRemoteBothDisabledErrorMessage);
            }

            ValidateRemoteCacheSettings(settings);
            ValidateNotificationsSettings(settings);
            ValidateLoggerSettings(settings);
            ValidateCachedPolicies(settings);

            return param;
        }

        private static void ValidateRemoteCacheSettings(IClientSettings settings)
        {
            if (settings.RemoteCacheEnabled && settings.RemoteCacheType.Equals(RemoteCacheType.None))
            {
                throw new CacheConfigurationException(RemoteNotConfiguredErrorMessage);
            }
        }

        private static void ValidateNotificationsSettings(IClientSettings settings)
        {
            if (settings.NotificationEnabled && settings.RemoteCacheEnabled == false)
            {
                throw new CacheConfigurationException(NotificationsRequireRemoteToBeEnabledErrorMessage);
            }

            // TODO: Perform further validations
        }

        private static void ValidateLoggerSettings(IClientSettings settings)
        {
            //if (settings.LogProvider == null)
            //{
            //    throw new CacheConfigurationException(LoggerNotConfiguredErrorMessage);
            //}
        }

        private static void ValidateCachedPolicies(IClientSettings settings)
        {
            if (settings.CachedPolicies == null || settings.CachedPolicies.Count == 0)
            {
                throw new CacheConfigurationException(CachePoliciesNotConfiguredErrorMessage);
            }
            else if (settings.CachedPolicies.DefaultPolicy == null)
            {
                throw new CacheConfigurationException(CachePoliciesDefaultNotConfiguredErrorMessage);
            }
        }
    }
}
