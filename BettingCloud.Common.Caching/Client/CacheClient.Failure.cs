﻿using BettingCloud.Common.Caching.Client.Requests;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Providers;
using BettingCloud.Common.Caching.Providers.Local;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        private const string FailureExceptionMessage = "Remote caching failed and will recover automatically. Please refer to logs for more details.";

        private CacheItem<T> CheckForFailures<T>(CacheItem<T> cacheItem, GetRequest<T> request = null)
        {
            var location = Settings.LocationType();

            if (cacheItem == null ||
                cacheItem.Value.IsNullOrDefault() ||
                location.Has(CacheLocationType.Remote) == false ||
                Remote.IsInWorkingState())
            {
                return cacheItem;
            }

            var behaviour = FailureBehaviours.Get(cacheItem.Policy?.Name);

            switch (behaviour)
            {
                case FailureBehaviour.ThrowException:
                    throw new CacheFailureException(FailureExceptionMessage);

                case FailureBehaviour.UseFetchOrDefault:
                    return PerformFetchOrDefault(cacheItem, request);

                case FailureBehaviour.NotSet:
                case FailureBehaviour.UseMemory:
                    return cacheItem;

                default:
                    return cacheItem;
            }
        }

        private CacheItem<T> PerformFetchOrDefault<T>(CacheItem<T> cacheItem, GetRequest<T> request = null)
        {
            var key = GetKey(cacheItem, request);
            var fetch = GetFetch(cacheItem, request);
            var fetchAsync = GetFetchAsync(cacheItem, request);
            var policy = GetPolicy(cacheItem, request);

            try
            {
                var value = GetValue(fetch, fetchAsync);

                return new LocalCacheItem<T>(
                    key,
                    value,
                    fetch,
                    fetchAsync,
                    policy,
                    DateTime.UtcNow);
            }
            catch (Exception ex)
            {
                //LogProvider.ExceptionAsync(ex, $"Failed to fetch item {request?.Key}");
            }

            return CacheItem<T>.Default;
        }

        private string GetKey<T>(CacheItem<T> cacheItem, GetRequest<T> request)
        {
            if (string.IsNullOrWhiteSpace(request?.Key) == false)
            {
                return request.Key;
            }

            return cacheItem.Key;
        }

        private ItemPolicy GetPolicy<T>(CacheItem<T> cacheItem, GetRequest<T> request)
        {
            if (request?.Policy != null)
            {
                return request.Policy;
            }

            return cacheItem.Policy;
        }

        private Func<T> GetFetch<T>(CacheItem<T> cacheItem, GetRequest<T> request)
        {
            Func<T> fetch = null;

            if (cacheItem.Fetch != null)
            {
                fetch = cacheItem.Fetch;
            }
            else if (request?.Fetch != null)
            {
                fetch = request.Fetch;
            }

            return fetch;
        }

        private Func<Task<T>> GetFetchAsync<T>(CacheItem<T> cacheItem, GetRequest<T> request)
        {
            Func<Task<T>> fetchAsync = null;

            if (cacheItem.FetchAsync != null)
            {
                fetchAsync = cacheItem.FetchAsync;
            }
            else if (request?.FetchAsync != null)
            {
                fetchAsync = request.FetchAsync;
            }

            return fetchAsync;
        }

        private T GetValue<T>(Func<T> fetch, Func<Task<T>> fetchAsync)
        {
            if (fetch != null)
            {
                return fetch();
            }

            if (fetchAsync != null)
            {
                var task = fetchAsync();
                task.Wait();

                return task.Result;
            }

            return default(T);
        }
    }
}
