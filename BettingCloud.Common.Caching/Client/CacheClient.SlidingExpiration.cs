﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Providers;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        private ConcurrentDictionary<string, ICachePolicy> _slidingExpirationBucket = new ConcurrentDictionary<string, ICachePolicy>();

        private static readonly TimeSpan SlidingStartDelay = TimeSpan.FromSeconds(1);
        private static readonly TimeSpan SlidingProcessPeriod = TimeSpan.FromMinutes(5);

        private readonly object _slidingExpirationLock = new object();

        private Timer _slidingExpirationProcessTimer;

        private void InitSlidingExpirationProcess()
        {
            _slidingExpirationProcessTimer = new Timer(OnSlidingExpirationProcess, null, SlidingStartDelay, SlidingProcessPeriod);
        }

        internal void OnSlidingExpirationProcess(object stateInfo)
        {
            if (_slidingExpirationBucket.Count == 0)
            {
                return;
            }

            ConcurrentDictionary<string, ICachePolicy> currentBucket;

            lock (_slidingExpirationLock)
            {
                currentBucket = _slidingExpirationBucket;

                _slidingExpirationBucket = new ConcurrentDictionary<string, ICachePolicy>();
            }

            foreach (var key in currentBucket.Keys.Distinct())
            {
                var policy = currentBucket[key];

                if (policy?.Ttl.HasValue == true)
                    Remote.UpdateExpiry(key, policy.Ttl.Value);
            }
        }

        private void AddToSlidingExpirationUpdate<T>(CacheItem<T> cacheItem)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Remote) == false)
            {
                return;
            }

            if (cacheItem.Policy?.Remote.IsSliding == true &&
                cacheItem.Policy?.Remote?.Ttl.HasValue == true)
            {
                if (_slidingExpirationBucket.ContainsKey(cacheItem.Key))
                {
                    return;
                }

                lock(_slidingExpirationLock)
                {
                    if (_slidingExpirationBucket.ContainsKey(cacheItem.Key))
                    {
                        return;
                    }

                    _slidingExpirationBucket.GetOrAdd(cacheItem.Key, cacheItem.Policy.Remote);
                }
            }
        }
    }
}
