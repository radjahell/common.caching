﻿using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;
using BettingCloud.Common.Caching.Providers.Notifications;

namespace BettingCloud.Common.Caching.Client
{
    public interface ICacheClient : IDisposable
    {
        event CacheKeyRemovedEventHandler KeyRemoved;

        event CacheKeyInvalidatedEventHandler KeyInvalidated;

        event CacheFlushEventHandler Flushed;


        bool PolicyExists(string policyName);

        void AddPolicy(string policyName, ItemPolicy policy);


        T Get<T>(string key);

        Task<T> GetAsync<T>(string key);


        T AddOrGet<T>(string key, Func<T> fetch, ItemPolicy policy);

        Task<T> AddOrGetAsync<T>(string key, Func<T> fetch, ItemPolicy policy);

        Task<T> AddOrGetAsync<T>(string key, Func<Task<T>> fetch, ItemPolicy policy);


        T AddOrGet<T>(string key, Func<T> fetch, string policyName);

        Task<T> AddOrGetAsync<T>(string key, Func<T> fetch, string policyName);

        Task<T> AddOrGetAsync<T>(string key, Func<Task<T>> fetch, string policyName);


        void Set<T>(string key, T item, ItemPolicy policy);

        Task SetAsync<T>(string key, T item, ItemPolicy policy);

        void Set<T>(string key, T item, string policyName);

        Task SetAsync<T>(string key, T value, string policyName);


        void Invalidate(string key);

        Task InvalidateAsync(string key);

        void Remove(string key);

        Task RemoveAsync(string key);
    }
}
