﻿using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Client.Requests
{
    internal class SetRequest<T> : BaseRequest
    {
        public T Value { get; set; }

        public Func<T> Fetch { get; set; }

        public Func<Task<T>> FetchAsync { get; set; }
    }
}
