﻿using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Client.Requests
{
    internal abstract class BaseRequest
    {
        public string Key { get; set; }

        public ItemPolicy Policy { get; set; }
    }
}
