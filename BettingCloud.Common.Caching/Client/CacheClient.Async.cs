﻿using BettingCloud.Common.Caching.Client.Requests;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Providers;
using BettingCloud.Common.Caching.Providers.Remote;
using EnsureThat;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        public async Task<T> GetAsync<T>(string key)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            key = FixKey(key);

            var request = new GetRequest<T>() { Key = key };

            var cacheItem = await GetInternalAsync(request).ConfigureAwait(false);

            return cacheItem.Value;
        }

        public Task<T> AddOrGetAsync<T>(string key, Func<Task<T>> fetch, string policyName)
        {
            var policy = Settings.CachedPolicies.Get(policyName);

            return AddOrGetAsync(key, fetch, policy);
        }

        public Task<T> AddOrGetAsync<T>(string key, Func<T> fetch, string policyName)
        {
            var policy = Settings.CachedPolicies.Get(policyName);

            return AddOrGetAsync(key, fetch, policy);
        }

        public async Task<T> AddOrGetAsync<T>(string key, Func<Task<T>> fetch, ItemPolicy policy)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            key = FixKey(key);

            var request = new GetRequest<T>()
            {
                Key = key,
                Policy = policy,
                FetchAsync = fetch
            };

            var cacheItem = await GetInternalAsync(request).ConfigureAwait(false);

            if (!cacheItem.IsNullValue)
            {
                return cacheItem.Value;
            }

            var localAsyncLockObject = Locker.GetAsyncLockObject(key);

            using (await localAsyncLockObject.LockAsync().ConfigureAwait(false))
            {
                cacheItem = await GetInternalAsync(request).ConfigureAwait(false);

                if (!cacheItem.IsNullValue)
                {
                    return cacheItem.Value;
                }

                var value = await FetchAndSetInternalAsync(request).ConfigureAwait(false);
                return value;
            }
        }

        public async Task<T> AddOrGetAsync<T>(string key, Func<T> fetch, ItemPolicy policy)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            key = FixKey(key);

            var request = new GetRequest<T>()
            {
                Key = key,
                Fetch = fetch,
                Policy = policy
            };

            var cacheItem = await GetInternalAsync(request).ConfigureAwait(false);

            if (!cacheItem.IsNullValue)
            {
                return cacheItem.Value;
            }

            var localAsyncLockObject = Locker.GetAsyncLockObject(key);

            using (await localAsyncLockObject.LockAsync().ConfigureAwait(false))
            {
                cacheItem = await GetInternalAsync(request).ConfigureAwait(false);

                if (!cacheItem.IsNullValue)
                {
                    return cacheItem.Value;
                }

                var value = await FetchAndSetInternalAsync(request).ConfigureAwait(false);
                return value;
            }
        }

        public async Task InvalidateAsync(string key)
        {
            key = FixKey(key);

            await RemoveAsync(key);
            Notifier.EmitInvalidateKeyMessage(key);
        }

        public async Task RemoveAsync(string key)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            key = FixKey(key);

            Local.Remove(key);
            await Remote.RemoveAsync(key).ConfigureAwait(false);

            Notifier.EmitRemoveKeyMessage(key);
        }

        public Task SetAsync<T>(string key, T value, string policyName)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            var policy = Settings.CachedPolicies.Get(policyName);

            return SetAsync(key, value, policy);
        }

        public Task SetAsync<T>(string key, T item, ItemPolicy policy)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            key = FixKey(key);

            var request = new SetRequest<T>()
            {
                Key = key,
                Value = item,
                Policy = policy
            };

            return SetInternalAsync(request);
        }

        private async Task SetInternalAsync<T>(SetRequest<T> request)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local))
            {
                Local.Set(
                    request.Key,
                    request.Value,
                    request.Fetch,
                    request.FetchAsync,
                    request.Policy);
            }

            if (location.Has(CacheLocationType.Remote))
            {
                var cacheItem = new RemoteCacheItem<T>(
                    request.Key,
                    request.Value,
                    request.Fetch,
                    request.FetchAsync,
                    request.Policy,
                    DateTime.UtcNow);

                var result = await Remote.SetAsync(cacheItem).ConfigureAwait(false);

                if (result == RemoteCacheSetResult.Updated)
                {
                    Notifier.EmitUpdatedKeyMessage(request.Key);
                }
            }
        }

        private async Task<CacheItem<T>> GetInternalAsync<T>(GetRequest<T> request)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local))
            {
                var localItem = Local.Get<T>(request.Key);

                if (!localItem.IsNullOrDefault() && !localItem.IsNullValue)
                {
                    AddToSlidingExpirationUpdate(localItem);

                    return CheckForFailures(localItem);
                }
            }

            if (location.Has(CacheLocationType.Remote))
            {
                var remoteItem = await Remote.GetAsync<T>(request.Key).ConfigureAwait(false);

                if (!remoteItem.IsNullOrDefault() && !remoteItem.IsNullValue)
                {
                    if (location.Has(CacheLocationType.Local))
                    {
                        Local.Set(
                            request.Key,
                            remoteItem.Value,
                            request.Fetch,
                            request.FetchAsync,
                            remoteItem.Policy);
                    }

                    AddToSlidingExpirationUpdate(remoteItem);

                    return CheckForFailures(remoteItem);
                }
            }

            return CacheItem<T>.Default;
        }

        private async Task<T> FetchAndSetInternalAsync<T>(GetRequest<T> request)
        {
            var item = default(T);

            if (!request.CanLoad)
            {
                return item;
            }

            try
            {
                if (request.Fetch != null)
                {
                    item = request.Fetch();
                }

                if (request.FetchAsync != null)
                {
                    item = await request.FetchAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                //await LogProvider.ExceptionAsync(ex, $"Failed to fetch item {request.Key}");

                throw new CacheFetchException(Constants.Client.FetchException, ex);
            }

            try
            {
                if (!item.IsNullOrDefault())
                {
                    var setRequest = new SetRequest<T>()
                    {
                        Key = request.Key,
                        Value = item,
                        Policy = request.Policy,
                        Fetch = request.Fetch,
                        FetchAsync = request.FetchAsync
                    };

                    await SetInternalAsync(setRequest).ConfigureAwait(false);
                }

                return item;
            }
            catch (Exception ex)
            {
               // await LogProvider.ExceptionAsync(ex, $"Failed to setinternal for item {request.Key}");
            }

            return item;
        }
    }
}
