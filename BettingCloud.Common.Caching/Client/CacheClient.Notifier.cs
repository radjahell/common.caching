﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Providers.Notifications;
using System;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        public event CacheFlushEventHandler Flushed;
        public event CacheKeyInvalidatedEventHandler KeyInvalidated;
        public event CacheKeyRemovedEventHandler KeyRemoved;
        public event CacheKeyUpdatedEventHandler KeyUpdated;

        private void InitNotifications()
        {
            Notifier.Flushed += OnFlushed;
            Notifier.KeyInvalidated += OnKeyInvalidated;
            Notifier.KeyRemoved += OnKeyRemoved;
            Notifier.KeyUpdated += OnKeyUpdated;
        }

        private void OnKeyUpdated(object sender, KeyEventArgs e)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local) &&
                Local.Contains(e.Key))
            {
                Local.Remove(e.Key);
                KeyUpdated?.Invoke(this, e);
            }
        }

        private void OnKeyRemoved(object sender, KeyEventArgs e)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local) &&
                Local.Contains(e.Key))
            {
                Local.Remove(e.Key);
                KeyRemoved?.Invoke(this, e);
            }
        }

        private void OnKeyInvalidated(object sender, KeyEventArgs e)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local) &&
                Local.Contains(e.Key))
            {
                Local.Remove(e.Key);
                KeyInvalidated?.Invoke(this, e);
            }
        }

        private void OnFlushed(object sender, EventArgs e)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local))
            {
                Local.Flush();
                Flushed?.Invoke(this, e);
            }
        }
    }
}
