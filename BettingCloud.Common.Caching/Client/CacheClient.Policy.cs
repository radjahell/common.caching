﻿using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        public bool PolicyExists(string policyName)
        {
            if (Settings?.CachedPolicies?.Exists(policyName) == true)
                return true;

            return false;
        }

        public void AddPolicy(string policyName, ItemPolicy policy)
        {
            Settings?.CachedPolicies?.Add(policyName, policy);
        }
    }
}
