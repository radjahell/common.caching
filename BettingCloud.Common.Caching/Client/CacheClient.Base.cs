﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Context;
using BettingCloud.Common.Caching.Failure;
using BettingCloud.Common.Caching.Locking;
using BettingCloud.Common.Caching.Providers.Local;
using BettingCloud.Common.Caching.Providers.Notifications;
using BettingCloud.Common.Caching.Providers.Remote;
//using BettingCloud.Common.Diagnostics.Logging;
using EnsureThat;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient : ICacheClient
    {
        internal readonly IClientContext Context;

        internal CacheClient(IClientContext context)
        {
            Ensure.That(context, Constants.Validation.Keys.Context).IsNotNull();

            Context = context;

            InitNotifications();
            InitSlidingExpirationProcess();
        }
        
        private ILocalCacheProvider Local => Context.Local;

        private IRemoteCacheProvider Remote => Context.Remote;

      //  private ILogProvider LogProvider => Context.LogProvider;

        private INotificationProvider Notifier => Context.Notifier;

        private ILockingService Locker => Context.Locker;

        private IClientSettings Settings => Context.Settings;

        private IFailureBehaviourRepository FailureBehaviours => Settings.FailureBehaviours;

        private string FixKey(string key)
        {
            if (string.IsNullOrWhiteSpace(Settings.Namespace))
            {
                return key;
            }

            if (key.StartsWith(Settings.Namespace))
            {
                return key;
            }

            return $"{Settings.Namespace}{key}";
        }

        public void Dispose()
        {
            Context?.Dispose();
            _slidingExpirationProcessTimer?.Dispose();
        }
    }
}
