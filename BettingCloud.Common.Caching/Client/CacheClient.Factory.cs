﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Context;
using System;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        /// <summary>
        /// Create a new CacheClient instance.
        /// Note that CacheClient implements IDisposable and can be disposed when no longer required.
        /// It is exceptionally rare that you would want to use a CacheClient briefly, as the idea is to re-use this object and 2 redis connections are attached to each instance.
        /// </summary>
        /// <param name="configure">an Action to configure the cache client.</param>
        /// <returns>Instance of CacheClient</returns>
        public static ICacheClient Create(Action<IClientConfigurator> configure = null)
        {
            var settings = new ClientSettings();
            var configurator = new ClientConfigurator(settings);

            configure?.Invoke(configurator);

            var ctx = new ClientContext(settings);
            var client = new CacheClient(ctx);
            ctx.Client = client;

            return client;
        }
    }
}
