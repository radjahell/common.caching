﻿using BettingCloud.Common.Caching.Client.Requests;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Exceptions;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Providers;
using BettingCloud.Common.Caching.Providers.Remote;
using EnsureThat;
using System;

namespace BettingCloud.Common.Caching.Client
{
    public partial class CacheClient
    {
        public T Get<T>(string key)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            key = FixKey(key);

            var cacheItem = GetInternal(new GetRequest<T>() { Key = key });

            return cacheItem.Value;
        }

        public T AddOrGet<T>(string key, Func<T> fetch, string policyName)
        {
            var policy = Settings.CachedPolicies.Get(policyName);

            return AddOrGet(key, fetch, policy);
        }

        public T AddOrGet<T>(string key, Func<T> fetch, ItemPolicy policy)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            key = FixKey(key);

            var getRequest = new GetRequest<T>() { Key = key , Fetch = fetch, Policy = policy};

            var cacheItem = GetInternal(getRequest);

            if (!cacheItem.IsNullValue)
            {
                return cacheItem.Value;
            }
            
            var lockObject = Locker.GetLockObject(key);

            lock (lockObject)
            {
                cacheItem = GetInternal(getRequest);

                if (!cacheItem.IsNullValue)
                {
                    return cacheItem.Value;
                }

                try
                {
                    var value = fetch();

                    if (!value.IsNullOrDefault())
                    {
                        var setRequest = new SetRequest<T>()
                        {
                            Key = key,
                            Value = value,
                            Policy = policy,
                            Fetch = fetch
                        };

                        SetInternal(setRequest);
                        return value;
                    }
                }
                catch (Exception ex)
                {
                    //LogProvider.ExceptionAsync(ex, $"Failed to fetch item {key}");

                    throw new CacheFetchException(Constants.Client.FetchException, ex);
                }
            }

            return cacheItem.Value;
        }
        
        public void Invalidate(string key)
        {
            key = FixKey(key);

            Remove(key);
            Notifier.EmitInvalidateKeyMessage(key);
        }

        public void Remove(string key)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            key = FixKey(key);

            Local.Remove(key);
            Remote.Remove(key);
            Notifier.EmitRemoveKeyMessage(key);
        }
        
        public void Set<T>(string key, T item, string policyName)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();

            var policy = Settings.CachedPolicies.Get(policyName);

            Set(key, item, policy);
        }

        public void Set<T>(string key, T item, ItemPolicy policy)
        {
            Ensure.That(key, Constants.Validation.Keys.Key).IsNotNullOrEmpty();
            Ensure.That(policy, Constants.Validation.Keys.Policy).IsNotNull();

            key = FixKey(key);

            SetInternal(new SetRequest<T>() {
                Key = key,
                Value = item,
                Policy = policy
            });
        }

        private ICacheItem<T> GetInternal<T>(GetRequest<T> request)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local))
            {
                var localItem = Local.Get<T>(request.Key);

                if (!localItem.IsNullOrDefault() && !localItem.IsNullValue)
                {
                    AddToSlidingExpirationUpdate(localItem);

                    return CheckForFailures(localItem);
                }
            }

            if (location.Has(CacheLocationType.Remote))
            {
                var remoteItem = Remote.Get<T>(request.Key);

                if (!remoteItem.IsNullOrDefault() && !remoteItem.IsNullValue)
                {
                    if (location.Has(CacheLocationType.Local))
                    {
                        Local.Set(request.Key, remoteItem.Value, request.Fetch, request.FetchAsync, remoteItem.Policy);
                    }

                    AddToSlidingExpirationUpdate(remoteItem);

                    return CheckForFailures(remoteItem);
                }
            }

            return CacheItem<T>.Default;
        }

        private void SetInternal<T>(SetRequest<T> request)
        {
            var location = Settings.LocationType();

            if (location.Has(CacheLocationType.Local))
            {
                Local.Set(request.Key, request.Value, request.Fetch, request.FetchAsync, request.Policy);
            }

            if (location.Has(CacheLocationType.Remote))
            {
                var cacheItem = new RemoteCacheItem<T>(
                    request.Key,
                    request.Value,
                    request.Fetch,
                    request.FetchAsync,
                    request.Policy,
                    DateTime.UtcNow);

                var result = Remote.Set(cacheItem);

                if (result == RemoteCacheSetResult.Updated)
                {
                    Notifier.EmitUpdatedKeyMessage(request.Key);
                }
            }
        }
    }
}
