﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers.Remote
{
    internal class RemoteCacheItem<T> : CacheItem<T>
    {
        /// <summary>
        /// A default readonly empty value.
        /// </summary>
        public new static readonly RemoteCacheItem<T> Default = new RemoteCacheItem<T>(string.Empty, default(T), null, null, ItemPolicy.Default, DateTime.UtcNow);

        /// <summary>
        /// Default ctor.
        /// </summary>
        /// <param name="key">Cache item key.</param>
        /// <param name="value">Cache item value.</param>
        /// <param name="policy">Cache item policy.</param>
        /// <param name="added">Added date (for RA)</param>
        public RemoteCacheItem(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy, DateTime added)
            : base(key, value, fetch, fetchAsync, policy, added) { }

        /// <summary>
        /// CacheItem location.
        /// </summary>
        public override CacheLocationType Location => CacheLocationType.Remote;
    }
}
