﻿using System;
using System.Threading.Tasks;
using BettingCloud.Common.Caching.Enumerations;

namespace BettingCloud.Common.Caching.Providers.Remote
{
    internal interface IRemoteCacheProvider : IDisposable
    {
        Task<RemoteCacheSetResult> SetAsync<T>(RemoteCacheItem<T> cacheItem);

        RemoteCacheSetResult Set<T>(RemoteCacheItem<T> cacheItem);

        bool UpdateExpiry(string key, TimeSpan ttl);

        Task<RemoteCacheItem<T>> GetAsync<T>(string key);

        RemoteCacheItem<T> Get<T>(string key);

        Task<long> RemoveAsync(params string[] keys);

        long Remove(params string[] keys);

        bool IsInWorkingState();
    }
}
