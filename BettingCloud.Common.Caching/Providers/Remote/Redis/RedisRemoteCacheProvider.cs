﻿using BettingCloud.Common.Caching.Context;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using BettingCloud.Common.Caching.Providers.Remote.Redis.Serialization;
//using BettingCloud.Common.Diagnostics.Logging;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Polly;
using Polly.CircuitBreaker;

namespace BettingCloud.Common.Caching.Providers.Remote.Redis
{
    internal class RedisRemoteCacheProvider : BaseProvider, IRemoteCacheProvider
    {
        private IDatabase _client;
        private ConnectionMultiplexer _connection;
        private readonly IMessageSerializer _serializer;
        private readonly LuaScript _setScript;
        private readonly LuaScript _updateExpiryScript;

        private readonly CircuitBreakerPolicy _ciruitBreaker;
        private readonly CircuitBreakerPolicy _ciruitBreakerAsync;

        private const int ExceptionsAllowedBeforeBreaking = 2;
        private static readonly TimeSpan DurationOfBreak = TimeSpan.FromMinutes(1);

        private const string UnhandledException = "An unhandled exception occured.";
        private const string ConnectionFailedError = "Connection to Redis has failed due to: {0}";
        private const string ConnectionRestoredError = "Connection to Redis has been restored.";

        public RedisRemoteCacheProvider(IClientContext context) : base(context)
        {
            _serializer = new GZipMessageSerializer(
                new JsonMessageSerializer(),
                Settings.UseCompression,
                Settings.UseCompressionByteSizeThreshold);

            _setScript = LuaScript.Prepare(LuaScripts.Set);
            _updateExpiryScript = LuaScript.Prepare(LuaScripts.UpdateExpiry);

            var policyBuilder = Polly.Policy
                .Handle<RedisServerException>()
                .Or<RedisConnectionException>()
                .Or<RedisTimeoutException>()
                .Or<RedisException>()
                .Or<SocketException>();

            _ciruitBreaker = policyBuilder
                .CircuitBreaker(
                    ExceptionsAllowedBeforeBreaking,
                    DurationOfBreak,
                    onBreak: async (exception, circuitState) =>
                    {
                       // await context.LogProvider.ExceptionAsync(exception, "Remote cache not working.");
                    },
                    onReset: async () =>
                    {
                       // await context.LogProvider.LogAsync(LogEventLevel.Information, "Remote cache restored.");
                    }
                );

            _ciruitBreakerAsync = policyBuilder
                .CircuitBreakerAsync(
                    ExceptionsAllowedBeforeBreaking,
                    DurationOfBreak,
                    onBreak: async (exception, circuitState) =>
                    {
                       // await context.LogProvider.ExceptionAsync(exception, "Remote cache not working.");
                    },
                    onReset: async () =>
                    {
                       // await context.LogProvider.LogAsync(LogEventLevel.Information, "Remote cache restored.");
                    }
                );

            Initialize();
        }

        private void Initialize()
        {
            if (_connection == null ||
                _connection.IsConnected == false)
            {
                try
                {
                    _connection?.Dispose();
                    _connection = ConnectionMultiplexer.Connect(ConnectionString);
                    _connection.ConnectionFailed += OnConnectionFailed;
                    _connection.ConnectionRestored += OnConnectionRestored;

                    _client = _connection.GetDatabase(DatabaseId);
                }
                catch (Exception ex)
                {
                   // LogProvider.ExceptionAsync(ex, UnhandledException);
                }
            }
        }

        public bool IsInWorkingState()
        {
            return _connection?.IsConnected == true &&
                   _client != null &&
                   (_ciruitBreaker.CircuitState == CircuitState.Closed || _ciruitBreaker.CircuitState == CircuitState.HalfOpen) &&
                   (_ciruitBreakerAsync.CircuitState == CircuitState.Closed || _ciruitBreakerAsync.CircuitState == CircuitState.HalfOpen);
        }

        private string ConnectionString => Settings.RemoteRedisConnectionString;

        // ReSharper disable once PossibleInvalidOperationException
        private int DatabaseId => Settings.RemoteRedisDatabaseId.Value;

        public RemoteCacheItem<T> Get<T>(string key)
        {
            if (IsInWorkingState() == false)
            {
                return null;
            }

            try
            {
                HashEntry[] fields = null;

                _ciruitBreaker.Execute(() =>
                {
                    fields = _client.HashGetAll(key);
                });

                if (TryMapProperties(key, fields, out RemoteCacheItem<T> cacheItem) == false)
                {
                   // PerformanceProvider.HitAsync(key, Constants.Performance.RemoteMiss);
                    return cacheItem;
                }

               // PerformanceProvider.HitAsync(key, Constants.Performance.RemoteHit);
                return cacheItem;
            }
            catch (Exception ex)
            {
               // LogProvider.ExceptionAsync(ex, UnhandledException);
                return null;
            }
        }

        public async Task<RemoteCacheItem<T>> GetAsync<T>(string key)
        {
            if (IsInWorkingState() == false)
            {
                return null;
            }

            try
            {
                return await _ciruitBreakerAsync.ExecuteAsync(async () =>
                {
                    var fields = await _client.HashGetAllAsync(key);

                    if (TryMapProperties(key, fields, out RemoteCacheItem<T> cacheItem) == false)
                    {
                       // await PerformanceProvider.HitAsync(key, Constants.Performance.RemoteMiss);
                        return cacheItem;
                    }

                   // await PerformanceProvider.HitAsync(key, Constants.Performance.RemoteHit);
                    return cacheItem;
                });
            }
            catch (Exception ex)
            {
                //await LogProvider.ExceptionAsync(ex, UnhandledException);
                return null;
            }
        }

        public long Remove(params string[] keys)
        {
            const long defaultResult = 0L;

            if (IsInWorkingState() == false)
            {
                return defaultResult;
            }

            try
            {
                foreach (var key in keys)
                {
                   // PerformanceProvider.HitAsync(key, Constants.Performance.RemoteRemove);
                }

                var result = defaultResult;

                _ciruitBreaker.Execute(() =>
                {
                    result = _client.KeyDelete(keys.Select(k => (RedisKey)k).ToArray());
                });

                return result;
            }
            catch (Exception ex)
            {
               // LogProvider.ExceptionAsync(ex, UnhandledException);
                return defaultResult;
            }
        }

        public async Task<long> RemoveAsync(params string[] keys)
        {
            var defaultResult = 0L;

            if (IsInWorkingState() == false)
            {
                return defaultResult;
            }

            try
            {
                foreach (var key in keys)
                {
                  //  await PerformanceProvider.HitAsync(key, Constants.Performance.RemoteRemove);
                }

                return await _ciruitBreakerAsync.ExecuteAsync(() =>
                {
                    return _client.KeyDeleteAsync(keys.Select(k => (RedisKey)k).ToArray());
                });
            }
            catch (Exception ex)
            {
               // await LogProvider.ExceptionAsync(ex, UnhandledException);

                return defaultResult;
            }
        }

        public RemoteCacheSetResult Set<T>(RemoteCacheItem<T> cacheItem)
        {
            const int defaultResult = -1;

            if (IsInWorkingState() == false)
            {
                return RemoteCacheSetResult.Failed;
            }

            var key = cacheItem.Key;

            try
            {
                var data = _serializer.Serialize(cacheItem.Value);
                var parameters = new RedisRemoteCacheScriptParameters(key, cacheItem.Policy, cacheItem.Added, data);

                var result = RedisResult.Create(defaultResult);

                _ciruitBreaker.Execute(() =>
                {
                    result = _setScript.Evaluate(_client, parameters);
                });

               // PerformanceProvider.HitAsync(key, Constants.Performance.RemoteSet);

                if (result.IsNull)
                    return RemoteCacheSetResult.Failed;

                var resultValue = (int)result;

                switch (resultValue)
                {
                    case 0:
                        return RemoteCacheSetResult.Added;
                    case 1:
                        return RemoteCacheSetResult.Updated;
                    default:
                        return RemoteCacheSetResult.None;
                }
            }
            catch (Exception ex)
            {
               // LogProvider.ExceptionAsync(ex, UnhandledException);

                return RemoteCacheSetResult.Failed;
            }
        }

        public async Task<RemoteCacheSetResult> SetAsync<T>(RemoteCacheItem<T> cacheItem)
        {
            if (IsInWorkingState() == false)
            {
                return RemoteCacheSetResult.Failed;
            }

            var key = cacheItem.Key;

            try
            {
                return await _ciruitBreakerAsync.ExecuteAsync(async () =>
                {
                    var data = _serializer.Serialize(cacheItem.Value);
                    var parameters = new RedisRemoteCacheScriptParameters(key, cacheItem.Policy, cacheItem.Added, data);
                    var result = await _setScript.EvaluateAsync(_client, parameters);

                   // await PerformanceProvider.HitAsync(key, Constants.Performance.RemoteSet);

                    if (result.IsNull)
                        return RemoteCacheSetResult.Failed;

                    var resultValue = (int)result;

                    if (resultValue == 0)
                        return RemoteCacheSetResult.Added;

                    if (resultValue == 1)
                        return RemoteCacheSetResult.Updated;

                    return RemoteCacheSetResult.None;
                });
            }
            catch (Exception ex)
            {
              //  await LogProvider.ExceptionAsync(ex, UnhandledException);

                return RemoteCacheSetResult.Failed;
            }
        }

        public bool UpdateExpiry(string key, TimeSpan ttl)
        {
            if (IsInWorkingState() == false)
            {
                return false;
            }

            try
            {
                var result = RedisResult.Create((int?)null);

                _ciruitBreaker.Execute(() =>
                {
                    var parameters = new RedisRemoteCacheScriptParameters(key, ttl);
                    result = _updateExpiryScript.Evaluate(_client, parameters);
                });

                return !result.IsNull;
            }
            catch (Exception ex)
            {
               // LogProvider.ExceptionAsync(ex, UnhandledException);
                return false;
            }
        }

        private bool TryMapProperties<T>(string key, HashEntry[] fields, out RemoteCacheItem<T> result)
        {
            result = RemoteCacheItem<T>.Default;
            if (fields == null || fields.Length == 0)
            {
                return false;
            }

            try
            {
                var policy = MapItemPolicy(fields);
                var added = DateTime.UtcNow;
                var val = default(T);

                foreach (var field in fields)
                {
                    switch (field.Name)
                    {
                        case RedisRemoteCacheConstants.AddedKey:
                            var addedTicks = (long?)field.Value;
                            if (addedTicks.HasValue && addedTicks.Value != RedisRemoteCacheConstants.NotPresent)
                            {
                                added = new DateTime(addedTicks.Value, DateTimeKind.Utc);
                            }
                            break;

                        case RedisRemoteCacheConstants.DataKey:
                            var data = (byte[])field.Value;

                            if (data != null)
                            {
                                if (data.GetType() == typeof(T))
                                {
                                    val = (T)Convert.ChangeType(data, typeof(T));
                                }
                                else
                                {
                                    val = _serializer.Deserialize<T>(data);
                                }
                            }
                            break;
                    }
                }

                result = new RemoteCacheItem<T>(key, val, null, null, policy, added);
                return true;
            }
            catch (Exception ex)
            {
              //  LogProvider.ExceptionAsync(ex, "Error mapping the Redis properties.");
                result = RemoteCacheItem<T>.Default;
                return false;
            }
        }

        private ItemPolicy MapItemPolicy(HashEntry[] fields)
        {
            if (fields == null || fields.Length == 0)
            {
                return Context.Settings.CachedPolicies.DefaultPolicy;
            }

            var policyName = "unmappedPolicy";
            TimeSpan? localTtl = null;
            var localIsSliding = false;
            TimeSpan? remoteTtl = null;
            var remoteIsSliding = false;

            foreach (var field in fields)
            {
                switch (field.Name)
                {
                    case RedisRemoteCacheConstants.PolicyName:
                        policyName = field.Value;
                        break;

                    case RedisRemoteCacheConstants.LocalTtlKey:
                        var localTicks = (long?)field.Value;
                        if (localTicks.HasValue && localTicks.Value != RedisRemoteCacheConstants.NotPresent)
                        {
                            localTtl = TimeSpan.FromSeconds(localTicks.Value);
                        }
                        break;

                    case RedisRemoteCacheConstants.LocalSlidingKey:
                        localIsSliding = (bool)field.Value;
                        break;

                    case RedisRemoteCacheConstants.RemoteTtlKey:
                        var remoteTicks = (long?)field.Value;
                        if (remoteTicks.HasValue && remoteTicks.Value != RedisRemoteCacheConstants.NotPresent)
                        {
                            remoteTtl = TimeSpan.FromSeconds(remoteTicks.Value);
                        }
                        break;

                    case RedisRemoteCacheConstants.RemoteSlidingKey:
                        remoteIsSliding = (bool)field.Value;
                        break;
                }
            }

            var policy = Context.Settings.CachedPolicies.Get(policyName);

            if (policy.Name.Equals(Context.Settings.CachedPolicies.DefaultPolicy.Name) == false)
            {
                return policy;
            }

            var policyBuilder = new ItemPolicyBuilder();

            if (localTtl.HasValue)
            {
                policyBuilder.LocalPolicy(localTtl.Value, localIsSliding);
            }

            if (remoteTtl.HasValue)
            {
                policyBuilder.RemotePolicy(remoteTtl.Value, remoteIsSliding);
            }

            return policyBuilder.Create(policyName);
        }

        private void OnConnectionRestored(object sender, ConnectionFailedEventArgs e)
        {
          //  LogProvider.LogAsync(LogEventLevel.Information, ConnectionRestoredError);

            Initialize();
        }

        private void OnConnectionFailed(object sender, ConnectionFailedEventArgs e)
        {
           // LogProvider.ExceptionAsync(e.Exception, string.Format(ConnectionFailedError, e.FailureType));
        }

        #region IDisposable implementation

        private bool _isDisposing;

        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposing)
            {
                return;
            }

            if (disposing)
            {
                _isDisposing = true;

                if (_connection != null)
                {
                    _connection.Close();
                    _connection.Dispose();
                }
            }

        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
