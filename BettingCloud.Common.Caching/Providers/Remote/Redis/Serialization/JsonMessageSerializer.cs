﻿using Newtonsoft.Json;
using System.Text;

namespace BettingCloud.Common.Caching.Providers.Remote.Redis.Serialization
{
    internal class JsonMessageSerializer : IMessageSerializer
    {
        private readonly JsonSerializerSettings _jsonSettings;
        private readonly UTF8Encoding _encoder;

        public JsonMessageSerializer()
        {
            _jsonSettings = new JsonSerializerSettings {
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };

            _encoder = new UTF8Encoding();
        }

        public byte[] Serialize<T>(T item)
        {
            var json = JsonConvert.SerializeObject(item, _jsonSettings);
            return _encoder.GetBytes(json);
        }

        public T Deserialize<T>(byte[] source)
        {
            var json = _encoder.GetString(source);

            if (typeof(T) == typeof(ulong))
            {
                return (T)(object)ulong.Parse(json);
            }

            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
