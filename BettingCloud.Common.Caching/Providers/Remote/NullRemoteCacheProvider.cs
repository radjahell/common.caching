﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BettingCloud.Common.Caching.Enumerations;

namespace BettingCloud.Common.Caching.Providers.Remote
{
    internal class NullRemoteCacheProvider : IRemoteCacheProvider
    {
        public void Dispose()
        {
        }

        public bool IsInWorkingState()
        {
            return true;
        }

        public RemoteCacheItem<T> Get<T>(string key)
        {
            return null;
        }

        public Task<RemoteCacheItem<T>> GetAsync<T>(string key)
        {
            return Task.FromResult(default(RemoteCacheItem<T>));
        }

        public long Remove(params string[] keys)
        {
            return 0L;
        }

        public Task<long> RemoveAsync(params string[] keys)
        {
            return Task.FromResult(0L);
        }

        public RemoteCacheSetResult Set<T>(RemoteCacheItem<T> cacheItem)
        {
            return RemoteCacheSetResult.Failed;
        }

        public Task<RemoteCacheSetResult> SetAsync<T>(RemoteCacheItem<T> cacheItem)
        {
            return Task.FromResult(RemoteCacheSetResult.Failed);
        }

        public bool UpdateExpiry(string key, TimeSpan ttl)
        {
            return true;
        }
    }
}
