﻿using BettingCloud.Common.Caching.Context;
using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Locking;
using BettingCloud.Common.Caching.Providers.Local;
using BettingCloud.Common.Caching.Providers.Notifications;
using BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ;
using BettingCloud.Common.Caching.Providers.Notifications.Redis;
using BettingCloud.Common.Caching.Providers.Remote;
using BettingCloud.Common.Caching.Providers.Remote.Redis;
using EnsureThat;

namespace BettingCloud.Common.Caching.Providers
{
    internal static class ProviderFactory
    {
        private static readonly ILocalCacheProvider NullLocalCacheProvider = new NullLocalCacheProvider();
        private static readonly IRemoteCacheProvider NullRemoteCacheProvider = new NullRemoteCacheProvider();
        private static readonly INotificationProvider NullNotificationProvider = new NullNotificationProvider();

        internal static ILocalCacheProvider NewLocal(IClientContext context)
        {
            Ensure.That(context, Constants.Validation.Keys.Context).IsNotNull();

            if (context.Settings.LocalCacheEnabled)
            {
                return new MemoryCacheProvider(context);
            }

            return NullLocalCacheProvider;
        }

        internal static IRemoteCacheProvider NewRemote(IClientContext context)
        {
            Ensure.That(context, Constants.Validation.Keys.Context).IsNotNull();

            if (context.Settings.RemoteCacheEnabled == false)
            {
                return NullRemoteCacheProvider;
            }

            switch (context.Settings.RemoteCacheType)
            {
                case RemoteCacheType.Redis:
                    return new RedisRemoteCacheProvider(context);

                default:
                    return NullRemoteCacheProvider;
            }
        }

        internal static INotificationProvider NewNotifier(IClientContext context)
        {
            Ensure.That(context, Constants.Validation.Keys.Context).IsNotNull();

            if (context.Settings.RemoteCacheEnabled == false)
            {
                return NullNotificationProvider;
            }

            switch (context.Settings.NotificationType)
            {
                case NotificationType.RabbitMq:
                    return new RabbitMQNotificationProvider(context);

                case NotificationType.Redis:
                    return new RedisNotificationProvider(context);

                default:
                    return NullNotificationProvider;
            }
        }

        public static ILockingService NewLocker(IClientContext context)
        {
            Ensure.That(context, Constants.Validation.Keys.Context).IsNotNull();

            var lockingService = new LockingService();
            return lockingService;
        }
    }
}
