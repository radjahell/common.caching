﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers
{
    internal interface ICacheItem<T>
    {
        /// <summary>
        /// Cache item Key.
        /// </summary>
        string Key { get; }
        
        /// <summary>
        /// Cache item value.
        /// </summary>
        T Value { get; }

        Func<T> Fetch { get; }

        Func<Task<T>> FetchAsync { get; }

        /// <summary>
        /// Cache item policy.
        /// </summary>
        ItemPolicy Policy { get; }
        
        /// <summary>
        /// Added date (used for RA).
        /// </summary>
        DateTime Added { get; }

        /// <summary>
        /// Indicates if the inner value is Null or Default(generic)
        /// </summary>
        bool IsNullValue { get; }

        /// <summary>
        /// Cache item location (local/remote).
        /// </summary>
        CacheLocationType Location { get; }
    }
}
