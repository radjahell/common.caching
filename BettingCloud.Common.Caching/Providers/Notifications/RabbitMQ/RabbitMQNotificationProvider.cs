﻿using BettingCloud.Common.Caching.Context;
using BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ.Messages;
//using BettingCloud.Common.Diagnostics.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ
{
    internal class RabbitMQNotificationProvider : BaseProvider, INotificationProvider
    {
        private IConnection _connection;
        private IModel _channel;
        private QueueDeclareOk _queue;
        private EventingBasicConsumer _consumer;
        private bool _isDisposing;
        private CancellationTokenSource _cancellationToken;

        private const string ExchangeTypeFanout = "fanout";
        private const string MessageTypeHeader = "x-message-type";
        private const string UnhandledException = "RabbitMQ connection throw an unhandled exception.";
        private const string ReceivedMessageWithoutTypeHeaderException = "Received a message wihtout the type header.";
        private const string MessageTypeNameNotFoundException = "Message type {0} cannot be found.";

        private static string CacheKeyRemoveTypeName = typeof(CacheKeyRemoveNotification).FullName;
        private static string CacheKeyInvalidatedTypeName = typeof(CacheKeyInvalidatedNotification).FullName;
        private static string CacheKeyUpdatedTypeName = typeof(CacheKeyUpdatedNotification).FullName;
        private static string CacheFlushTypeName = typeof(CacheFlushNotification).FullName;
        private static double MaximumReconnectTimeout = TimeSpan.FromMinutes(5).TotalMilliseconds;

        public event CacheKeyRemovedEventHandler KeyRemoved;
        public event CacheKeyInvalidatedEventHandler KeyInvalidated;
        public event CacheKeyUpdatedEventHandler KeyUpdated;
        public event CacheFlushEventHandler Flushed;

        public RabbitMQNotificationProvider(IClientContext context) : base(context)
        {
            _cancellationToken = new CancellationTokenSource();

            Connect();
        }

        public bool IsInWorkingState()
        {
            throw new NotImplementedException();
        }

        private void Connect(bool calledFromReconnect = false)
        {
            try
            {
                var factory = new ConnectionFactory()
                {
                    HostName = Host,
                    UserName = Username,
                    Password = Password
                };

                if (string.IsNullOrWhiteSpace(VirtualHost) == false)
                {
                    factory.VirtualHost = VirtualHost;
                }

                if (Port.HasValue)
                {
                    factory.Port = Port.Value;
                }

                _connection = factory.CreateConnection();
                _connection.ConnectionShutdown += OnConnectionShutdown;

                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(
                    exchange: Exchange,
                    type: ExchangeTypeFanout,
                    durable: false,
                    autoDelete: false);

                _queue = _channel.QueueDeclare(
                    queue: QueueName,
                    durable: false,
                    autoDelete: true,
                    exclusive: true);

                _channel.QueueBind(queue: _queue.QueueName,
                                  exchange: Exchange,
                                  routingKey: string.Empty);

                _consumer = new EventingBasicConsumer(_channel);
                _consumer.Received += OnReceivedMessage;

                _channel.BasicConsume(queue: _queue.QueueName,
                                     autoAck: true,
                                     consumer: _consumer);
            }
            catch (Exception ex)
            {
                //LogProvider.ExceptionAsync(ex, UnhandledException);

                if (calledFromReconnect == false)
                {
                    Reconnect();
                }
            }
        }

        private void Reconnect()
        {
            if (IsConnected || _isDisposing)
            {
                return;
            }

            var manualReset = new ManualResetEventSlim(false);
            var timeout = 1000D;

            while (!manualReset.Wait(TimeSpan.FromMilliseconds(timeout), _cancellationToken.Token))
            {
                if (_isDisposing)
                {
                    break;
                }

                Cleanup();
                Connect(true);

                if (_isDisposing)
                {
                    break;
                }

                if (IsConnected)
                {
                    manualReset.Set(); // state set to true - breaks out of loop
                }
                else
                {
                    timeout = timeout * 2;

                    if (timeout > MaximumReconnectTimeout)
                    {
                        timeout = MaximumReconnectTimeout;
                    }
                }
            }
        }

        private void Cleanup()
        {
            if (_channel != null)
            {
                _channel.Dispose();
                _channel = null;
            }

            if (_connection != null)
            {
                _connection.ConnectionShutdown -= OnConnectionShutdown;
                _connection.Close(0);
                _connection.Dispose();
                _connection = null;
            }
        }

        private bool IsConnected
        {
            get
            {
                if (_connection?.IsOpen == true)
                {
                    return true;
                }

                return false;
            }
        }

        private void OnConnectionShutdown(object sender, ShutdownEventArgs e)
        {
#if NET
            if (e.Cause is ThreadAbortException)
            {
                // Do not try to reconnect
                return;
            }
#endif

          //  LogProvider.LogAsync(LogEventLevel.Warning, $"RabbitMQ connection was shutdown. {e.ReplyText}");

            Task.Run(() => Reconnect());
        }

        private string Host
        {
            get
            {
                return Context.Settings.NotificationRabbitMqHost;
            }
        }

        private int? Port
        {
            get
            {
                return Context.Settings.NotificationRabbitMqPort;
            }
        }

        private string Username
        {
            get
            {
                return Context.Settings.NotificationRabbitMqUsername;
            }
        }

        private string Password
        {
            get
            {
                return Context.Settings.NotificationRabbitMqPassword;
            }
        }

        private string Exchange
        {
            get
            {
                return Context.Settings.NotificationRabbitMqExchangeName;
            }
        }

        private string VirtualHost
        {
            get
            {
                return Context.Settings.NotificationRabbitMqVirtualHost;
            }
        }

        private string QueueName
        {
            get
            {
                return $"{Exchange}.{Environment.MachineName}.{Context.Id}";
            }
        }

        private void Publish<T>(T message) where T : INotificationMessage, new()
        {
            if (IsConnected == false)
            {
                return;
            }

            try
            {
                var json = JsonConvert.SerializeObject(message);
                var body = Encoding.UTF8.GetBytes(json);

                var headers = new Dictionary<string, object>();
                headers.Add(MessageTypeHeader, typeof(T).FullName);

                var properties = _channel.CreateBasicProperties();
                properties.Headers = headers;

                _channel.BasicPublish(exchange: Exchange,
                                        routingKey: string.Empty,
                                        basicProperties: properties,
                                        body: body);
            }
            catch (Exception ex)
            {
              //  LogProvider.ExceptionAsync(ex, UnhandledException);
            }
        }

        private void OnReceivedMessage(object model, BasicDeliverEventArgs args)
        {
            var typeName = Encoding.UTF8.GetString((byte[])args.BasicProperties.Headers[MessageTypeHeader]);
            
            if (string.IsNullOrWhiteSpace(typeName))
            {
               // LogProvider.LogAsync(LogEventLevel.Error, ReceivedMessageWithoutTypeHeaderException);
            }

            var type = Type.GetType(typeName, false);

            if (type == null)
            {
                var errMessage = string.Format(MessageTypeNameNotFoundException, typeName);
               // LogProvider.LogAsync(LogEventLevel.Error, errMessage);
            }

            var json = Encoding.UTF8.GetString(args.Body);
            var message = (INotificationMessage)JsonConvert.DeserializeObject(json, type);

            if (typeName.Equals(CacheKeyRemoveTypeName))
            {
                HandleMessage((CacheKeyRemoveNotification)message);
            }
            else if (typeName.Equals(CacheKeyInvalidatedTypeName))
            {
                HandleMessage((CacheKeyInvalidatedNotification)message);
            }
            else if (typeName.Equals(CacheKeyUpdatedTypeName))
            {
                HandleMessage((CacheKeyUpdatedNotification)message);
            }
            else if (typeName.Equals(CacheFlushTypeName))
            {
                HandleMessage((CacheFlushNotification)message);
            }
        }

        private void HandleMessage(CacheKeyRemoveNotification message)
        {
            KeyRemoved?.Invoke(this, new KeyEventArgs(message.Key));
        }

        private void HandleMessage(CacheKeyInvalidatedNotification message)
        {
            KeyInvalidated?.Invoke(this, new KeyEventArgs(message.Key));
        }

        private void HandleMessage(CacheKeyUpdatedNotification message)
        {
            KeyUpdated?.Invoke(this, new KeyEventArgs(message.Key));
        }

        private void HandleMessage(CacheFlushNotification message)
        {
            Flushed?.Invoke(this, EventArgs.Empty);
        }

        public void EmitFlushMessage()
        {
            Publish(new CacheFlushNotification()
            {
                Originator = Environment.MachineName,
                Timestamp = DateTime.UtcNow
            });
        }

        public void EmitInvalidateKeyMessage(string key)
        {
            Publish(new CacheKeyInvalidatedNotification()
            {
                Key = key,
                Originator = Environment.MachineName,
                Timestamp = DateTime.UtcNow
            });
        }

        public void EmitRemoveKeyMessage(string key)
        {
            Publish(new CacheKeyRemoveNotification()
            {
                Key = key,
                Originator = Environment.MachineName,
                Timestamp = DateTime.UtcNow
            });
        }

        public void EmitUpdatedKeyMessage(string key)
        {
            Publish(new CacheKeyUpdatedNotification()
            {
                Key = key,
                Originator = Environment.MachineName,
                Timestamp = DateTime.UtcNow
            });
        }

        public void Dispose()
        {
            _isDisposing = true;
            _cancellationToken.Cancel();

            Cleanup();
        }
    }
}
