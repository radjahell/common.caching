﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ.Messages
{
    public class CacheKeyInvalidatedNotification : INotificationMessage
    {
        public string Key { get; set; }

        public string Originator { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
