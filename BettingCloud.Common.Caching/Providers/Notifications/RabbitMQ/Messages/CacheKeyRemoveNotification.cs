﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ.Messages
{
    public class CacheKeyRemoveNotification : INotificationMessage
    {
        public string Key { get; set; }

        public string Originator { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
