﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ.Messages
{
    public class CacheFlushNotification : INotificationMessage
    {
        public string Originator { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
