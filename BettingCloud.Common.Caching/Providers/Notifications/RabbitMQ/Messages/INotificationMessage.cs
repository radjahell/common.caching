﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications.RabbitMQ.Messages
{
    internal interface INotificationMessage
    {
        string Originator { get; }

        DateTime Timestamp { get; }
    }
}
