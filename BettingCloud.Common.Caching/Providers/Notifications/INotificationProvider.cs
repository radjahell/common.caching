﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications
{
    internal interface INotificationProvider : IDisposable
    {
        event CacheKeyRemovedEventHandler KeyRemoved;

        event CacheKeyInvalidatedEventHandler KeyInvalidated;

        event CacheKeyUpdatedEventHandler KeyUpdated;

        event CacheFlushEventHandler Flushed;

        void EmitInvalidateKeyMessage(string key);

        void EmitRemoveKeyMessage(string key);

        void EmitUpdatedKeyMessage(string key);

        void EmitFlushMessage();

        bool IsInWorkingState();
    }
}
