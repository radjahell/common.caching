﻿namespace BettingCloud.Common.Caching.Providers.Notifications.Redis
{
    internal class RedisNotificationMessage
    {
        public string Channel { get; set; }

        public string Value { get; set; }
    }
}
