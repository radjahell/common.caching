﻿using BettingCloud.Common.Caching.Context;
//using BettingCloud.Common.Diagnostics.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Providers.Notifications.Redis
{
    internal class RedisNotificationProvider : BaseProvider, INotificationProvider
    {
        private ConnectionMultiplexer _connection;
        private ISubscriber _subscriber;

        private const string ConnectionFailedError = "Connection to Redis notifications has failed due to: {0}";
        private const string ConnectionRestoredError = "Connection to Redis notifications has been restored.";
        private const string ErrorMessageReceived = "Redis notifications received this error message from {1}: {0}";

        public event CacheFlushEventHandler Flushed;
        public event CacheKeyInvalidatedEventHandler KeyInvalidated;
        public event CacheKeyRemovedEventHandler KeyRemoved;
        public event CacheKeyUpdatedEventHandler KeyUpdated;

        private readonly object _internalLock = new object();
        private List<RedisNotificationMessage> _pendingMessages = new List<RedisNotificationMessage>();

        public RedisNotificationProvider(IClientContext context) : base(context)
        {
            Subscribe();
        }

        public bool IsInWorkingState()
        {
            return _connection?.IsConnected == true && _subscriber != null;
        }

        private string ConnectionString => Settings.NotificationRedisConnectionString;

        private void Subscribe()
        {
            lock (_internalLock)
            {
                if (_connection == null ||
                    _connection.IsConnected == false)
                {
                    _connection?.Dispose();
                    _connection = ConnectionMultiplexer.Connect(ConnectionString);
                    _connection.ConnectionFailed += OnConnectionFailed;
                    _connection.ConnectionRestored += OnConnectionRestored;
                    _connection.ErrorMessage += OnErrorMessage;
                }

                try
                {
                    _subscriber?.UnsubscribeAll();
                    _subscriber = null;
                }
                catch
                {
                    // Do Nothing
                }

                _subscriber = _connection.GetSubscriber();
                _subscriber.Subscribe(RedisNotificationConstants.InvalidationChannel, OnInvalidationMessage);
                _subscriber.Subscribe(RedisNotificationConstants.RemovedChannel, OnRemovedMessage);
                _subscriber.Subscribe(RedisNotificationConstants.UpdatedChannel, OnUpdatedMessage);
                _subscriber.Subscribe(RedisNotificationConstants.FlushChannel, OnFlushMessage);

                var keyEventDeleteChannel = string.Format(RedisNotificationConstants.KeyEventDeleteChannel, Settings.NotificationRedisDatabaseId);
                _subscriber.Subscribe(keyEventDeleteChannel, OnKeyEventMessage);

                var keyEventExpiredChannel = string.Format(RedisNotificationConstants.KeyEventExpiredChannel, Settings.NotificationRedisDatabaseId);
                _subscriber.Subscribe(keyEventExpiredChannel, OnKeyEventMessage);
            }
        }

        private void OnErrorMessage(object sender, RedisErrorEventArgs e)
        {
            var message = string.Format(ErrorMessageReceived, e.Message, e.EndPoint);

           // LogProvider.LogAsync(LogEventLevel.Warning, message);
        }

        private void OnConnectionRestored(object sender, ConnectionFailedEventArgs e)
        {
            //LogProvider.LogAsync(LogEventLevel.Information, ConnectionRestoredError);

            Subscribe();
            PublishPending();
        }

        private void OnConnectionFailed(object sender, ConnectionFailedEventArgs e)
        {
            //LogProvider.ExceptionAsync(e.Exception, string.Format(ConnectionFailedError, e.FailureType));
        }

        private void OnFlushMessage(RedisChannel channel, RedisValue message)
        {
            Flushed?.Invoke(this, EventArgs.Empty);
        }

        private void OnKeyEventMessage(RedisChannel channel, RedisValue message)
        {
            KeyRemoved?.Invoke(this, new KeyEventArgs(message.ToString()));
        }

        private void OnInvalidationMessage(RedisChannel channel, RedisValue message)
        {
            KeyInvalidated?.Invoke(this, new KeyEventArgs(message.ToString()));
        }

        private void OnRemovedMessage(RedisChannel channel, RedisValue message)
        {
            KeyRemoved?.Invoke(this, new KeyEventArgs(message.ToString()));
        }

        private void OnUpdatedMessage(RedisChannel channel, RedisValue message)
        {
            KeyUpdated?.Invoke(this, new KeyEventArgs(message.ToString()));
        }

        public void EmitFlushMessage()
        {
            Publish(RedisNotificationConstants.FlushChannel, string.Empty);
        }

        public void EmitInvalidateKeyMessage(string key)
        {
            Publish(RedisNotificationConstants.InvalidationChannel, key);
        }

        public void EmitRemoveKeyMessage(string key)
        {
            Publish(RedisNotificationConstants.RemovedChannel, key);
        }

        public void EmitUpdatedKeyMessage(string key)
        {
            Publish(RedisNotificationConstants.UpdatedChannel, key);
        }

        private void PublishPending()
        {
            List<RedisNotificationMessage> pendingMessages;

            lock (_internalLock)
            {
                pendingMessages = _pendingMessages;
                _pendingMessages = new List<RedisNotificationMessage>();
            }

            foreach (var pendingMessage in pendingMessages)
            {
                Publish(pendingMessage.Channel, pendingMessage.Value);
            }
        }

        private void Publish(string channel, string value)
        {
            lock (_internalLock)
            {
                if (IsInWorkingState())
                {
                    var distributedChannel = new RedisDistributedChannel(channel, Context.Id);
                    _subscriber.PublishAsync(distributedChannel, value);
                }
                else
                {
                    _pendingMessages.Add(new RedisNotificationMessage()
                    {
                        Channel = channel,
                        Value = value
                    });
                }
            }
        }

        #region IDisposable implementation

        private bool _isDisposing;

        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposing || !disposing)
                return;

            _isDisposing = true;

            _subscriber?.UnsubscribeAll();
            _connection?.Close();
            _connection?.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
