﻿namespace BettingCloud.Common.Caching.Providers.Notifications.Redis
{
    internal static class RedisNotificationConstants
    {
        public const char ChannelSeparator = '.';
        public const string InvalidationChannel = "dc_invalidate.*";
        public const string RemovedChannel = "dc_removed.*";
        public const string UpdatedChannel = "dc_updated.*";
        public const string FlushChannel = "dc_flush.*";
        public const string KeyEventDeleteChannel = "__keyevent@{0}__:del*";
        public const string KeyEventExpiredChannel = "__keyevent@{0}__:expired*";
    }
}
