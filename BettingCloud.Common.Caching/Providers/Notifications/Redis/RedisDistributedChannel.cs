﻿using StackExchange.Redis;
using System;

namespace BettingCloud.Common.Caching.Providers.Notifications.Redis
{
    internal class RedisDistributedChannel
    {
        public Guid ClientId { get; set; }

        public RedisChannel Channel { get; set; }

        public static implicit operator RedisDistributedChannel(RedisChannel value)
        {
            var parts = value.ToString().Split(RedisNotificationConstants.ChannelSeparator);
            Guid id;
            Guid.TryParse(parts[1], out id);
            return new RedisDistributedChannel(parts[0], id);
        }

        public static implicit operator RedisChannel(RedisDistributedChannel value)
        {
            return value.Channel.ToString().Replace("*", value.ClientId.ToString());
        }

        public RedisDistributedChannel(RedisChannel channel, Guid clientId)
        {
            Channel = channel;
            ClientId = clientId;
        }
    }
}
