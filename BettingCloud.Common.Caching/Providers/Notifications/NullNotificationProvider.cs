﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications
{
    internal class NullNotificationProvider : INotificationProvider
    {
        public event CacheFlushEventHandler Flushed;
        public event CacheKeyInvalidatedEventHandler KeyInvalidated;
        public event CacheKeyRemovedEventHandler KeyRemoved;
        public event CacheKeyUpdatedEventHandler KeyUpdated;

        public void Dispose()
        {
        }

        public bool IsInWorkingState()
        {
            return true;
        }

        public void EmitFlushMessage()
        {
        }

        public void EmitInvalidateKeyMessage(string key)
        {
        }

        public void EmitRemoveKeyMessage(string key)
        {
        }

        public void EmitUpdatedKeyMessage(string key)
        {
        }
    }
}
