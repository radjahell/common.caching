﻿using System;

namespace BettingCloud.Common.Caching.Providers.Notifications
{
    public delegate void CacheKeyRemovedEventHandler(object sender, KeyEventArgs e);

    public delegate void CacheKeyInvalidatedEventHandler(object sender, KeyEventArgs e);

    public delegate void CacheKeyUpdatedEventHandler(object sender, KeyEventArgs e);

    public delegate void CacheFlushEventHandler(object sender, EventArgs e);

    public class KeyEventArgs : EventArgs
    {
        public KeyEventArgs(string key)
        {
            Key = key;
        }

        public string Key { get; }
    }
}
