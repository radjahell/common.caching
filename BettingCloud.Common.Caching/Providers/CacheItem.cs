﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Extensions;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers
{
    internal class CacheItem<T> : ICacheItem<T>
    {
        /// <summary>
        /// A default and readonly empty value.
        /// </summary>
        internal static readonly CacheItem<T> Default = new CacheItem<T>(string.Empty, default(T), null, null, ItemPolicy.Default, DateTime.MinValue);

        /// <summary>
        /// Default ctor.
        /// </summary>
        /// <param name="key">Cache item key.</param>
        /// <param name="value">Cache item value.</param>
        /// <param name="fetch">Fetch function to get the data.</param>
        /// <param name="fetchAsync">Fetch function returning a Task with the data.</param>
        /// <param name="policy">Cache item policy.</param>
        /// <param name="added">Added date (for RA)</param>
        protected CacheItem(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy, DateTime added)
        {
            Key = key;
            Value = value;
            Policy = policy;
            Fetch = fetch;
            FetchAsync = fetchAsync;
            Added = added;
        }

        public Func<T> Fetch { get; }

        public Func<Task<T>> FetchAsync { get; }

        /// <summary>
        /// Cache item Key.
        /// </summary>
        public string Key { get; }
        
        /// <summary>
        /// Cache item value.
        /// </summary>
        public T Value { get; internal set; }
        
        /// <summary>
        /// Cache item policy.
        /// </summary>
        public ItemPolicy Policy { get; }
        
        /// <summary>
        /// Added date (used for RA).
        /// </summary>
        public DateTime Added { get; }
        
        /// <summary>
        /// internal stale date.
        /// </summary>
        protected DateTime StaleAt = DateTime.MaxValue;

        /// <summary>
        /// Cache item location (local/remote).
        /// </summary>
        public virtual CacheLocationType Location => CacheLocationType.None;

        /// <summary>
        /// Indicates if this item is stale or not.
        /// </summary>
        public virtual bool IsStale => StaleAt < DateTime.UtcNow;

        /// <summary>
        /// Indicates if the inner value is Null or Default(generic)
        /// </summary>
        public bool IsNullValue => Value.IsNullOrDefault();
    }
}
