﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Context;
//using BettingCloud.Common.Diagnostics.Logging;
//using BettingCloud.Common.Diagnostics.Performance;

namespace BettingCloud.Common.Caching.Providers
{
    internal abstract class BaseProvider
    {
        internal const string InitializationContextKey = "Initialization";
        internal const string GetContextKey = "Get";
        internal const string SetContextKey = "Set";
        internal const string RemoveContextKey = "Remove";
        internal const string UpdateExpiryContextKey = "UpdateExpiry";

        protected BaseProvider(IClientContext context)
        {
            Context = context;
        }

        protected IClientContext Context { get; }

        protected IClientSettings Settings => Context?.Settings;

      //  protected ILogProvider LogProvider => Context.Settings.LogProvider;

      //  protected IPerformanceProvider PerformanceProvider => Context.Settings.PerformanceProvider;
    }
}
