﻿using BettingCloud.Common.Caching.Context;
using BettingCloud.Common.Caching.Policy;
using System;

namespace BettingCloud.Common.Caching.Providers.Local
{
#if NETCORE

    using Microsoft.Extensions.Caching.Memory;
    using System.Threading.Tasks;

    internal class MemoryCacheProvider : BaseProvider, ILocalCacheProvider
    {
        private IMemoryCache _cache;
        private static readonly object SyncLock = new object();

        public MemoryCacheProvider(IClientContext context) : base(context)
        {
            _cache = new MemoryCache(new MemoryCacheOptions());
        }

        public bool Contains(string key)
        {
            return _cache.TryGetValue(key, out object _);
        }

        public void Dispose()
        {
            _cache.Dispose();
        }

        public void Flush()
        {
            lock(SyncLock)
            {
                var currentCache = _cache;
                _cache = new MemoryCache(new MemoryCacheOptions());
                currentCache.Dispose();
            }
        }

        public LocalCacheItem<T> Get<T>(string key)
        {
            var item = _cache.Get<LocalCacheItem<T>>(key);

            if (item == null || item.IsStale)
            {
                //PerformanceProvider.HitAsync(key, Constants.Performance.LocalMiss);
                return LocalCacheItem<T>.Default;
            }

           // PerformanceProvider.HitAsync(key, Constants.Performance.LocalHit);

            item.SetLastAccessed();
            return item;
        }

        public void Remove(string key)
        {
            _cache.Remove(key);

          //  PerformanceProvider.HitAsync(key, Constants.Performance.LocalRemove);
        }

        public void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions();

            if (policy.Local.Ttl.HasValue)
            {
                if (policy.Local.IsSliding)
                {
                    cacheEntryOptions.SlidingExpiration = policy.Local.Ttl.Value;
                }
                else
                {
                    var ttlInTicks = policy.Local.Ttl.Value.Ticks;
                    cacheEntryOptions.AbsoluteExpiration = DateTime.UtcNow.AddTicks(ttlInTicks);
                }
            }

            var cacheItem = new LocalCacheItem<T>(key, value, fetch, fetchAsync, policy, DateTime.UtcNow);

            _cache.Set(key, cacheItem, cacheEntryOptions);
          //  PerformanceProvider.HitAsync(key, Constants.Performance.LocalSet);
        }
    }

#elif NET

    using System.Runtime.Caching;
    using System.Threading.Tasks;

    internal class MemoryCacheProvider : BaseProvider, ILocalCacheProvider
    {
        private MemoryCache _cache;
        private static readonly object syncLock = new object();

        public MemoryCacheProvider(IClientContext context) : base(context)
        {
            _cache = new MemoryCache(Context.Id.ToString());
        }

        public bool Contains(string key)
        {
            return _cache[key] != null;
        }

        public void Flush()
        {
            lock(syncLock)
            {
                var currentCache = _cache;
                _cache = new MemoryCache(Context.Id.ToString());
                currentCache.Dispose();
            }
        }

        public LocalCacheItem<T> Get<T>(string key)
        {
            if (_cache.Contains(key) == false)
            {
               // PerformanceProvider.HitAsync(key, Constants.Performance.LocalMiss);
                return LocalCacheItem<T>.Default;
            }

            var item = _cache.Get(key) as LocalCacheItem<T>;

            if (item == null || item.IsStale)
            {
              //  PerformanceProvider.HitAsync(key, Constants.Performance.LocalMiss);
                return LocalCacheItem<T>.Default;
            }

           // PerformanceProvider.HitAsync(key, Constants.Performance.LocalHit);

            item.SetLastAccessed();
            return item;
        }

        /// <summary>
        /// Removes an item from local cache
        /// </summary>
        /// <param name="key">Key to remove (without namespace)</param>
        public void Remove(string key)
        {
            _cache.Remove(key);

          //  PerformanceProvider.HitAsync(key, Constants.Performance.LocalRemove);
        }

        /// <summary>
        /// Places an item of type T into local cache for the specified duration
        /// </summary>
        /// <param name="key">cache key</param>
        /// <param name="value">cache item value</param>
        /// <param name="policy">Caching policy</param>
        public void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy)
        {
            var cachePolicy = new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default 
            };

            if (policy.Local.Ttl.HasValue)
            {
                if (policy.Local.IsSliding)
                {
                    cachePolicy.SlidingExpiration = policy.Local.Ttl.Value;
                }
                else
                {
                    var ttlInTicks = policy.Local.Ttl.Value.Ticks;
                    cachePolicy.AbsoluteExpiration = DateTime.UtcNow.AddTicks(ttlInTicks);
                }
            }

            var cacheItem = new LocalCacheItem<T>(key, value, fetch, fetchAsync, policy, DateTime.UtcNow);

            _cache.Set(key, cacheItem, cachePolicy);
          //  PerformanceProvider.HitAsync(key, Constants.Performance.LocalSet);
        }

        public long Count()
        {
            return _cache.GetCount();
        }

        #region IDisposable

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_cache != null)
                        _cache.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion
    }

#endif
}

