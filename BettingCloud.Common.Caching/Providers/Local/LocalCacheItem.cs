﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers.Local
{
    internal class LocalCacheItem<T> : CacheItem<T>
    {
        /// <summary>
        /// A default readonly empty value.
        /// </summary>
        public new static readonly LocalCacheItem<T> Default = new LocalCacheItem<T>(string.Empty, default(T), null, null, ItemPolicy.Default, DateTime.UtcNow);

        /// <summary>
        /// Default ctor.
        /// </summary>
        /// <param name="key">Cache item key.</param>
        /// <param name="value">Cache item value.</param>
        /// <param name="fetch">Fetch function to get the data.</param>
        /// <param name="fetchAsync">Fetch function returning a Task with the data.</param>
        /// <param name="policy">Cache item policy.</param>
        /// <param name="added">Added date (for RA)</param>
        public LocalCacheItem(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy, DateTime added)
            : base(key, value, fetch, fetchAsync, policy, added)
        {
            if (policy != null)
            {
                StaleAt = Policy.Local.Ttl.HasValue && Policy.Local.Ttl != TimeSpan.MaxValue
                    ? Added.Add(Policy.Local.Ttl.Value) : DateTime.MaxValue;
            }

            LastAccessed = added;
        }

        public void SetLastAccessed()
        {
            LastAccessed = DateTime.UtcNow;
        }

        public DateTime LastAccessed { get; private set; }

        public override bool IsStale
        {
            get
            {
                if (Policy?.Local == null || Policy.Local.Ttl.HasValue == false)
                {
                    return false;
                }

                if (Policy.Local.IsSliding)
                {
                    return DateTime.UtcNow - LastAccessed > Policy.Local.Ttl.Value;
                }

                return DateTime.UtcNow - Added > Policy.Local.Ttl.Value;
            }
        }

        /// <summary>
        /// CacheItem location.
        /// </summary>
        public override CacheLocationType Location => CacheLocationType.Local;
    }
}
