﻿using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers.Local
{
    internal interface ILocalCacheProvider : IDisposable
    {
        bool Contains(string key);

        void Flush();

        void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy);

        LocalCacheItem<T> Get<T>(string key);

        void Remove(string key);
    }
}
