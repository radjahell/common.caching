﻿using BettingCloud.Common.Caching.Policy;
using System;
using System.Threading.Tasks;

namespace BettingCloud.Common.Caching.Providers.Local
{
    internal class NullLocalCacheProvider : ILocalCacheProvider
    {
        public bool Contains(string key)
        {
            return false;
        }

        public void Dispose() { }

        public void Flush() { }

        public LocalCacheItem<T> Get<T>(string key)
        {
            return null;
        }

        public void Remove(string key) { }

        public void Set<T>(string key, T value, Func<T> fetch, Func<Task<T>> fetchAsync, ItemPolicy policy) { }
    }
}
