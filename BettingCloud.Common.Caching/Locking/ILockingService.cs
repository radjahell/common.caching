﻿using System;

namespace BettingCloud.Common.Caching.Locking
{
    internal interface ILockingService : IDisposable
    {
        AsyncLock GetAsyncLockObject(string key);

        object GetLockObject(string key);
    }
}
