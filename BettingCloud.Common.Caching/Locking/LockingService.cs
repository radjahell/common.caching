﻿using System.Collections.Concurrent;

namespace BettingCloud.Common.Caching.Locking
{
    internal class LockingService : ILockingService
    {
        private readonly ConcurrentDictionary<string, object> _lockDictionary;
        private readonly ConcurrentDictionary<string, AsyncLock> _asyncLockDictionary;

        public LockingService()
        {
            _lockDictionary = new ConcurrentDictionary<string, object>();
            _asyncLockDictionary = new ConcurrentDictionary<string, AsyncLock>();
        }

        public object GetLockObject(string key)
        {
            return _lockDictionary.AddOrUpdate(key, k => new object(), (k, old) => old);
        }

        public AsyncLock GetAsyncLockObject(string key)
        {
            return _asyncLockDictionary.AddOrUpdate(key, k => new AsyncLock(), (k, old) => old);
        }

        public void Dispose()
        {
            _lockDictionary.Clear();
            _asyncLockDictionary.Clear();
        }
    }
}
