﻿using System;

namespace BettingCloud.Common.Caching.Locking
{
    internal class Lock : IDisposable
    {
        public static readonly Lock Empty = new Lock(null);

        private Func<bool> _unlocker;

        public bool Taken => _unlocker != null;

        public Lock(Func<bool> unlocker)
        {
            _unlocker = unlocker;
        }

        public bool Unlock()
        {
            if (_unlocker == null)
                return true;

            _unlocker = null;

            return true;
        }

        public void Dispose()
        {
            Unlock();
        }
    }
}
