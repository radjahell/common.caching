﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;
using System;
using System.Collections.Generic;

namespace BettingCloud.Common.Caching.Failure
{
    internal class FailureBehaviourRepository : IFailureBehaviourRepository
    {
        private readonly Dictionary<string, FailureBehaviour> _behaviours;

        public FailureBehaviourRepository()
        {
            Default = FailureBehaviour.NotSet;
            _behaviours = new Dictionary<string, FailureBehaviour>();
        }

        public FailureBehaviour Default { get; private set; }

        public void Add(FailureBehaviour behaviour, ItemPolicy policy)
        {
            Add(behaviour, policy?.Name);
        }

        public void Add(FailureBehaviour behaviour, string policyName)
        {
            if (string.IsNullOrWhiteSpace(policyName))
            {
                throw new ArgumentNullException(nameof(policyName));
            }

            if (_behaviours.ContainsKey(policyName))
            {
                throw new ArgumentException($"The policyName {policyName} has already been set.", nameof(policyName));
            }

            _behaviours.Add(policyName, behaviour);
        }

        public FailureBehaviour Get(ItemPolicy policy)
        {
            var policyName = policy?.Name;
            return Get(policyName);
        }

        public FailureBehaviour Get(string policyName)
        {
            if (string.IsNullOrWhiteSpace(policyName))
            {
                return Default;
            }

            FailureBehaviour behaviour;

            if (_behaviours.TryGetValue(policyName, out behaviour))
            {
                return behaviour;
            }

            return Default;
        }

        public void SetDefault(FailureBehaviour behaviour)
        {
            Default = behaviour;
        }
    }
}
