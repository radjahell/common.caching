﻿using BettingCloud.Common.Caching.Enumerations;
using BettingCloud.Common.Caching.Policy;

namespace BettingCloud.Common.Caching.Failure
{
    internal interface IFailureBehaviourRepository
    {
        void Add(FailureBehaviour behaviour, ItemPolicy policy);

        void Add(FailureBehaviour behaviour, string policyName);

        void SetDefault(FailureBehaviour behaviour);

        FailureBehaviour Default { get; }

        FailureBehaviour Get(ItemPolicy policy);

        FailureBehaviour Get(string policyName);
    }
}
