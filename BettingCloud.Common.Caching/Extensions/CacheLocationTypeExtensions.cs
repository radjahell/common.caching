﻿using BettingCloud.Common.Caching.Enumerations;

namespace BettingCloud.Common.Caching.Extensions
{
    internal static class CacheLocationTypeExtensions
    {
        public static bool Has(this CacheLocationType src, CacheLocationType other)
        {
            return (src & other) == other;
        }
    }
}
