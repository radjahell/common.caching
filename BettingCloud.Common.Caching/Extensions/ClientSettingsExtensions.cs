﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Enumerations;

namespace BettingCloud.Common.Caching.Extensions
{
    internal static class ClientSettingsExtensions
    {
        internal static CacheLocationType LocationType(this IClientSettings settings)
        {
            var locationType = CacheLocationType.None;

            if (settings != null)
            {
                if (settings.LocalCacheEnabled)
                {
                    locationType = locationType | CacheLocationType.Local;
                }

                if (settings.RemoteCacheEnabled)
                {
                    locationType = locationType | CacheLocationType.Remote;
                }
            }

            return locationType;
        }
    }
}
