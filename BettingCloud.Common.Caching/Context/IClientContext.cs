﻿using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Locking;
using BettingCloud.Common.Caching.Providers.Local;
using BettingCloud.Common.Caching.Providers.Notifications;
using BettingCloud.Common.Caching.Providers.Remote;
//using BettingCloud.Common.Diagnostics.Logging;
using System;

namespace BettingCloud.Common.Caching.Context
{
    internal interface IClientContext : IDisposable
    {
        IClientSettings Settings { get; }

        ILocalCacheProvider Local { get; }

        IRemoteCacheProvider Remote { get; }

        INotificationProvider Notifier { get; }

       // ILogProvider LogProvider { get; }

        ILockingService Locker { get; }

        Guid Id { get; }
    }
}
