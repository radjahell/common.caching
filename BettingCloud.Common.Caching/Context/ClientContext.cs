﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Configuration;
using BettingCloud.Common.Caching.Locking;
using BettingCloud.Common.Caching.Providers;
using BettingCloud.Common.Caching.Providers.Local;
using BettingCloud.Common.Caching.Providers.Notifications;
using BettingCloud.Common.Caching.Providers.Remote;
using BettingCloud.Common.Caching.Validations;
//using BettingCloud.Common.Diagnostics.Logging;
using EnsureThat;
using System;

namespace BettingCloud.Common.Caching.Context
{
    /// <summary>
    /// Keep current state configuration of the Cache.
    /// </summary>
    internal class ClientContext : IClientContext
    {
        public ClientContext(IClientSettings settings)
        {
            Ensure.That(settings, Constants.Validation.Keys.Settings).IsValid();

            Settings = settings;
            Id = Guid.NewGuid();
            Setup();
        }

        public ICacheClient Client { get; internal set; }

        public IClientSettings Settings { get; internal set; }

        public ILocalCacheProvider Local { get; internal set; }

        public IRemoteCacheProvider Remote { get; internal set; }

        public ILockingService Locker { get; internal set; }

        public INotificationProvider Notifier { get; internal set; }

        public Guid Id { get; }

       // public ILogProvider LogProvider { get; internal set; }

        public void Setup()
        {
            Local = ProviderFactory.NewLocal(this);
            Remote = ProviderFactory.NewRemote(this);
            Notifier = ProviderFactory.NewNotifier(this);
            Locker = ProviderFactory.NewLocker(this);
           // LogProvider = Settings.LogProvider;
        }

        public void Dispose()
        {
            if (Local != null)
            {
                var local = Local;
                Local = new NullLocalCacheProvider();
                local.Dispose();
            }

            if (Remote != null)
            {
                var remote = Remote;
                Remote = new NullRemoteCacheProvider();
                remote.Dispose();
            }

            if (Notifier != null)
            {
                var notifier = Notifier;
                Notifier = new NullNotificationProvider();
                notifier.Dispose();
            }
        }
    }
}
