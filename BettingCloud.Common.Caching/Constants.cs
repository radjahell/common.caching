﻿using BettingCloud.Common.Caching.Enumerations;
using System.Collections.Generic;
//using BettingCloud.Common.Diagnostics.Logging;
using Polly;
using StackExchange.Redis;

namespace BettingCloud.Common.Caching
{
    internal static class Constants
    {
        internal static class Client
        {
            public const string FetchException = "An error occured when executing the fetch function.";
        }

        internal static class Validation
        {
            internal static class Keys
            {
                public const string Key = "key";
                public const string Policy = "policy";
                public const string Name = "name";
                public const string Context = "context";
                public const string Settings = "settings";
                public const string Host = "host";
                public const string ConnectionString = "connectionString";
                public const string DatabaseId = "databaseId";
            }
        }

        internal static class Performance
        {
            internal static Dictionary<string, string> LocalHit = CreateAdditionalData("Hit", CacheLocationType.Local);
            internal static Dictionary<string, string> LocalRemove = CreateAdditionalData("Remove", CacheLocationType.Local);
            internal static Dictionary<string, string> LocalSet = CreateAdditionalData("Set", CacheLocationType.Local);
            internal static Dictionary<string, string> LocalMiss = CreateAdditionalData("Miss", CacheLocationType.Local);

            internal static Dictionary<string, string> RemoteHit = CreateAdditionalData("Hit", CacheLocationType.Remote);
            internal static Dictionary<string, string> RemoteRemove = CreateAdditionalData("Remove", CacheLocationType.Remote);
            internal static Dictionary<string, string> RemoteSet = CreateAdditionalData("Set", CacheLocationType.Remote);
            internal static Dictionary<string, string> RemoteMiss = CreateAdditionalData("Miss", CacheLocationType.Remote);

            internal static Dictionary<string, string> CreateAdditionalData(string type, CacheLocationType cacheLocation)
            {
                var data = new Dictionary<string, string>
                {
                    {"Type", type},
                    { "CacheLocation", cacheLocation.ToString()}
                };

                return data;
            }
        }
    }
}
