﻿using System;
using System.Runtime.Serialization;

namespace BettingCloud.Common.Caching.Exceptions
{
#if NET
	[Serializable]
#endif
	public class CacheProviderException : Exception
	{
#if NET
		public CacheProviderException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
#endif

		public CacheProviderException(string message) : base(message)
        {
        }

        public CacheProviderException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
