﻿using System;
using System.Runtime.Serialization;

namespace BettingCloud.Common.Caching.Exceptions
{
#if NET
	[Serializable]
#endif
	public class CacheFailureException : Exception
    {
#if NET
		public CacheFailureException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
#endif
		public CacheFailureException(string message) : base(message)
        {
        }

        public CacheFailureException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
