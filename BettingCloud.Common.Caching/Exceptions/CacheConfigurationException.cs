﻿using System;
using System.Runtime.Serialization;

namespace BettingCloud.Common.Caching.Exceptions
{
#if NET
	[Serializable]
#endif
	public class CacheConfigurationException : Exception
    {
#if NET
		public CacheConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
#endif
		public CacheConfigurationException(string message) : base(message)
        {
        }

        public CacheConfigurationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
