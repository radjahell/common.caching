﻿using System;
using System.Runtime.Serialization;

namespace BettingCloud.Common.Caching.Exceptions
{
#if NET
	[Serializable]
#endif
	public class CacheFetchException : Exception
    {
#if NET
		public CacheFetchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
#endif
		public CacheFetchException(string message) : base(message)
        {
        }

        public CacheFetchException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
