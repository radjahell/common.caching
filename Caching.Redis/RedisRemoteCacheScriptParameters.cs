﻿ 
using System;

namespace  Caching.Redis
{
    internal class RedisRemoteCacheScriptParameters
    {
        public RedisRemoteCacheScriptParameters(string key, TimeSpan remoteTtl)
        {
            Key = key;
            RemoteTtl = (long)remoteTtl.TotalSeconds;
            Ticks = DateTime.UtcNow.Ticks;
        }

        public RedisRemoteCacheScriptParameters(string key, DateTime added, byte[] data)
        {
            if (string.IsNullOrEmpty(key)) // || policy == null)
                return;

            Key = key;

            LocalTtl = 12;
            LocalSliding = true;

            RemoteTtl = 12;
            RemoteSliding = true;

            Ticks = added.Ticks;
            Data = data;
            PolicyName = "MyPolicy";
        }

        public string Key { get; }

        public long LocalTtl { get; }

        public bool LocalSliding { get; }

        public long RemoteTtl { get; }

        public bool RemoteSliding { get; }

        public byte[] Data { get; }

        public long Ticks { get; }

       public string PolicyName { get; }
    }
}
