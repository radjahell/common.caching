﻿using Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caching.Redis
{
    public static class RedisCacheExtension
    {
        public static ICache ConfigureRedis(this IRemoteCache cacheProvider, 
            string connectionString,
            int databaseId)
        {
            cacheProvider.Configure(connectionString, databaseId);
            return cacheProvider;
        } 
    }
}
