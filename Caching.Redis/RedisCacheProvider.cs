﻿
using Caching.Abstracts;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Caching.Redis
{
    public class RedisCacheProvider : IRemoteCache
    {
        private IDatabase _client;
        private ConnectionMultiplexer _connection;
        private string RedisConnectionString = "localhost:6379,abortConnect=false,syncTimeout=3000";
        private int RedisDatabaseId = 0;

        public RedisCacheProvider()
        {
            Initialize();
        }

        public async Task<bool> ContainsAsync(string key)
        {
            var value = await GetAsync<CacheItem<string>>(key);

            if (value != null)
                return true;
            return false;
        }

        public void Set<T>(string key, T value, ItemPolicy itemPolicy)
        {
            var cacheItem = new CacheItem<T>(key, value, DateTime.Now, itemPolicy.Ttl, itemPolicy.IsSliding);

            var result = JsonConvert.SerializeObject(cacheItem, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                TypeNameHandling = TypeNameHandling.Objects
            });


            _client.StringSet(key, result, itemPolicy.Ttl, When.Always, CommandFlags.FireAndForget);
        }

        public async Task<CacheItem<T>> GetAsync<T>(string key)
        {

            using (var semaphore = new SemaphoreSlim(1))
            {
                await semaphore.WaitAsync();
                try
                {
                    var value = await _client.StringGetAsync(key);
                    if (!value.HasValue)
                        return null;

                    var ttl = _client.KeyTimeToLive(key);
                    var result = JsonConvert.DeserializeObject<CacheItem<T>>(value, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        TypeNameHandling = TypeNameHandling.Objects
                    });

                    if (ttl < (result.Ttl / 2) && result.IsSliding == true)
                    {
                        Remove(key);
                        Set<T>(key, result.Value, new ItemPolicy { Ttl = result.Ttl, IsSliding = result.IsSliding });
                    }

                    return result;
                } 
                finally
                {

                    semaphore.Release();
                }
            }
        }

        public void Remove(string key)
        {
            _client.KeyDelete(key);
        }

        private void Initialize()
        {
            if (_connection == null ||
                _connection.IsConnected == false)
            {
                try
                {
                    _connection?.Dispose();
                    _connection = ConnectionMultiplexer.Connect(RedisConnectionString);
                    // _connection.ConnectionFailed += OnConnectionFailed;
                    // _connection.ConnectionRestored += OnConnectionRestored;

                    _client = _connection.GetDatabase(RedisDatabaseId);

                }
                catch (Exception ex)
                {
                    // LogProvider.ExceptionAsync(ex, UnhandledException);
                }
            }
        }

        public void Configure(string connectionString, int databaseId)
        {
            RedisConnectionString = connectionString;
            RedisDatabaseId = databaseId;
        }

    }
}
