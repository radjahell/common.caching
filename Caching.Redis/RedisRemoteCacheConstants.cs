﻿namespace Caching.Redis
{
    internal class RedisRemoteCacheConstants
    {
        //public const string LocalTtlKey = "l_ttl";
        //public const string LocalSlidingKey = "l_sld";
        //public const string RemoteTtlKey = "r_ttl";
        //public const string RemoteSlidingKey = "r_sld";
        public const string AddedKey = "added";
        public const string DataKey = "data";
       // public const string PolicyName = "p_name";
        public const long NotPresent = -1;
    }
}
