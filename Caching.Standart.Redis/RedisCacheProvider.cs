﻿using Caching.Standart.Abstracts;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;
using Caching.Standart.Redis.Infrastructure;

namespace Caching.Standart.Redis
{
    public class RedisCacheProvider : IRemoteCache
    {
        private IDatabase _client;
        private ConnectionMultiplexer _connection;
        private string RedisConnectionString = "localhost:6379,abortConnect=false,syncTimeout=3000";
        private readonly IMessageSerializer _serializer;
        private int RedisDatabaseId = 0;
        private readonly LuaScript _setScript;
        private const string LuaScriptSet = @"local keyExists = redis.call('EXISTS', @Key)
                                    redis.call('HMSET', @Key, 'l_ttl', @LocalTtl, 'l_sld', @LocalSliding, 'r_ttl', @RemoteTtl, 'r_sld', @RemoteSliding, 'added', @Ticks, 'data', @Data, 'p_name', @PolicyName)
                                    if @RemoteTtl ~= '-1' then
                                    redis.call('EXPIRE', @Key, @RemoteTtl)
                                    end
                                    return keyExists";



        public RedisCacheProvider()
        {
            _setScript = LuaScript.Prepare(LuaScriptSet);

            _serializer = new GZipMessageSerializer(
                new JsonMessageSerializer(),
                false,
                1000);
            Initialize();
        }

        public async Task<bool> ContainsAsync(string key)
        {
            var value = await GetAsync<CacheItem<string>>(key);

            if (value != null)
                return true;
            return false;
        }

        public void Set<T>(string key, T value, ItemPolicy itemPolicy)
        {
            RemoteCacheSetResult setResult;
            const int defaultResult = -1;

            var data = _serializer.Serialize(value);
            var parameters = new RedisRemoteCacheScriptParameters(key, itemPolicy, DateTime.Now, data);
            var result = RedisResult.Create(defaultResult);
            result = _setScript.Evaluate(_client, parameters);


            if (result.IsNull)
                setResult = RemoteCacheSetResult.Failed;

            var resultValue = (int)result;

            switch (resultValue)
            {
                case 0:
                    setResult = RemoteCacheSetResult.Added;
                    break;
                case 1:
                    setResult = RemoteCacheSetResult.Updated;
                    break;
                    
                default:
                    setResult = RemoteCacheSetResult.None;
                    break;
            }


            //var cacheItem = new CacheItem<T>(key, value, DateTime.Now, itemPolicy.Ttl, itemPolicy.IsSliding);

            //var result = JsonConvert.SerializeObject(cacheItem, Formatting.Indented, new JsonSerializerSettings
            //{
            //    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            //    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            //    TypeNameHandling = TypeNameHandling.Objects
            //});


            //_client.StringSet(key, result, itemPolicy.Ttl, When.Always, CommandFlags.FireAndForget);
        }

        public async Task<CacheItem<T>> GetAsync<T>(string key)
        {

            using (var semaphore = new SemaphoreSlim(1))
            {
                await semaphore.WaitAsync();
                try
                {

                    var fields = await _client.HashGetAllAsync(key);
                    if (TryMapProperties(key, fields, out CacheItem<T> cacheItem) == false)
                    {
                        // await PerformanceProvider.HitAsync(key, Constants.Performance.RemoteMiss);
                        return cacheItem;
                    }


                    //var value = await _client.StringGetAsync(key);
                    //if (!value.HasValue)
                    //    return null;

                    //var ttl = _client.KeyTimeToLive(key);
                    //var result = JsonConvert.DeserializeObject<CacheItem<T>>(value, new JsonSerializerSettings
                    //{
                    //    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    //    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    //    TypeNameHandling = TypeNameHandling.Objects
                    //});

                    //if (ttl < (result.Ttl / 2) && result.IsSliding == true)
                    //{
                    //    Remove(key);
                    //    Set<T>(key, result.Value, new ItemPolicy { Ttl = result.Ttl, IsSliding = result.IsSliding });
                    //}

                    return cacheItem;
                }
                finally
                {

                    semaphore.Release();
                }
            }
        }

        private bool TryMapProperties<T>(string key, HashEntry[] fields, out CacheItem<T> result)
        {
            result = CacheItem<T>.Default;
            if (fields == null || fields.Length == 0)
            {
                return false;
            }

            try
            {
                var policy = MapItemPolicy(fields);
                var added = DateTime.UtcNow;
                var val = default(T);

                foreach (var field in fields)
                {
                    switch (field.Name)
                    {
                        case RedisRemoteCacheConstants.AddedKey:
                            var addedTicks = (long?)field.Value;
                            if (addedTicks.HasValue && addedTicks.Value != RedisRemoteCacheConstants.NotPresent)
                            {
                                added = new DateTime(addedTicks.Value, DateTimeKind.Utc);
                            }
                            break;

                        case RedisRemoteCacheConstants.DataKey:
                            var data = (byte[])field.Value;

                            if (data != null)
                            {
                                if (data.GetType() == typeof(T))
                                {
                                    val = (T)Convert.ChangeType(data, typeof(T));
                                }
                                else
                                {
                                    val = _serializer.Deserialize<T>(data);
                                }
                            }
                            break;
                    }
                }
    
                result = new CacheItem<T>(key, val, DateTime.UtcNow, TimeSpan.FromSeconds(50), true);
                return true;
            }
            catch (Exception ex)
            {
                //  LogProvider.ExceptionAsync(ex, "Error mapping the Redis properties.");
                result = CacheItem<T>.Default;
                return false;
            }
        }

        private ItemPolicy MapItemPolicy(HashEntry[] fields)
        {
            if (fields == null || fields.Length == 0)
            {
                return ItemPolicy.Default;
            }

            var policyName = "unmappedPolicy";
            TimeSpan? localTtl = null;
            var localIsSliding = false;
            TimeSpan? remoteTtl = null;
            var remoteIsSliding = false;

            foreach (var field in fields)
            {
                switch (field.Name)
                {
                    case RedisRemoteCacheConstants.PolicyName:
                        policyName = field.Value;
                        break;

                    case RedisRemoteCacheConstants.LocalTtlKey:
                        var localTicks = (long?)field.Value;
                        if (localTicks.HasValue && localTicks.Value != RedisRemoteCacheConstants.NotPresent)
                        {
                            localTtl = TimeSpan.FromSeconds(localTicks.Value);
                        }
                        break;

                    case RedisRemoteCacheConstants.LocalSlidingKey:
                        localIsSliding = (bool)field.Value;
                        break;

                    case RedisRemoteCacheConstants.RemoteTtlKey:
                        var remoteTicks = (long?)field.Value;
                        if (remoteTicks.HasValue && remoteTicks.Value != RedisRemoteCacheConstants.NotPresent)
                        {
                            remoteTtl = TimeSpan.FromSeconds(remoteTicks.Value);
                        }
                        break;

                    case RedisRemoteCacheConstants.RemoteSlidingKey:
                        remoteIsSliding = (bool)field.Value;
                        break;
                }
            }

            //var policy = Context.Settings.CachedPolicies.Get(policyName);

            //if (policy.Name.Equals(Context.Settings.CachedPolicies.DefaultPolicy.Name) == false)
            //{
            //    return policy;
            //}

            var policyBuilder = new ItemPolicyBuilder();

            //if (localTtl.HasValue)
            //{
            //    policyBuilder.LocalPolicy(localTtl.Value, localIsSliding);
            //}

            if (remoteTtl.HasValue)
            {
                policyBuilder.RemotePolicy(remoteTtl.Value, remoteIsSliding);
            }

            return policyBuilder.Create(policyName);
        }

        public void Remove(string key)
        {
            _client.KeyDelete(key);
        }

        private void Initialize()
        {
            if (_connection == null ||
                _connection.IsConnected == false)
            {
                try
                {
                    _connection?.Dispose();
                    _connection = ConnectionMultiplexer.Connect(RedisConnectionString);
                    // _connection.ConnectionFailed += OnConnectionFailed;
                    // _connection.ConnectionRestored += OnConnectionRestored;

                    _client = _connection.GetDatabase(RedisDatabaseId);

                }
                catch (Exception ex)
                {
                    // LogProvider.ExceptionAsync(ex, UnhandledException);
                }
            }
        }

        public void Configure(string connectionString, int databaseId)
        {
            RedisConnectionString = connectionString;
            RedisDatabaseId = databaseId;
        }

    }
}
