﻿using Caching.Standart.Abstracts;
using System;

namespace Caching.Standart.Redis.Infrastructure
{
    internal class ItemPolicyBuilder  
    {
        private ItemPolicy _policy;

        public ItemPolicyBuilder()
        {
            _policy = new ItemPolicy();
        }

        

        

        public ItemPolicyBuilder RemotePolicy(TimeSpan ttl)
        {
            _policy = new ItemPolicy()
            {
                Ttl = ttl
            };

            return this;
        }

        public ItemPolicyBuilder RemotePolicy(TimeSpan ttl, bool isSliding)
        {
            _policy = new ItemPolicy()
            {
                Ttl = ttl,
                IsSliding = isSliding
            };

            return this;
        }

        public ItemPolicyBuilder RemotePolicy(int ttlInSecs)
        {
            _policy = new ItemPolicy
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs)
            };

            return this;
        }

        public ItemPolicyBuilder RemotePolicy(int ttlInSecs, bool isSliding)
        {
            _policy = new ItemPolicy()
            {
                Ttl = TimeSpan.FromSeconds(ttlInSecs),
                IsSliding = isSliding
            };

            return this;
        }

        public ItemPolicy Create(string name)
        {
            _policy.Name = name;
            return _policy;
        }
    }
}
