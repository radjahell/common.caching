﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caching.Standart.Redis.Infrastructure
{
    internal interface IMessageSerializer
    {
        /// <summary>
        /// Serialize item into bytes.
        /// </summary>
        /// <typeparam name="T">Type of item.</typeparam>
        /// <param name="item">Item to Serialize.</param>
        /// <returns>The serialized value</returns>
        byte[] Serialize<T>(T item);

        /// <summary>
        /// Deserialize bytes into item of Type T.
        /// </summary>
        /// <typeparam name="T">Requested type.</typeparam>
        /// <param name="source">Bytes to deserialize.</param>
        /// <returns>The deserialized item</returns>
        T Deserialize<T>(byte[] source);
    }
}
