﻿using System.IO;
using System.IO.Compression;

namespace Caching.Standart.Redis.Infrastructure
{
    internal class GZipMessageSerializer : IMessageSerializer
    {
        private readonly IMessageSerializer _serializer;
        private readonly int _compressBytesSizeThreshold;
        private readonly bool _useCompression;

        public GZipMessageSerializer(
            IMessageSerializer inner,
            bool useCompression,
            int compressBytesSizeThreshold)
        {
            _serializer = inner;
            _useCompression = useCompression;
            _compressBytesSizeThreshold = compressBytesSizeThreshold;
        }

        public T Deserialize<T>(byte[] source)
        {
            var data = Decompress(source);
            return _serializer.Deserialize<T>(data);
        }

        public byte[] Serialize<T>(T item)
        {
            var bytes = _serializer.Serialize(item);

            if (_useCompression)
            {
                if (bytes.Length > _compressBytesSizeThreshold)
                {
                    bytes = Compress(bytes);
                }
            }

            return bytes;
        }

        private static byte[] Compress(byte[] input)
        {
            if (input == null || input.Length == 0)
                return new byte[0];

            using (var ms = new MemoryStream())
            {
                using (var zipStream = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    zipStream.Write(input, 0, input.Length);
                }
                return ms.ToArray();
            }
        }

        private static byte[] Decompress(byte[] input)
        {
            if (input == null || input.Length == 0)
                return new byte[0];

            //gzip magic numbers 1F 8B
            if (input[0] != 31 && input[1] != 139)
                return input;

            const int size = 4096;
            var buffer = new byte[4096];
            using (var ms = new MemoryStream())
            {
                using (var zipStream = new GZipStream(new MemoryStream(input), CompressionMode.Decompress, false))
                {
                    int count;

                    do
                    {
                        count = zipStream.Read(buffer, 0, size);
                        if (count > 0)
                            ms.Write(buffer, 0, count);
                    }
                    while (count > 0);
                }
                return ms.ToArray();
            }
        }
    }
}
