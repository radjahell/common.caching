﻿using System;
using Caching.Standart.Abstracts;

namespace Caching.Standart.Redis.Infrastructure
{
    internal class RedisRemoteCacheScriptParameters
    {
        public RedisRemoteCacheScriptParameters(string key, TimeSpan remoteTtl)
        {
            Key = key;
            RemoteTtl = (long)remoteTtl.TotalSeconds;
            Ticks = DateTime.UtcNow.Ticks;
        }

        public RedisRemoteCacheScriptParameters(string key, ItemPolicy policy, DateTime added, byte[] data)
        {
            if (string.IsNullOrEmpty(key) || policy == null)
                return;

            Key = key;

            //LocalTtl = policy.Local.Ttl.HasValue ? (long)policy.Local.Ttl.Value.TotalSeconds : RedisRemoteCacheConstants.NotPresent;
            //LocalSliding = policy.Local.IsSliding;

            RemoteTtl = policy.Ttl.HasValue ? (long)policy.Ttl.Value.TotalSeconds : RedisRemoteCacheConstants.NotPresent;
            RemoteSliding = policy.IsSliding;

            Ticks = added.Ticks;
            Data = data;
            PolicyName = policy.Name;
        }

        public string Key { get; }

        public long LocalTtl { get; }

        public bool LocalSliding { get; }

        public long RemoteTtl { get; }

        public bool RemoteSliding { get; }

        public byte[] Data { get; }

        public long Ticks { get; }

        public string PolicyName { get; }
    }
}
