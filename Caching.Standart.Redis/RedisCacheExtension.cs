﻿using Caching.Standart.Abstracts;

namespace Caching.Standart.Redis
{
    public static class RedisCacheExtension
    {
        public static ICache ConfigureRedis(this IRemoteCache cacheProvider,
            string connectionString,
            int databaseId)
        {
            cacheProvider.Configure(connectionString, databaseId);
            return cacheProvider;
        }
    }
}
