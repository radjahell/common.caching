﻿using BettingCloud.Common.Caching.Client;
using BettingCloud.Common.Caching.Policy;
using Bogus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BettingCloud.Common.Caching.ConsoleTests.Extensions; 
using Timer = System.Timers.Timer;

namespace BettingCloud.Common.Caching.ConsoleTests
{
    class Program
    {
        //private const string RedisConnectionString = "redis.dev.bettingcloud.com:6379,abortConnect=false,syncTimeout=3000";
        private const string RedisConnectionString = "localhost:6379,abortConnect=false,syncTimeout=3000";
        private const int RedisDatabaseId = 0;
        private const int Iterations = 1;
        private static readonly Faker Faker = new Faker();

        static void Main(string[] args)
        {
            //TestConcurrency();

            //var client = Create("client", RedisConnectionString, RedisDatabaseId);

            //client.Get<string>("betradar:objectmapper:markettype:links:outcometype:38");


            var client = Create("client", RedisConnectionString, RedisDatabaseId);

            TestNormal(client, 10, TimeSpan.FromMilliseconds(50));

            WriteInfo(@"DONE");
            Console.ReadLine();
        }

        private static void TestConcurrency()
        {
            var client1 = Create("client1", RedisConnectionString, RedisDatabaseId);

            var client2 = Create("client2", RedisConnectionString, RedisDatabaseId);

            var concurrent1Task = TestConcurrency(client1);
            var concurrent2Task = TestConcurrency(client2);
            Task.WhenAll(concurrent1Task, concurrent2Task).Wait();

            AddTestData(client1);
            TestNormal(client1);
            TestCountInvalidations(client1, client2);
            TestInvalidation(client1, client2);
        }


        private static Task TestConcurrency(ICacheClient client)
        {
            var iterations = 100;
            var policy = ItemPolicy
                .Builder
                .LocalPolicy(TimeSpan.FromMinutes(10), false)
                .RemotePolicy(TimeSpan.FromMinutes(10), false)
                .Create("CacheFor10Minutes");

            var cacheKeys = new string[10000].Select(c => Guid.NewGuid().ToString()).ToList();

            var taskList = new List<Task>();

            for (var i = 0; i < iterations; i++)
            {
                foreach (var cacheKey in cacheKeys)
                {
                    var task = client.AddOrGetAsync(cacheKey, async () =>
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(2000));
                        return cacheKey;
                    }, policy).TimeoutAfter(1000);

                    taskList.Add(task);
                }
            }

            return Task.WhenAll(taskList);
        }

        private static void AddTestData(ICacheClient client)
        {
            var keys = new List<string>();

            for (var i = 0; i < Iterations; i++)
                keys.Add($@"test:{Guid.NewGuid()}");

            Parallel.ForEach(keys.ToArray(), key =>
            {
                var value = Guid.NewGuid().ToString();

                WriteInfo($@"Adding key '{key}' on client1");
                var data = client.AddOrGetAsync(key, () => value, ItemPolicy.Default).Result;
            });
        }


        private static void TestCountInvalidations(ICacheClient client1, ICacheClient client2)
        {
            var keys = new List<string>();

            for (var i =0; i < Iterations; i++)
                keys.Add($@"test:{Guid.NewGuid()}");

            var keysInCache = new List<string>();

            client1.KeyRemoved += (sender, args) =>
            {
                keysInCache.Remove(args.Key);
            };

            Parallel.ForEach(keys.ToArray(), key =>
            {
                var value = Guid.NewGuid().ToString();

                WriteInfo($@"Adding key '{key}' on client1");
                var data = client1.AddOrGetAsync(key, () => value, ItemPolicy.Default).Result;

                if (!value.Equals(data))
                    WriteError($@"'{value}' is not the expected value '{data}' on client1");

                keysInCache.Add(key);

                WriteInfo($@"Invalidating key '{key}' on client2");
                client2.InvalidateAsync(key).Wait();

                WriteInfo($@"Get key '{key}' on client2");
                data = client2.Get<string>(key);
                Debug.Assert(data == null, $@"Found key {key} in client2");

                ReadKey(client1, nameof(client1), key, null, TimeSpan.FromSeconds(10));
            });

            Thread.Sleep(1000);

            Debug.Assert(!keysInCache.Any());

            Parallel.ForEach(keys, key =>
            {
                var data = client1.Get<string>(key);
                Debug.Assert(data == null);

                data = client2.Get<string>(key);
                Debug.Assert(data == null);
            });
        }

        private static void TestNormal(ICacheClient client)
        {
            for (var i = 0; i < Iterations; i++)
            {
                var value = Guid.NewGuid().ToString();
                var key = $"test:{value}";

                Console.WriteLine($@"Adding key '{key}' on client1");
                var data = client.AddOrGetAsync(key, () => value, ItemPolicy.Default).Result;
                Debug.Assert(value.Equals(data));

                Thread.Sleep(100);

                WriteInfo($@"Invalidating key '{key}' on client1");
                client.Invalidate(key);
            }
        }

        private static void TestNormal(ICacheClient client, int iterations, TimeSpan sleepDuration)
        {
            var counter = 0;
            var timer = new Timer(TimeSpan.FromSeconds(1).TotalMilliseconds);
            timer.Elapsed += (sender, args) =>
            {
                Console.WriteLine($"{counter} op/s");
                Debug.WriteLine($"{counter} op/s");
                counter = 0;
            };
            timer.Start();

            for (var i = 0; i < iterations; i++)
            {
                try
                {
                    var value = Guid.NewGuid().ToString();
                    var key = $"test:{value}";

                    Console.WriteLine($@"Adding key '{key}'");
                    var data = client.AddOrGetAsync(key, () => value, ItemPolicy.Default).Result;
                    //var data = client.Set(key, () => value, ItemPolicy.Default).Result;
                    Interlocked.Add(ref counter, 1);

                    Console.WriteLine($@"Getting key '{key}'");
                    data = client.Get<string>(key);
                    Interlocked.Add(ref counter, 1);

                    //Console.WriteLine($@"Setting key '{key}'");
                    //value = Guid.NewGuid().ToString();
                    //client.Set(key, value, ItemPolicy.Default);
                    //Interlocked.Add(ref counter, 1);

                    //client.SetAsync(key, value, ItemPolicy.Default).Wait();
                    //Interlocked.Add(ref counter, 1);

                    //Console.WriteLine($@"Getting key '{key}'");
                    //data = client.GetAsync<string>(key).Result;
                    //Interlocked.Add(ref counter, 1);

                    //WriteInfo($@"Invalidating key '{key}'");
                    //client.Invalidate(key);
                    //Interlocked.Add(ref counter, 1);

                    //client.InvalidateAsync(key).Wait();
                    //Interlocked.Add(ref counter, 1);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                }
                finally
                {
                    Thread.Sleep(sleepDuration);
                }
            }
        }

        private static void TestInvalidation(ICacheClient client1, ICacheClient client2)
        {
            Parallel.For(0, Iterations, (i) =>
            {
                var value = Guid.NewGuid().ToString();
                var key = $"test:{value}";

                Console.WriteLine($@"Adding key '{key}' on client1");
                var data = client1.AddOrGetAsync(key, () => value, ItemPolicy.Default).Result;
                Debug.Assert(value.Equals(data));

                WriteInfo($@"Reading key '{key}' on client2");
                data = client2.Get<string>(key);
                Debug.Assert(value.Equals(data));

                WriteInfo($@"Invalidating key '{key}' on client1");
                client1.Invalidate(key);

                WriteInfo($@"Reading key '{key}' on client1");
                ReadKey(client1, nameof(client1), key, null, TimeSpan.FromSeconds(10));

                WriteInfo($@"Reading key '{key}' on client2");
                ReadKey(client2, nameof(client2), key, null, TimeSpan.FromSeconds(10));
            });
        }

        private static ICacheClient Create(string name, string redisConnectionString, int redisDatabaseId)
        {
             
            var client = CacheClient.Create(conf =>
            {
                conf.LocalCache.Enable();
                conf.RemoteCache.UseRedis(redisConnectionString, redisDatabaseId).Enable();
                conf.Notifications.UseRedis(redisConnectionString, redisDatabaseId).Enable();
            });

            client.KeyRemoved += (sender, args) =>
            {
                WriteInfo($@"Cache key '{args.Key}' removed on {name}");
            };

            client.KeyInvalidated += (sender, args) =>
            {
                WriteInfo($@"Cache key '{args.Key}' invalidated on {name}");
            };

            client.Flushed += (sender, args) =>
            {
                WriteInfo($@"Cache flushed on {name}");
            };

            return client;
        }

        private static ICacheClient CreateRemoteOnly(string name, string redisConnectionString, int redisDatabaseId)
        {
            var client = CacheClient.Create(conf =>
            {
                conf.LocalCache.Disable();
                conf.RemoteCache.UseRedis(redisConnectionString, redisDatabaseId).Enable();
                conf.Notifications.UseRedis(redisConnectionString, redisDatabaseId).Enable();
            });

            client.KeyRemoved += (sender, args) =>
            {
                WriteInfo($@"Cache key '{args.Key}' removed on {name}");
            };

            client.KeyInvalidated += (sender, args) =>
            {
                WriteInfo($@"Cache key '{args.Key}' invalidated on {name}");
            };

            client.Flushed += (sender, args) =>
            {
                WriteInfo($@"Cache flushed on {name}");
            };

            return client;
        }

        private static void ReadKey(ICacheClient client, string clientName, string key, string expectedValue, TimeSpan timeout)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            WriteInfo($@"Get key '{key}' on {clientName}");
            var data = client.GetAsync<string>(key).Result;

            if (data == expectedValue)
                return;

            WriteWarning($@"Key '{key}' on {clientName} was not as expected.");

            while (true)
            {
                Thread.Sleep(20);

                data = client.GetAsync<string>(key).Result;

                if (data == expectedValue)
                    break;

                if (stopwatch.Elapsed > timeout)
                {
                    WriteError($@"Key {key} was did not match the expected value in {clientName} after {timeout.TotalMilliseconds}ms.");
                    return;
                }
            }

            stopwatch.Stop();

            WriteWarning($@"Key '{key}' on {clientName} was the expected after {stopwatch.ElapsedMilliseconds}ms.");
        }

        private static void WriteInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Error.WriteLine(message);
            Console.ResetColor();
        }

        private static void WriteWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();

            Debug.WriteLine(message);
        }

        private static void WriteError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Error.WriteLine(message);
            Console.ResetColor();

            Debug.WriteLine(message);
        }
    }
}
