﻿using System;

namespace Caching.Abstracts
{
    public class CacheItem<T>
    {
        public static readonly CacheItem<T> Default = new CacheItem<T>(string.Empty, default(T), DateTime.Now, TimeSpan.FromSeconds(2),false);

        public CacheItem(string key, T value, DateTime added, TimeSpan? ttl, bool isSliding)
        {
            Value = value;
            Key = key;
            Added = added;
            Ttl = ttl;
            IsSliding = isSliding; 
        }

        public T Value { get; set; }
        public string Key { get; set; }
        public DateTime Added { get; set; } 
        public TimeSpan? Ttl { get; set; }
        public bool IsSliding { get; set; }
    }
}
