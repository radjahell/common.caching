﻿using System;

namespace  Caching.Abstracts
{ 
    public class ItemPolicy
    { 
        public static readonly ItemPolicy Default = new ItemPolicy { Name = "(default)" };

        public ItemPolicy()
        {
            Name = "(notset)"; 
        } 
        public string Name { get; internal set; } 
        public TimeSpan? Ttl { get; set; }
        public bool IsSliding { get; set; }
    }
}
