﻿using System.Threading.Tasks;

namespace Caching.Abstracts
{
    public interface ICache
    {
        void Set<T>(string key, T value, ItemPolicy itemPolicy);
        Task<CacheItem<T>> GetAsync<T>(string key);
        void Remove(string key); 
        Task<bool> ContainsAsync(string key); 
    }

    public interface IRemoteCache : ICache
    {
        void Configure(string connectionString,
            int databaseId);
    }
}
