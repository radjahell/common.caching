﻿using System;

namespace Caching.Abstracts
{
    [Flags]
    public enum CacheType
    { 
        None = 0, 
        Memory = 1, 
        Remote = 2, 
        Both = Memory | Remote
    }
}
