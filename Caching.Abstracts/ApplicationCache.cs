﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Caching.Abstracts
{
    public class ApplicationCache : Cache
    {
        static object locker = new object();
        private ItemPolicy _itemPolicy { get; set; }

        public ApplicationCache()
        { 
            _itemPolicy = new ItemPolicy {};
        } 

        public async override Task<bool> ContainsAsync(string key)
        {
            

                if (_cacheType.HasFlag(CacheType.Memory))
                    return await MemoryCache.ContainsAsync(key);

                if (_cacheType.HasFlag(CacheType.Remote))
                    return await RemoteCache.ContainsAsync(key);

                return false; 
        }

        public async override Task<CacheItem<T>> GetAsync<T>(string key)
        {
            if (_cacheType.HasFlag(CacheType.Memory))
                return await MemoryCache.GetAsync<T>(key);

            if (_cacheType.HasFlag(CacheType.Remote)) 
                return await RemoteCache.GetAsync<T>(key); 

            return CacheItem<T>.Default;
        }

        public override void Remove(string key)
        {
            if (_cacheType.HasFlag(CacheType.Memory))
                MemoryCache.Remove(key);

            if (_cacheType.HasFlag(CacheType.Remote))
                RemoteCache.Remove(key);
        } 

        public override void Set<T>(string key, T value)
        {

            if (_cacheType.HasFlag(CacheType.Memory))
                MemoryCache.Set<T>(key, value, _itemPolicy);

            if (_cacheType.HasFlag(CacheType.Remote))
                RemoteCache.Set<T>(key, value, _itemPolicy);
        }


        public ApplicationCache SetTTL(TimeSpan ttl)
        {
            _itemPolicy.Ttl = ttl; 
            return this;
        }

        public ApplicationCache EnableSlidingExpiration()
        {
            _itemPolicy.IsSliding = true;  
            return this;
        }

        public ApplicationCache EnableMemoryCache()
        { 
            if (_cacheType.HasFlag(CacheType.Remote))
                _cacheType = CacheType.Both;
            else
                _cacheType = CacheType.Memory;
            return this;
        }

        public ApplicationCache EnableRemoteCache()
        {
            if (_cacheType.HasFlag(CacheType.Memory))
                _cacheType = CacheType.Both;
            else
                _cacheType = CacheType.Remote;
            return this;
        }

        public async override Task<CacheItem<T>> AddOrGetAsync<T>(string key, T value)
        {
            using (var semaphore = new SemaphoreSlim(1))
            {
                await semaphore.WaitAsync();

                try
                {
                    if (_cacheType.HasFlag(CacheType.Remote))
                    {
                        if (await RemoteCache.ContainsAsync(key))
                            return await RemoteCache.GetAsync<T>(key);
                        else
                        {
                            RemoteCache.Set(key, value, _itemPolicy);
                            return await RemoteCache.GetAsync<T>(key);
                        }
                    }

                    if (_cacheType.HasFlag(CacheType.Memory))
                    {
                        if (await MemoryCache.ContainsAsync(key))
                            return await MemoryCache.GetAsync<T>(key);
                        else
                        {
                            MemoryCache.Set(key, value, _itemPolicy);
                            return await MemoryCache.GetAsync<T>(key);
                        }
                    }

                    return CacheItem<T>.Default;
                }
                finally
                {
                    semaphore.Release(); 
                } 
            }
        }
    };
}

