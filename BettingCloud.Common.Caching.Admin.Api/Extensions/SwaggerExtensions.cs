﻿using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.SwaggerGen.Application;
using System.IO;

namespace BettingCloud.Common.Caching.Admin.Api.Extensions
{
    internal static class SwaggerExtensions
    {
        public static void IncludeXmlDocumentation(this SwaggerGenOptions options, string xmlFilename)
        {
            var path = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, xmlFilename);

            options.IncludeXmlComments(path);
        }

    }
}
