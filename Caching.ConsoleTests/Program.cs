﻿
using Caching.Standart.Abstracts;
using Caching.Standart.Memory;
using Caching.Standart.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Caching.ConsoleTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //Cache cache = new ApplicationCache
            //{
            //    MemoryCache = new MemoryCacheProvider(),
            //    RemoteCache = new RedisCacheProvider()
            //                        .ConfigureRedis("localhost:6379,abortConnect=false,syncTimeout=3000", 0)
            //}
            ////.EnableMemoryCache()
            //.SetTTL(new TimeSpan(0,0,40))
            //.EnableSlidingExpiration()
            //.EnableRemoteCache();


             testAsync();
          



            //var guid = Guid.NewGuid();
            //var key = $"rmo:{guid}";
            
            //cache.Set(key, guid.ToString());

            //var item = cache.Get<string>(key);

     

        }

        public static async Task testAsync()
        {
 

            Cache cache = new ApplicationCache
            {
                MemoryCache = new MemoryCacheProvider(),
                RemoteCache = new RedisCacheProvider()
                                  .ConfigureRedis("localhost:6379,abortConnect=false,syncTimeout=3000", 0)
            }
          .EnableMemoryCache()
          .SetTTL(new TimeSpan(0, 0, 100))
          .EnableSlidingExpiration()
          .EnableRemoteCache();

            Cache cache2 = new ApplicationCache
            {
                MemoryCache = new MemoryCacheProvider(),
                RemoteCache = new RedisCacheProvider()
                                   .ConfigureRedis("localhost:6379,abortConnect=false,syncTimeout=3000", 0)
            }
           .EnableMemoryCache()
           .SetTTL(new TimeSpan(0, 0, 100))
           .EnableSlidingExpiration()
           .EnableRemoteCache();

            await TestConcurrency(cache); 
            await TestConcurrency(cache2); 

            //Parallel.ForEach(keys, (key) =>
            //{
            //    Console.WriteLine($"{key}");
            //    cache2.AddOrGetAsync(key.Key, key.Value);
            //    cache.AddOrGetAsync(key.Key, key.Value);

            //});
            Console.ReadLine();
        }

        private async static Task TestConcurrency(Cache cache)
        {
            var iterations = 2; 

            var cacheKeys = new string[70].Select(c => Guid.NewGuid().ToString()).ToList();

            var taskList = new List<Task>();

            for (var i = 0; i < iterations; i++)
            {
                foreach (var cacheKey in cacheKeys)
                {
                    var key = $"test:{cacheKey}";
                    Console.WriteLine($"{key}");
                    var task =  cache.AddOrGetAsync(key, cacheKey); 
                    taskList.Add(task);
                }
            }

            await Task.WhenAll(taskList);
        }
    }
}
