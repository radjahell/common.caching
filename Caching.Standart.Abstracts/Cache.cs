﻿using System;
using System.Threading.Tasks;

namespace Caching.Standart.Abstracts
{
    public abstract class Cache
    {
        public ICache MemoryCache { get; set; }
        public ICache RemoteCache { get; set; }
        protected CacheType _cacheType { get; set; }
        protected ItemPolicy itemPolicy { get; set; }

        public abstract void Set<T>(string key, T value);
        public abstract void Remove(string key);
        public abstract Task<CacheItem<T>> GetAsync<T>(string key);
        public abstract Task<bool> ContainsAsync(string key);
        public abstract Task<CacheItem<T>> AddOrGetAsync<T>(string key, T value);
    }
}
