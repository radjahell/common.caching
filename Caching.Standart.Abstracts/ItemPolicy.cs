﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caching.Standart.Abstracts
{
    public class ItemPolicy
    {
        public static readonly ItemPolicy Default = new ItemPolicy { Name = "(default)" };

        public ItemPolicy()
        {
            Name = "(notset)";
        }
        public string Name { get; set; }
        public TimeSpan? Ttl { get; set; }
        public bool IsSliding { get; set; }
    }
}
