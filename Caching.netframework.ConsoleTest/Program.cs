﻿
using Caching.Standart.Abstracts;
using Caching.Standart.Memory;
using Caching.Standart.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caching.netframework.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            testAsync();

        }


        public static async Task testAsync()
        {


            Cache cache = new ApplicationCache
            {
                MemoryCache = new MemoryCacheProvider(),
                RemoteCache = new RedisCacheProvider()
                                  .ConfigureRedis("localhost:6379,abortConnect=false,syncTimeout=3000", 0)
            }
         // .EnableMemoryCache()
          .SetTTL(new TimeSpan(0, 0, 100))
          .EnableSlidingExpiration()
          .EnableRemoteCache();

           // Cache cache2 = new ApplicationCache
           // {
           //     MemoryCache = new MemoryCacheProvider(),
           //     RemoteCache = new RedisCacheProvider()
           //                        .ConfigureRedis("localhost:6379,abortConnect=false,syncTimeout=3000", 0)
           // }
           //.EnableMemoryCache()
           //.SetTTL(new TimeSpan(0, 0, 100))
           //.EnableSlidingExpiration()
           //.EnableRemoteCache();

            await TestConcurrency(cache);
         //   await TestConcurrency(cache2);

            //Parallel.ForEach(keys, (key) =>
            //{
            //    Console.WriteLine($"{key}");
            //    cache2.AddOrGetAsync(key.Key, key.Value);
            //    cache.AddOrGetAsync(key.Key, key.Value);

            //});
            Console.ReadLine();
        }

        private static async Task TestConcurrency(Cache cache)
        {
            const int iterations = 1;

            var cacheKeys = new string[1].Select(c => Guid.NewGuid().ToString()).ToList();

            var taskList = new List<Task>();

            for (var i = 0; i < iterations; i++)
            {
                foreach (var cacheKey in cacheKeys)
                {
                    var key = $"test:{cacheKey}";
                    Console.WriteLine($"{key}");
                    var task = cache.AddOrGetAsync(key, cacheKey);
                    taskList.Add(task);
                }
            }

            await Task.WhenAll(taskList);
        }
    }
}
