# BettingCloud.Caching

Cache brings data which is constantly required (such as translations, currency conversion, business rules, configuration) closer to the consumer for fast retrieval by avoiding the need to continuously ask for a fresh copy from source (such as database, web service or rest API). The *BettingCloud.Caching* library provides a distributed cache implementation which supports 1st level (memory) and 2nd level (Redis) cache stores, cache item policies with both absolute and sliding expiration and remote cache invalidation.

## Configuring the ICacheClient
The *BettingCloud.Caching* library offers a flexible configuration to support the different caching requirements depending on the context. The following sections describe the basic configuration to get you started and each configuration option in detail.

### Basic configuration
We need to create an instance of ICacheClient using the Create() method of the CacheClient factory. The method accepts an Action<IClientConfigurator> which is used to configure the cache client according to your needs.

```csharp
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
});
```

### Remote cache
The remote cache can be configured using the IClientConfigurator.RemoteCache property. The *BettingCloud.Caching* library supports one (1) remote cache providers, namely Redis. It is very important that the same remote cache provider is used throughout your applications to make sure to use the same 2nd level cache store.

**Using Redis**
The Redis remote cache provider is configured using the following syntax.

|Method|Parameters|Description|
| --------|---------|-------|
|RemoteCache.UseRedis()|*string* connectionString|The Redis connection settings used by the underlying StackExchange.Redis library (see [here for more details](https://github.com/StackExchange/StackExchange.Redis/blob/master/Docs/Configuration.md)).
||*int* databaseId|The Redis database ID to be used.|

```csharp
var client = CacheClient.Create(c => {
    c.RemoteCache.UseRedis("localhost:6379", 0);
});
```

### Notifications
The *BettingCloud.Caching* cache client needs to register for change notifications to be able to manage both the 1st and 2nd cache stores and minimize the probability of using stale data. Notifications provides Redis or RabbitMQ implementations.

**Using Redis**
The Redis notification provider is configured using the following syntax. It is important to note that apart from using the Publish/Subscribe features of Redis, the Redis notification provider subscribes to keyevent notifications which are useful when using Redis remote cache provider because these events are triggered when the cache is expired and evicted from the Redis server. 

|Method|Parameters|Description|
| --------|---------|-------|
|Notifications.UseRedis()|*string* connectionString|The Redis connection settings used by the underlying StackExchange.Redis library (see [here for more details](https://github.com/StackExchange/StackExchange.Redis/blob/master/Docs/Configuration.md)).|

```csharp
var client = CacheClient.Create(c => {
    c.RemoteCache.UseRedis("localhost:6379", 0);
    c.Notifications.UseRedis("localhost:6379");
});
```

**Using RabbitMQ**
The RabbitMQ notification provider is configured using the following syntax.


|Method|Parameters|Description|
| --------|---------|-------|
|Notifications.UseRabbitMQ()|*string* host|The RabbitMQ host (usually the load balancer address).|
||*string* exchangeName|The RabbitMQ fanout exchange to register for notifications.|
||*int?* port|(Optional) The RabbitMQ port if not using the standard 5672 TCP port.|
||*string* username|(Optional) The username to use to authenticate against RabbitMQ.|
||*string* password|(Optional) The password to use to authenticate against RabbitMQ.|
||*string* virtualHost|(Optional) The RabbitMQ virtual host to be used.|

```csharp
var client = CacheClient.Create(c => {
     c.RemoteCache.UseRedis("localhost:6379", 0);
     c.Notifications.UseRabbitMQ("localhost", "Cache.Notifications", 5672, "guest", "guest", "cacheVirtualHost");
});
```

### Diagnostics
The *BettingCloud.Caching* support two (2) types of diagnostics, logging and performance counters. The configuration expects the caller to provide an instance which implements *BettingCloud.Caching.Diagnostics.Logging.ILogger* and *BettingCloud.Caching.Diagnostics.Performance.IPerformanceCounters*, thus allow the caller to wrap their preferred logging and performance counter libraries.

**Logging**
```csharp
public class ConsoleLogger : ILogger {
    public void Exception(Exception ex, string message) {
        LogToConsole(LogEventLevel.Error, message, ex);
    }
 
    public void Log(LogEventLevel level, string message) {
        LogToConsole(level, message);
    }
 
    private void LogToConsole(LogEventLevel level, string message, Exception ex = null) {
        var timestamp = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
        var msg = $"{timestamp}:{level}:{message}";
  
        if (ex != null) {
            msg += $"\r\n{timestamp}:{level}:{ex.ToString()}";
        }
 
        Console.WriteLine(msg);
    }
}
  
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.Diagnostics.UseLogger(new ConsoleLogger());
});
```

**Performance counters**
```csharp
public class ConsolePerformanceCounters : IPerformanceCounters {
    public void Hit(string key, CacheLocationType cacheLocation) {
        var message = "Hit:{cacheLocation.ToString()}:{key}";
        Write(message, ConsoleColor.Green);
    }
 
    public void Miss(string key, CacheLocationType cacheLocation) {
        var message = "Miss:{cacheLocation.ToString()}:{key}";
        Write(message, ConsoleColor.Red);
    }
 
    public void Remove(string key, CacheLocationType cacheLocation) {
        var message = "Remove:{cacheLocation.ToString()}:{key}";
        Write(message, ConsoleColor.Yellow);
    }
 
    public void Set(string key, CacheLocationType cacheLocation) {
        var message = "Set:{cacheLocation.ToString()}:{key}";
        Write(message, ConsoleColor.Blue);
    }
 
    private void Write(string message, ConsoleColor color) {
        var timestamp = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
        Console.ForegroundColor = color;
        Console.WriteLine(message);
        Console.ResetColor();
    }
}

var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.Diagnostics.UsePerformanceCounters(new ConsolePerformanceCounters());
});
```

### Cache item policies
Policies provides a way to control how different cache items are cached and expired. Cache item policies can be created using the ItemPolicy.Builder() static factory builder method. The ItemPolicy class provides a default policy which when used will set the cache item not to expire.

|Method|Parameters|Description|
|----|----|----|
|ItemPolicy.Builder.LocalPolicy()|*int* ttlInSecs|The time-to-live in seconds for local cache.|
||*bool* isSliding|(Optional) Indicates whether the cache item should use absolute (false) or sliding expiration (true).|
|ItemPolicy.Builder.LocalPolicy()|*TimeSpan* ttl|The time-to-live for local cache.|
||*bool* isSliding|(Optional) Indicates whether the cache item should use absolute (false) or sliding expiration (true).|
|ItemPolicy.Builder.RemotePolicy()|*int* ttlInSecs|The time-to-live in seconds for remote cache.|
||*bool* isSliding|(Optional) Indicates whether the cache item should use absolute (false) or sliding expiration (true).|
|ItemPolicy.Builder.RemotePolicy()|*TimeSpan* ttl|The time-to-live for remote cache.|
||*bool* isSliding|(Optional) Indicates whether the cache item should use absolute (false) or sliding expiration (true).|
|ItemPolicy.Builder.Create()|*string* name|Assigned the policy name and returns an ItemPolicy instance.|

```csharp
var timeToLive = TimeSpan.FromMinutes(30);
var isSliding = true;
var policyExpireAfter30Mins = ItemPolicy.Builder
       .LocalPolicy(timeToLive, isSliding)
       .RemotePolicy(timeToLive, isSliding)
       .Create("ExpiryAfter30MinutesUsingSlidingExpiration");
```

The cache item policies need to be configured through the CacheClient factory builder using the following methods.
|Method|Parameters|Description|
|----|----|----|
|Policies.Add()|*ItemPolicy* policy|Adds the policy to the list of policies available in the ICacheClient instance.|
|Policies.SetDefault()|*ItemPolicy* policy|Sets the specified policy as the default policy to be applied when no policy is provided.|

```csharp
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.RemoteCache.UseRedis("localhost:6379", 0);
    c.Notifications.UseRedis("localhost:6379");
    c.Policies
        .Add(policyExpireAfter30Mins)
        .SetDefault(ItemPolicy.Default);
});
```

### Failure behaviour
Even though services such as Redis and RabbitMQ are configured with the highest degree of availability to offer a consistent and reliable service, service outages are still inevitable. The *BettingCloud.Caching* library is able to automatically recover from connection failures and offers a configurable way to control the internal behaviour during outages. Please note that exceptions raised by system outages will be logged through the ILogger irrespective of the configured failure behaviour.

|Behaviour|Description|
|----|----|
|FailureBehaviour.NotSet|(Default) Will continue using the local memory cache if set, otherwise will fetch the data from source.|
|FailureBehaviour.ThrowException|Will through an exception which needs to be handled by the caller.|
|FailureBehaviour.UseMemory|Will use the local memory cache only until the remote caching is available.|
|FailureBehaviour.UseFetchOrDefault|Will fetch the data from source (if the fetch function is provided) and returns a default value if case the fetch is not available or throws an exception.|

Different failure bebaviours are associated with cache items through cache item policies using the OnFailure property in the CacheClient factory.

|Method|Parameters|Description|
|----|----|----|
|OnFailure.Use|*FailureBehaviour* behaviour|The failure behaviour to take for request to cache items associated with the specified ItemPolicy.|
||*ItemPolicy* policy|The ItemPolicy associated with the specified failure behaviour.|
|OnFailure.Use|*FailureBehaviour* behaviour|The failure behaviour to take for request to cache items associated with the specified ItemPolicy.|
||*string* policyName|The name of the ItemPolicy associated with the specified failure behaviour.|
|OnFailure.SetDefault|*FailureBehaviour* behaviour|The default failure behaviour to be used.|

```csharp
var throwItemPolicy = ItemPolicy.Builder
       .LocalPolicy(TimeSpan.FromMinutes(5))
       .RemotePolicy(TimeSpan.FromMinutes(5))
       .Create("ThrowItemPolicy");
 
var alwaysFetchItemPolicy = ItemPolicy.Builder
       .LocalPolicy(TimeSpan.FromMinutes(5))
       .RemotePolicy(TimeSpan.FromMinutes(5))
       .Create("AlwaysFetchItemPolicy");
 
var useMemoryItemPolicy = ItemPolicy.Builder
       .LocalPolicy(TimeSpan.FromMinutes(5))
       .RemotePolicy(TimeSpan.FromMinutes(5))
       .Create("UseMemoryItemPolicy");
 
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.RemoteCache.UseRedis("localhost:6379", 0);
    c.Notifications.UseRedis("localhost:6379");
    c.OnFailure
       .Use(FailureBehaviour.UseFetchOrDefault, alwaysFetchItemPolicy.Name)
       .Use(FailureBehaviour.ThrowException, throwItemPolicy.Name)
       .Use(FailureBehaviour.UseMemory, useMemoryItemPolicy.Name)
       .SetDefault(FailureBehaviour.NotSet);
    c.Policies
       .Add(throwItemPolicy)
       .Add(alwaysFetchItemPolicy)
       .Add(useMemoryItemPolicy)
       .SetDefault(ItemPolicy.Default);
});
```

### Namespace
Namespaces allows the cache to be logically segregated using a key prefix using the format {namespace}:{key}. In our context the a namespace pattern of "brand:vertical" could be used to apply two (2) segregation, i.e. at brand level and vertical level (wallet, bonus, games, sports, etc.)
|Method|Parameters|Description|
|----|----|----|
|UseNamespace()|*string* ns|The namespace to be prefixed to the keys.|

```csharp
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.UseNamespace("brand:vertical");
});
```

### Compression
Complex cache items might be very large in size and can take a while to be streamed to the remote cache. The ICacheClient can be configured to use compression depending on the size of the object when using the Redis remote cache provider.

|Method|Parameters|Description|
|---|---|---|
|UseCompression()|*int* byteSizeThreshold|The byte size threshold determines whether a cache item should be compressed or not.|

```csharp
var client = CacheClient.Create(c => {
    c.LocalCache.Enable();
    c.RemoteCache.UseRedis("localhost:6379", 0);
    c.UseCompression(1024 * 10); // Compress object larger than 10kb
});
```

## How to use the ICacheClient
The ICacheClient offers a set of sync and async method to set, retrieve, invalidate and remove cached items. The following sections explain how these can be used in practice.

### Setting a new cache item
Cache items can be added using two (2) different methods, either the Set() or AddOrGet() methods or their async variants.

|Method|Parameters|Description|
|----|----|----|
|ICacheClient.Set<T>()|*string* key|The key that uniquely identifies the requested cache item.|
||*T* item|The item to be cached.|
||*ItemPolicy* policy|The ItemPolicy instance to be associated with the cached item.|
|ICacheClient.Set<T>()|*string* key|The key that uniquely identifies the requested cache item.|
||*T* item|The item to be cached.|
||*string* policyName|The item policy name of the ItemPolicy instance to be associated with the cached item.|
|ICacheClient.AddOrGet<T>()|*string* key|The key that uniquely identifies the requested cache item.|
||*Func<T>* fetch|The function which returns the item to be cached.|
||*ItemPolicy* policy|The ItemPolicy instance to be associated with the cached item.|
|ICacheClient.AddOrGet<T>()|*string* key|The key that uniquely identifies the requested cache item.|
||*Func<T>* fetch|The function which returns the item to be cached.|
||*string* policyName|The item policy name of the ItemPolicy instance to be associated with the cached item.|

```csharp
var data = repo.GetData();

client.Set("key", data, policyExpireAfter30Mins);
await client.SetAsync("key", data, policyExpireAfter30Mins);
```

```csharp
// A function which returns the actual data to be cached
Func<IEnumerable<Models.UserModel>> fetch = () => repo.GetData();                  

// A function which returns a Task<T> result
Func<Task<IEnumerable<Models.UserModel>>> fetchAsync = () => repo.GetDataAsync();

var dataFromSync  = client.AddOrGet("key", fetch, policyExpireAfter30Mins);
var dataFromAsync = await client.AddOrGetAsync("key", fetch, policyExpireAfter30Mins);
var dataFromAsyncTask = await client.AddOrGetAsync("key", fetchAsync, policyExpireAfter30Mins);
```

### Retrieving a cache item
Cache items can be retrieved using the Get() and AddOrGet() methods or their async variants.

|Method|Parameters|Description|
|----|----|----|
|ICacheClient.Get<T>()|*string* key|The key that uniquely identifies the requested cache item.|

```csharp
var dataFromSync = client.Get<DataType>("key");
var dataFromAsync = await client.GetAsync<DataType>("key");
```

### Invalidating a cache item
Cache items can be selectively invalidated from any ICacheClient consumer using the Invalidate() or InvalidateAsync() methods. The invalidation will publish an invalidation message to through the notification provider (Redis or RabbitMQ) depending on the configuration) which will be recevied by any client subscribed for notifications, which will delete both the local and remote cache items (if these exist).

It is a general practice that the cache invalidation is called by the business logic handling data changes (ex. Backoffice, Wallets, etc.) ensuring that consumers remove the cached values and retrieve a fresh copy when required.

|Method|Parameters|Description|
|----|----|----|
|ICacheClient.Invalidate()|*string* key|The key to be invalidated.|

```csharp
client.Invalidate("key");
await client.InvalidateAsync("key");
```

### Removing a cache item
The ICacheClient Remove() method works very similarly to Invalidate() with the only difference that it first calls the remove of the local and remote cache before publishing a message through the notification provider (Redis or RabbitMQ). The Remove() method is handy when the same instance is consuming and invalidating data.

|Method|Parameters|Description|
|----|----|----|
|ICacheClient.Remove()|*string* key|The key to be removed and invalidated.|

```csharp
client.Remove("key");
await client.RemoveAsync("key");
```
